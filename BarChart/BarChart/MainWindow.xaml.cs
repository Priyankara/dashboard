﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Windows.Threading;

namespace BarChart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //set label heights to 0
            label3.Height = label4.Height = label5.Height = label9.Height = label10.Height = label11.Height = 0;
        }

        //initializing data variables
        int x1,x2,x3,x4,x5,x6=0;

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //Init data table and add columns
            DataTable data = new DataTable();
            data.Columns.Add("Month");
            data.Columns.Add("NoOfItems");

            //set values
            data.Rows.Add("Jan",300);
            data.Rows.Add("Feb", 30);
            data.Rows.Add("Mar", 80);
            data.Rows.Add("Apr", 20);
            data.Rows.Add("May", 70);
            data.Rows.Add("Jun", 150);

            //timer
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timerx);
            timer.Interval = new TimeSpan(0,0, 0,0,10);
            timer.Start();

            //Pass noOfItems to variables
            x1 = int.Parse(data.Rows[0]["NoOfItems"].ToString());
            x2 = int.Parse(data.Rows[1]["NoOfItems"].ToString());
            x3 = int.Parse(data.Rows[2]["NoOfItems"].ToString());
            x4 = int.Parse(data.Rows[3]["NoOfItems"].ToString());
            x5 = int.Parse(data.Rows[4]["NoOfItems"].ToString());
            x6 = int.Parse(data.Rows[5]["NoOfItems"].ToString());

            //set month from table
            label6.Content = data.Rows[0]["Month"].ToString();
            label7.Content = data.Rows[1]["Month"].ToString();
            label8.Content = data.Rows[2]["Month"].ToString();
            label12.Content = data.Rows[3]["Month"].ToString();
            label13.Content = data.Rows[4]["Month"].ToString();
            label14.Content = data.Rows[5]["Month"].ToString();



        }

        private void timerx(object sender, EventArgs e)
        {
            //Check the condition and increse height bt tht time invoking
            if(label3.Height<x1)
                label3.Height =label3.Height+1;
            if(label4.Height<x2)
                label4.Height = label4.Height+1;
            if(label5.Height<x3)
                label5.Height =  label5.Height+1;
            if (label9.Height < x4)
                label9.Height = label9.Height + 1;
            if (label10.Height < x5)
                label10.Height = label10.Height + 1;
            if (label11.Height < x6)
                label11.Height = label11.Height + 1;
        }

        
    }
}
