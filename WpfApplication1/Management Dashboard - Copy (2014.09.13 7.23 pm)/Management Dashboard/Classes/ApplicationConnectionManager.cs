﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Management_Dashboard
{
    class ApplicationConnectionManager
    {
        public static SqlConnection NewConn;
        public static string ConStr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public static SqlConnection GetConnection()
        {
            NewConn = new SqlConnection(ConStr);
            return NewConn;
        }

        public List<List<string>> getsystabelcolumn(SqlConnection NewConn, string systemName)
        {
            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            List<string> table = new List<string>();
            List<List<string>> table_column = new List<List<string>>();

            try
            {
                SqlCommand newCmd = NewConn.CreateCommand();
                newCmd.Connection = NewConn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "SELECT distinct TableName FROM SysTableColumn where systemName='" + systemName + "'";

                SqlDataReader dr1 = newCmd.ExecuteReader();

                while (dr1.Read())
                {

                    table.Add(dr1[0].ToString());
                }
            }
            catch
            {
            }

            NewConn.Close();

            //get all columns of all tables
            int length = table.Count;

            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            for (int a = 0; a < length; a++)
            {
                if (NewConn.State.ToString() == "Closed")
                {
                    NewConn.Open();
                }
                List<string> column_t = new List<string>();
                try
                {
                    SqlCommand Cmd = NewConn.CreateCommand();
                    Cmd.Connection = NewConn;
                    Cmd.CommandType = CommandType.Text;
                    Cmd.CommandText = "SELECT columnName from SysTableColumn WHERE TableName = '" + table[a].ToString() + "' and systemName='"+systemName+"'";

                    SqlDataReader drd = Cmd.ExecuteReader();
                    column_t.Add(table[a].ToString());

                    while (drd.Read())
                    {
                        column_t.Add(drd[0].ToString());

                    }

                }
                catch
                {
                }
                table_column.Add(column_t);
                NewConn.Close();
            }

            NewConn.Close();
            return table_column;
        }

        public List<List<string>> getexceltabelcolumn(SqlConnection NewConn, string systemName)
        {
            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            List<string> table = new List<string>();
            List<List<string>> table_column = new List<List<string>>();

            try
            {
                SqlCommand newCmd = NewConn.CreateCommand();
                newCmd.Connection = NewConn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "SELECT distinct sheetName FROM SysExcelFiles where systemName='" + systemName + "'";

                SqlDataReader dr1 = newCmd.ExecuteReader();

                while (dr1.Read())
                {
                    table.Add(dr1[0].ToString());
                }
            }
            catch
            {
            }

            NewConn.Close();

            //get all columns of all tables
            int length = table.Count;

            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            for (int a = 0; a < length; a++)
            {
                if (NewConn.State.ToString() == "Closed")
                {
                    NewConn.Open();
                }
                List<string> column_t = new List<string>();
                try
                {
                    SqlCommand Cmd = NewConn.CreateCommand();
                    Cmd.Connection = NewConn;
                    Cmd.CommandType = CommandType.Text;
                    Cmd.CommandText = "SELECT columnName from SysExcelFiles WHERE sheetName = '" + table[a].ToString() + "' and systemName='" + systemName + "'";

                    SqlDataReader drd = Cmd.ExecuteReader();
                    column_t.Add(table[a].ToString());

                    while (drd.Read())
                    {
                        column_t.Add(drd[0].ToString());
                    }

                }
                catch
                {
                }
                table_column.Add(column_t);
                NewConn.Close();
            }

            NewConn.Close();
            return table_column;
        }

        public List<string[]> getsystems()
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();

            List<string[]> list = new List<string[]>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem ";

                SqlDataReader drd = Cmd.ExecuteReader();


                while (drd.Read())
                {
                    string[] temp=new string[3] ;
                    temp[0] = drd[0].ToString();
                    temp[1] = drd[1].ToString();
                    temp[2] = drd[9].ToString();
                    list.Add(temp);                  
                }
            }
            catch
            {
            }
            conn.Close();
            return list;
        }

        public string saveQuery(string queryName, string systemName, string queryType, string Query)
        {
            //check system name(pk) exist.
           SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

           try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM Query WHERE queryName = '" + queryName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    status = "exist";
                    conn.Close();
                    return status;
                }

            }
            catch
            {
                conn.Close();
                status = "fail";
                return status;
            }
            conn.Close();


            //save system
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;                
                newCmd.CommandText = "insert into Query(queryName, systemName, queryType, query)  values ('" + queryName + "','" + systemName + "','" + queryType + "','" + @Query + "')";

                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch
            {
                status = "fail";
                return status;
            }

            conn.Close();            
            return status;

        }

        public List<string[]> getQueries()
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string[]> queries = new List<string[]>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM Query";

                SqlDataReader drd = Cmd.ExecuteReader();
                while (drd.Read())
                {
                    string[] temp=new string[4];
                    temp[0]=drd[0].ToString();
                    temp[1]=drd[3].ToString();
                    temp[2] = drd[2].ToString();
                    temp[3] = drd[1].ToString();
                    queries.Add(temp);
                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return queries;
        }

        public string EditDeleteQuery(string queryName, string query_, int operetion)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string status = "Fail";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            if(operetion==1)
            {
                try
                {
                    SqlCommand Cmd = conn.CreateCommand();
                    Cmd.Connection = conn;
                    Cmd.CommandType = CommandType.Text;
                    Cmd.CommandText = "DELETE FROM Query WHERE queryName='" + queryName + "'";

                    Cmd.ExecuteNonQuery();
                    status = "Deleted";
                }
                catch
                {
                    conn.Close();
                    return status;
                }
                conn.Close();  
            }
            else if (operetion == 2)
            {
                try
                {
                    SqlCommand Cmd = conn.CreateCommand();
                    Cmd.Connection = conn;
                    Cmd.CommandType = CommandType.Text;
                    Cmd.CommandText = "UPDATE Query SET query='" + query_ + "' WHERE queryName='" + queryName + "'";

                    Cmd.ExecuteNonQuery();
                    status = "Updated";
                }
                catch
                {
                    conn.Close();
                    return status;
                }                
            }
            conn.Close();
            return status ;        
        }

        public string saveComponent(string chart_name, string com_type, int width_, int height_, int refresh_rate, int type_, string f1, string f2, string f3, string f4, string f5, string f6, string singel_uery_ame, List<string[]> multiple_bars)
        {
            //check system name(pk) exist.
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT componentName FROM Component WHERE componentName = '" + chart_name + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    status = "exist";
                    conn.Close();
                    return status;
                }
            }
            catch
            {
                conn.Close();
                status = "fail";                
                return status;
            }
            conn.Close();

            //save component
            
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "insert into Component(componentName, componentType, width, height, refreshRate, type, field1, field2, field3, field4, field5, field6) values ('" + chart_name + "', '" + com_type + "', " + width_ + ", " + height_ + ", " + refresh_rate + ", " + type_ + ", '" + f1 + "', '" + f2 + "', '" + f3 + "', '" + f4 + "', '" + f5 + "', '" + f6 + "')"; 
             
                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch(Exception e)
            {
                status = "fail";               
                conn.Close();
                return status;
            }
            conn.Close();

            //save componentQuery

            if (type_==1)
            {
                for (int i = 0; i < multiple_bars.Count; i++)
                {
                    if (conn.State.ToString() == "Closed")
                    {
                        conn.Open();
                    }
                    try
                    {
                        SqlCommand newCmd = conn.CreateCommand();
                        newCmd.Connection = conn;
                        newCmd.CommandType = CommandType.Text;
                        newCmd.CommandText = "insert into QueryComponent(componentName, queryName,fieldName) values ('" + chart_name + "', '" + multiple_bars[i][1].ToString() + "', '" + multiple_bars[i][0].ToString() + "')";

                        newCmd.ExecuteNonQuery();
                        status = "saved";
                    }
                    catch
                    {
                        status = "fail";                        
                        conn.Close();
                        return status;
                    }
                    conn.Close();
                }
            }
            else if(type_==2)
            {
                if (conn.State.ToString() == "Closed")
                {
                    conn.Open();
                }
                try
                {
                    SqlCommand newCmd = conn.CreateCommand();
                    newCmd.Connection = conn;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "insert into QueryComponent(componentName, queryName,fieldName) values ('" + chart_name + "', '" + singel_uery_ame+ "', 'NULL')";

                    newCmd.ExecuteNonQuery();
                    status = "saved";
                }
                catch
                {
                    status = "fail";                   
                    conn.Close();
                    return status;
                }
                conn.Close();
            }


            conn.Close();
            return status;
        }        

        public string saveDashBoardView(string viewName, double width, double height, List<string[]> component_of_view)
        {
            //check system name(pk) exist.
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT viewName FROM DashBoardView WHERE viewName = '" + viewName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    status = "exist";
                    conn.Close();
                    return status;
                }
            }
            catch
            {
                conn.Close();
                status = "fail";
                return status;
            }
            conn.Close();

            //save view
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "insert into DashBoardView(viewName,width,height)  values ('" + viewName + "', "+width+", "+height+")";
               

                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch
            {
                status = "fail";
                conn.Close();
                return status;
            }
            
            for (int i = 0; i < component_of_view.Count;i++ )
            {
                if (conn.State.ToString() == "Closed")
                {
                    conn.Open();
                }
                try
                {
                    SqlCommand newCmd = conn.CreateCommand();
                    newCmd.Connection = conn;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "insert into ViewComponent(componentName, viewName, location_x, location_y, scale_x, scale_y)  values ('" + component_of_view[i][0].ToString() + "', '" + viewName + "', " + Double.Parse(component_of_view[i][1].ToString()) + ", " + Double.Parse(component_of_view[i][2].ToString()) + ", " + Double.Parse(component_of_view[i][3].ToString()) + ", " + Double.Parse(component_of_view[i][4].ToString()) + ")";


                    newCmd.ExecuteNonQuery();
                    status = "saved";
                }
                catch
                {
                    status = "fail";
                    conn.Close();
                    return status;
                }
            }
            conn.Close();
            return status;
        }

        //------------------------------------

        public List<string[]> getDashBoardView(string username)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string[]> views = new List<string[]>();
            string query = "";

            if (username == "" || username == null)
            {
                query = "SELECT * FROM DashBoardView ";
            }
            else
            {
                query = "SELECT d.* FROM DashBoardView d where d.viewName in (SELECT v.VIEWNAME FROM USER_VIEWS v where v.USERNAME='" + username + "') ";
            }

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = query;

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    string[] view = new string[3];

                    view[0] = drd[0].ToString();
                    view[1] = drd[1].ToString();
                    view[2] = drd[2].ToString();

                    views.Add(view);
                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return views;
        }


        public List<string[]> getViewComponents(string view_name)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string[]> view_component = new List<string[]>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ViewComponent where viewName='"+view_name+"' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    string[] component = new string[5];
                    component[0] = drd[0].ToString();
                    component[1] = drd[2].ToString();
                    component[2] = drd[3].ToString();
                    component[3] = drd[4].ToString();
                    component[4] = drd[5].ToString();

                    view_component.Add(component);

                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return view_component;
        }


        public List<string> getComponent(string component_name)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string> component_property = new List<string>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM Component where componentName='" + component_name + "' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {          
                    component_property.Add(drd[1].ToString());
                    component_property.Add(drd[2].ToString());
                    component_property.Add(drd[3].ToString());
                    component_property.Add(drd[4].ToString());
                    component_property.Add(drd[5].ToString());
                    component_property.Add(drd[6].ToString());
                    component_property.Add(drd[7].ToString());
                    component_property.Add(drd[8].ToString());
                    component_property.Add(drd[9].ToString());
                    component_property.Add(drd[10].ToString());
                    component_property.Add(drd[11].ToString());
                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return component_property;
        }

        public List<string[]> getQueryComponent(string component_name)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string[]> component_query = new List<string[]>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM QueryComponent where componentName='" + component_name + "' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    string[] component = new string[2];
                    component[0] = drd[1].ToString();
                    component[1] = drd[2].ToString();
  
                    component_query.Add(component);
                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return component_query;
        }

    }
}
