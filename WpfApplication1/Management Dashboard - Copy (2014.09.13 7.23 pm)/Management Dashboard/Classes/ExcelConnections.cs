﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Management_Dashboard
{
    class ExcelConnections
    {
        public List<string> getFolder(string path)
        {
            //get all excel files in a folder
            List<string> files = new List<string>();
            try
            {
                string folderPath = path;
                DirectoryInfo Dir = new DirectoryInfo(folderPath);
                FileInfo[] FileList = Dir.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly);

                foreach (FileInfo FI in FileList)
                {
                    files.Add(FI.FullName.ToString());
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return files;
        }

        public List<List<string>> getFiles(string path)
        {
            //get sheets and their columns of a excel file
            OleDbConnection objConn = null;
            DataTable dt_table = null;
            DataTable dt_column = null;
            List<List<string>> sheet = new List<List<string>>();

            try
            {
                String connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=2'";

                objConn = new OleDbConnection(connString);
                objConn.Open();

                dt_table = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                dt_column = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, null);

                if (dt_table == null)
                {
                    objConn.Close();
                    return null;
                }

                foreach (DataRow row1 in dt_table.Rows)
                {
                    string sheetName = row1["TABLE_NAME"].ToString();
                    List<string> file = new List<string>();
                    file.Add(path);
                    file.Add(sheetName);

                    foreach (DataRow row2 in dt_column.Rows)
                    {
                        if (row2["TABLE_NAME"].ToString() == sheetName)
                        {
                            string columnName = row2["COLUMN_NAME"].ToString();
                            file.Add(columnName);
                        }
                    }

                    sheet.Add(file);
                }
            }
            catch
            {
                objConn.Close();
                return null;
            }
            objConn.Close();
            return sheet;
        }


        public string create_Excelconnection(string systemName, List<List<List<string>>> selections)
        {
            //check system name(pk) exist.
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem WHERE systemName = '" + systemName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    status = "exist";
                    conn.Close();
                    return status;
                }

            }
            catch
            {
                conn.Close();
                status = "fail";
                return status;
            }
            conn.Close();


            //save system
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                
                newCmd.CommandText = "insert into ConectedSystem(systemName,type,activated)  values ('" + systemName + "','Excel',0)";
               
                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch
            {
                status = "fail";
                return status;
            }

            conn.Close();

            //save tabels and columns
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                int file_count = selections.Count;
                for (int i = 0; i < file_count; i++)
                {
                    int sheet_count = selections[i].Count;
                    for (int j = 0; j < sheet_count; j++)
                    {
                        int column_count = selections[i][j].Count;
                        for(int k=2;k<column_count;k++)
                        {
                            SqlCommand newCmd = conn.CreateCommand();
                            newCmd.Connection = conn;
                            newCmd.CommandType = CommandType.Text;
                            newCmd.CommandText = "insert into SysExcelFiles(systemName,fileName,sheetName,columnName)  values ('" + systemName + "','" + selections[i][j][0].ToString() + "','[" + selections[i][j][1].ToString() + "]','" + selections[i][j][k].ToString() + "')";

                            newCmd.ExecuteNonQuery();
                        }
                        
                    }
                }
                status = "saved";
            }
            catch
            {
                status = "fail";
            }

            conn.Close();
            return status;

        }

        public OleDbConnection getConnection(string sysName)
        {
            // get saved system data
            string fileName = "";            
          
            SqlConnection conn = ApplicationConnectionManager.GetConnection();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM SysExcelFiles WHERE systemName = '" + sysName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    fileName = drd[1].ToString();                                  
                }

            }
            catch
            {
            }
            conn.Close();


            string con = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=2'";
            OleDbConnection NewConn = new OleDbConnection(con);
            return NewConn;
        }


        public DataTable getExcelData(string sysName, string sheetName, string columnName)
        {
            string conString = "";

            if (columnName == "")
            {
                conString = "SELECT * FROM [" + sheetName + "]";
            }
            else
            {
                conString = "SELECT " + columnName + " FROM [" + sheetName + "]";
            }
               
            //get connected to the existing system
            OleDbConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            DataTable dt = new DataTable("Data");
            try
            {
                OleDbCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = conString;

                OleDbDataAdapter sda = new OleDbDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch
            {
            }
            conn.Close();
            return dt;
        }

        public DataTable RunQuery(string sysName, string query)
        {
            //get connected to the existing system
            OleDbConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            DataTable dt = new DataTable("Data");
            try
            {
                OleDbCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = query;

                OleDbDataAdapter sda = new OleDbDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch
            {
            }
            conn.Close();
            return dt;
        }
    }
}
