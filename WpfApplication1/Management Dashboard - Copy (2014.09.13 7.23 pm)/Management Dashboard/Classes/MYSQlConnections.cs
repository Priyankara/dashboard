﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Management_Dashboard
{
    class MYSQlConnections
    {
        public List<List<string>> get_table_list(MySqlConnection NewConn,string database)
        {
            //get all tables
            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            List<string> table = new List<string>();
            List<List<string>> table_column = new List<List<string>>();

            try
            {
                MySqlCommand cmd = new MySqlCommand("show tables;");

                cmd.Connection = NewConn;               
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    table.Add(dr[0].ToString());
                }

                
            }
            catch (Exception ee)
            {
                NewConn.Close();
            }
            NewConn.Close();

                     

            //get all columns of all tables
            int length = table.Count;

            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            for (int a = 0; a < length; a++)
            {
                if (NewConn.State.ToString() == "Closed")
                {
                    NewConn.Open();
                }
                List<string> column_t = new List<string>();


                try
                {
                    MySqlCommand cmd2 = new MySqlCommand("select column_name from information_schema.columns where table_name = '"+table[a].ToString()+"' and table_schema='"+database+"'");

                    cmd2.Connection = NewConn;                    
                    MySqlDataReader dr = cmd2.ExecuteReader();

                    column_t.Add(table[a].ToString());

                    while (dr.Read())
                    {
                        column_t.Add(dr[0].ToString());
                    }

                   
                }
                catch (Exception ee)
                {

                }
                table_column.Add(column_t);
                NewConn.Close();

                          
            }

            NewConn.Close();
            return table_column;
        }

        public List<List<string>> test_MYSQLconnection(string server, string port, string database, string userName, string password)
        {
            MySqlConnection NewConn = null;
            
            List<List<string>> table_column = new List<List<string>>();

            NewConn = new MySqlConnection("server=" + server + "; uid=" + userName + ";pwd=" + password + "; database="+database+"; Port=" + port + ";Persist Security Info=True");

            try
            {
                NewConn.Open();
                table_column = get_table_list(NewConn,database);
                return table_column;

            }
            catch (Exception e)
            {
                return table_column;
            }
            if (NewConn.State.ToString() == "Opened")
            {
                NewConn.Close();
            }
            return table_column;

        }

        public string create_MYSQLconnection(string systemName, string server, string port, string catelogName, string userName, string password, List<List<string>> selections,int type)
        {
            //check system name(pk) exist.
            SqlConnection  conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem WHERE systemName = '" + systemName + "'";

                 SqlDataReader drd = Cmd.ExecuteReader();
                 if(drd.Read())
                 {
                     status = "exist";
                    conn.Close();
                    return status;
                 }
                                
            }
            catch
            {
                conn.Close();
                status = "fail";
                return status;
            }
            conn.Close();


            //save system
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {                
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                if (type == 1)
                {
                    newCmd.CommandText = "insert into ConectedSystem(systemName,DBName,serverName,username,password,port,type,activated)  values ('" + systemName + "','" + catelogName + "','" + server + "','" + userName + "','" + password + "','" + port + "','MYSQL',0)";
                }
                else if (type == 2)
                {
                    newCmd.CommandText = "insert into ConectedSystem(systemName,DBName,ip,port,username,password,type,activated)  values ('" + systemName + "','" + catelogName + "','" + server + "','" + port + "','" + userName + "','" + password + "','MYSQL',0)";
                }
               
                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch
            {
                status = "fail";
                return status;
            }

            conn.Close();

            //save tabels and columns
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                int table_count = selections.Count;
                for (int i = 0; i < table_count; i++)
                {
                    int column_count=selections[i].Count;
                    for (int j = 1; j < column_count; j++)
                    {
                        SqlCommand newCmd = conn.CreateCommand();
                        newCmd.Connection = conn;
                        newCmd.CommandType = CommandType.Text;
                        newCmd.CommandText = "insert into SysTableColumn(systemName,TableName,columnName)  values ('" + systemName + "','" + selections[i][0].ToString() + "','" + selections[i][j].ToString() + "')";

                        newCmd.ExecuteNonQuery();
                    }
                }
                status = "saved";
            }
            catch
            {
                status = "fail";
            }
            
            conn.Close();
            return status; 
        
        
        }

        public MySqlConnection getConnection(string sysName)
        {
            string systemName = "";
            string catelogName = "";
            string serverName = "";            
            string userName = "";
            string password = "";
            string serverIP = "";
            string port = "";

            SqlConnection conn = ApplicationConnectionManager.GetConnection();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            // get saved system data
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem WHERE systemName = '" + sysName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    systemName = drd[0].ToString();
                    catelogName = drd[1].ToString();
                    serverName = drd[2].ToString();
                    userName = drd[5].ToString();
                    password = drd[6].ToString();
                    serverIP = drd[7].ToString();
                    port = drd[8].ToString();
                }

            }
            catch
            {
            }
            conn.Close();

            //get connection to saved systems
            MySqlConnection NewConn = null;
            
            List<List<string>> table_column = new List<List<string>>();

            if (serverIP == "" || serverIP == null)
            {
                NewConn = new MySqlConnection("server=" + serverName + "; uid=" + userName + ";pwd=" + password + "; database=" + catelogName + "; Port=" + port + ";Persist Security Info=True");
            }
            else
            {
                NewConn = new MySqlConnection("server=" + serverIP + "; uid=" + userName + ";pwd=" + password + "; database=" + catelogName + "; Port=" + port + ";Persist Security Info=True");
            }

            return NewConn;

        }


        public DataTable getMYSQLData(string sysName, string tableName, string columnName)
        {
            string conString = "";
            if (columnName == "")
            {
                conString = "SELECT * FROM " + tableName + "";
            }
            else
            {
                conString = "SELECT " + columnName + " FROM " + tableName + "";
            }

            //get connected to the existing system
            MySqlConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            DataTable dt = new DataTable("Data");
            try
            {
                MySqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = conString;

                MySqlDataAdapter sda = new MySqlDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch
            {
            }
            conn.Close();
            return dt;
        }

        public DataTable RunQuery(string sysName, string query)
        {
            //get connected to the existing system
            MySqlConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            DataTable dt = new DataTable("Data");
            try
            {
                MySqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = query;

                MySqlDataAdapter sda = new MySqlDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch
            {
            }
            conn.Close();
            return dt;
        }
    }
}
