﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Management_Dashboard
{
    class SQLConnections
    {
        SqlConnection conn;

        public List<List<string>> get_table_list(SqlConnection NewConn)
        {
            //get all tables
            if (NewConn.State.ToString() == "Closed")
            {
                NewConn.Open();
            }

            List<string> table = new List<string>();
            List<List<string>> table_column = new List<List<string>>();

                try
                {
                    SqlCommand newCmd = NewConn.CreateCommand();
                    newCmd.Connection = NewConn;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables";

                    SqlDataReader dr = newCmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (dr[0].ToString() == "sysdiagrams")
                        {
                            continue;
                        }
                        table.Add(dr[0].ToString());
                    }
                }
                catch
                {
                }
                NewConn.Close();

                //get all columns of all tables
                int length = table.Count;

                if (NewConn.State.ToString() == "Closed")
                {
                    NewConn.Open();
                }

                for (int a = 0; a < length; a++)
                {
                    if (NewConn.State.ToString() == "Closed")
                    {
                        NewConn.Open();
                    }
                    List<string> column_t = new List<string>();
                    try
                    {
                        SqlCommand Cmd = NewConn.CreateCommand();
                        Cmd.Connection = NewConn;
                        Cmd.CommandType = CommandType.Text;
                        Cmd.CommandText = "SELECT COLUMN_NAME,* FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table[a].ToString() + "'";

                        SqlDataReader drd = Cmd.ExecuteReader();
                        column_t.Add(table[a].ToString());

                        while (drd.Read())
                        {
                            column_t.Add(drd[0].ToString());

                        }
                    }
                    catch
                    {
                    }
                    table_column.Add(column_t);
                    NewConn.Close();
                }
                
            
            NewConn.Close();
            return table_column;
        }

        public List<List<string>> test_SQLconnection(string serverName, string instanceName, string catelogName, string authenticationType, string userName, string password,int type)
        {
            SqlConnection NewConn=null;
            List<List<string>> table_column = new List<List<string>>();

            if (type == 1)
            {
                if (authenticationType == "Windows Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + " \\" + instanceName + "; Network Library=DBMSSOCN; Integrated Security=True;Encrypt=False");
                }
                else if (authenticationType == "SQL Server Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + " \\" + instanceName + "; Network Library=DBMSSOCN; User ID=" + userName + "; Password=" + password + "; Integrated Security=False;Encrypt=False");
                }
            }
            else if (type == 2)
            {
                if (authenticationType == "Windows Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + "," + instanceName + "; Network Library=DBMSSOCN; Integrated Security=False;Encrypt=False");
                }
                else if (authenticationType == "SQL Server Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + "," + instanceName + "; Network Library=DBMSSOCN; User ID=" + userName + "; Password=" + password + "; Integrated Security=False;Encrypt=False");
                }
            }

            try
            {
                NewConn.Open();                
                table_column=get_table_list(NewConn);                
                return table_column;
                
            }
            catch (Exception e)
            {
                return table_column;
            }
            if (NewConn.State.ToString() == "Opened")
            {
                NewConn.Close();
            }
            return table_column;

        }      

        public string create_SQLconnection(string systemName,string serverName, string instanceName, string catelogName, string authenticationType, string userName, string password, List<List<string>> selections, string serverIP, string instancePort,int type)
        {
            //check system name(pk) exist.
            conn = ApplicationConnectionManager.GetConnection();
            string status = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem WHERE systemName = '" + systemName + "'";

                 SqlDataReader drd = Cmd.ExecuteReader();
                 if(drd.Read())
                 {
                     status = "exist";
                    conn.Close();
                    return status;
                 }
                                
            }
            catch
            {
                conn.Close();
                status = "fail";
                return status;
            }
            conn.Close();


            //save system
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                if (type == 1)
                {
                    newCmd.CommandText = "insert into ConectedSystem(systemName,DBName,serverName,instanceName,authenticationType,username,password,type,activated)  values ('" + systemName + "','" + catelogName + "','" + serverName + "','" + instanceName + "','" + authenticationType + "','" + userName + "','" + password + "','SQL',0)";
                }
                else if (type == 2)
                {
                    newCmd.CommandText = "insert into ConectedSystem(systemName,DBName,ip,port,authenticationType,username,password,type,activated)  values ('" + systemName + "','" + catelogName + "','" + serverIP + "','" + instancePort + "','" + authenticationType + "','" + userName + "','" + password + "','SQL',0)";
                }


                newCmd.ExecuteNonQuery();
                status = "saved";
            }
            catch
            {
                status = "fail";
                return status;
            }

            conn.Close();

            //save tabels and columns
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                int table_count = selections.Count;
                for (int i = 0; i < table_count; i++)
                {
                    int column_count=selections[i].Count;
                    for (int j = 1; j < column_count; j++)
                    {
                        SqlCommand newCmd = conn.CreateCommand();
                        newCmd.Connection = conn;
                        newCmd.CommandType = CommandType.Text;
                        newCmd.CommandText = "insert into SysTableColumn(systemName,TableName,columnName)  values ('" + systemName + "','" + selections[i][0].ToString() + "','" + selections[i][j].ToString() + "')";

                        newCmd.ExecuteNonQuery();
                    }
                }
                status = "saved";
            }
            catch
            {
                status = "fail";
            }
            
            conn.Close();
            return status; 
        
        }

        public SqlConnection getConnection(string sysName)
        {
            // get saved system data
            string systemName = "";
            string catelogName = "";
            string serverName = "";
            string instanceName = "";            
            string authenticationType = "";
            string userName = "";
            string password = "";
            string serverIP = "";
            string port = "";

            conn = ApplicationConnectionManager.GetConnection();        

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM ConectedSystem WHERE systemName = '" + sysName + "'";

                SqlDataReader drd = Cmd.ExecuteReader();
                if (drd.Read())
                {
                    systemName = drd[0].ToString();
                    catelogName = drd[1].ToString();
                    serverName = drd[2].ToString();
                    instanceName = drd[3].ToString();
                    authenticationType = drd[4].ToString();
                    userName = drd[5].ToString();
                    password = drd[6].ToString();
                    serverIP = drd[7].ToString();
                    port = drd[8].ToString();                   
                }

            }
            catch
            {               
            }
            conn.Close();

            //get a connection to saved systems
            SqlConnection NewConn = null;
           // List<List<string>> table_column = new List<List<string>>();

            if (serverIP == "" || serverIP ==null)
            {
                if (authenticationType == "Windows Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + "\\" + instanceName + "; Network Library=DBMSSOCN; Integrated Security=True;Encrypt=False");
                }
                else if (authenticationType == "SQL Server Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverName + " \\" + instanceName + "; Network Library=DBMSSOCN; User ID=" + userName + "; Password=" + password + "; Integrated Security=False;Encrypt=False");
                }
            }
            else
            {
                if (authenticationType == "Windows Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverIP + "," + port + "; Network Library=DBMSSOCN; Integrated Security=False;Encrypt=False");
                }
                else if (authenticationType == "SQL Server Authentication")
                {
                    NewConn = new SqlConnection("Initial Catalog =" + catelogName + "; Data Source =" + serverIP + "," + port + "; Network Library=DBMSSOCN; User ID=" + userName + "; Password=" + password + "; Integrated Security=False;Encrypt=False");
                }
            }
           
            return NewConn;
        }

        public DataTable getSQLdata(string sysName, string tableName, string columnName)
        {
            string conString = "";
            if (columnName == "")
            {
                conString = "SELECT * FROM " + tableName + "";
            }
            else
            {
                conString="SELECT " + columnName + " FROM " + tableName + "";
            }

            //get connected to the existing system
            SqlConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            
            DataTable dt = new DataTable("Data");
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = conString;
                
                SqlDataAdapter sda = new SqlDataAdapter(Cmd);                
                sda.Fill(dt);
             }
            catch
            {                
            }
            conn.Close();
            return dt;
        }

        public DataTable RunQuery(string sysName, string query)
        {
            //get connected to the existing system
            SqlConnection conn = getConnection(sysName);
            List<string> data = new List<string>();

            //read data
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();               
            }

            DataTable dt = new DataTable("Data");
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = query;

                SqlDataAdapter sda = new SqlDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch
            {              
            }
            conn.Close();
            return dt;
        }


    }
}
