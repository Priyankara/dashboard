﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management_Dashboard
{
    class WaitThread
    {
        Thread newWindowThread;
        
        public void WaitWindowShow()
        {
            newWindowThread = new Thread((ThreadStart));
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
            
        }

        public void ThreadStart()
        {         
            WaitingWindow ww = new WaitingWindow();
            ww.Show();
            System.Windows.Threading.Dispatcher.Run();
            ww.Close();            
        }

        public void ThreadAbort()
        {
            newWindowThread.Abort(); 
        }

    }
}
