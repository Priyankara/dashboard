﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Loginnw.login;

namespace WpfToolkitChart.Login
{
    /// <summary>
    /// Interaction logic for ChangePasswordDefault.xaml
    /// </summary>
    public partial class ChangePasswordDefault : Window
    {
        private readonly SqlConnection conn;
        DBAccess dba = new DBAccess();
        public ChangePasswordDefault()
        {
            InitializeComponent();
            conn = ConnectionManager.GetConnection();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string guidval = Guid.NewGuid().ToString();

            if (TextBox2.Text.Length == 0 || TextBox1.Text.Length == 0)
            {
                TextBox1.Text = "Enter User Name.";

                TextBox2.Text = "Enter an email.";
                TextBox2.Focus();
            }
            else if (
                !Regex.IsMatch(TextBox2.Text,
                    @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                TextBox2.Text = "Enter a valid email.";
                //     TextBox2.
                TextBox2.Select(0, TextBox2.Text.Length);
                TextBox2.Focus();
            }
            else
            {
                

            try
            {
             
                dba.SendRegistrationEmail(TextBox2.Text, TextBox1.Text, guidval);
            
                SqlCommand cmd = new SqlCommand(" Update tblUsers set Password = '" + guidval + "' where  UserName = '" + TextBox1.Text + "' ", conn);
                cmd.CommandType = CommandType.Text;
                //   newCmd.CommandText = " Update tblUsers set Password = '"+ guidval+ "' where  UserName = '"+ TextBox1.Text+"' ";
                //    newCmd.CommandText = "insert into USER_VIEWS values('" + username + "','" + views[i].ToString() + "')";
                conn.Open();
                cmd.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
               // MessageBox.Show("SQL ERROR");
            }
            finally
            {
                // The finally block is guarenteed to execute even if there is an exception.
                //  This ensures connections are always properly closed.
                conn.Close();
            }

            }
           
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
