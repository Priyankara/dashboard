﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Loginnw.login
{
    /// <summary>
    /// Interaction logic for Change_Password.xaml
    /// </summary>
    public partial class Change_Password : Window
    {
        DBAccess dba = new DBAccess();
        private readonly SqlConnection conn;
        public Change_Password()
        {
            InitializeComponent();
            conn = ConnectionManager.GetConnection();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (txt_pw.Password != txt_cpw.Password)
            {
                MessageBox.Show(" passwords do not match");
            }

            else if (dba.validate_changepw(txt_name.Text, txt_confirmation.Text ) == false)
            {
                MessageBox.Show(" Invalid Authentication Code");

            }
            else
            {
               // dba.validate_changepw(txt_name.Text, txt_confirmation.Text);
                try
                {
                    SqlCommand cmd = new SqlCommand(" Update tblUsers set Password = '" + txt_cpw.Password + "' where  UserName = '" + txt_name.Text + "' ", conn);
                    cmd.CommandType = CommandType.Text;
                    //   newCmd.CommandText = " Update tblUsers set Password = '"+ guidval+ "' where  UserName = '"+ TextBox1.Text+"' ";
                    //    newCmd.CommandText = "insert into USER_VIEWS values('" + username + "','" + views[i].ToString() + "')";
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Password Changed Successfully ");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Password Changed UnSuccessfully ");
                }
                finally
                {

                    conn.Close();
                }


            }
        
     
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

     
    }
}
