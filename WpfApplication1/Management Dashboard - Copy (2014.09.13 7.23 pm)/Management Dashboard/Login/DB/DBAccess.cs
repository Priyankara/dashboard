﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms.VisualStyles;
using Management_Dashboard;

namespace Login
{
    class DBAccess
    {
        SqlConnection conn;

        public DBAccess()
        {
            conn = ConnectionManager.GetConnection();
        }

        public List<string> loadViews()
        {
            List<string> views=new List<string>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }


            SqlCommand newCmd = new SqlCommand("select viewName from DashBoardView", conn);

            SqlDataReader dr = newCmd.ExecuteReader();
        
            while (dr.Read())
            {
                views.Add(dr[0].ToString());
            }
            dr.Close();
            return views;
        }

        //add (This is temp method for testing purposes)
        public bool add(string name, string pw, string type)
        {
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            SqlCommand newCmd = conn.CreateCommand();

            newCmd.Connection = conn;
            newCmd.CommandType = CommandType.Text;
            newCmd.CommandText = "insert into dbo.LoginTable(UsrName, Password, UsrType) values ('" + name + "', '" + pw + "','"+ type +"')";

            newCmd.ExecuteNonQuery();

            status = true;
            conn.Close();

            return status;

        }

        //Login redirection
        public int getLog(string name, string pw, string type)
        {
           
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
           

            SqlCommand newCmd = new SqlCommand("select UsrName,Password,UsrType from dbo.LoginTable where UsrName='" + name + "' and Password='" + pw + "' and UsrType='" + type + "'", conn);

            SqlDataReader dr = newCmd.ExecuteReader();
            String un="";
            String p="";
            String t="";

            while (dr.Read())
            {
                un = dr["UsrName"].ToString();
                p = dr["Password"].ToString();
                t = dr["UsrType"].ToString();          
                
            }
            dr.Close();

            if (un.Equals(name) && p.Equals(pw) && t.Equals("Admin"))
            {
                conn.Close(); 
                return 1;
            }

            else if (un.Equals(name) && p.Equals(pw) && t.Equals("Manager"))
            {
                conn.Close(); 
                return 2;
            }

            else
            {
                conn.Close(); 
                return 3;
            }
    
        }

        //Load dataGrid
        public DataTable FillGrid()
        {
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

                SqlCommand cmd = new SqlCommand("select ViewDash as Dashboard_Views from dbo.Temp", conn);
               
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("Temp");
                sda.Fill(dt);
                return dt;
                
            
        }

        //Register user
        public bool register(string Uname,string password, string type, string views)
        {
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            SqlCommand newCmd = conn.CreateCommand();

            newCmd.Connection = conn;
            newCmd.CommandType = CommandType.Text;
            newCmd.CommandText = "insert into dbo.Login(UsrName, Password, UsrType,DView) values ('" + Uname + "', '" + password + "','" + type + "', '" + views + "')";

            newCmd.ExecuteNonQuery();

            status = true;
            conn.Close();

            return status;

        }

        //ValidateUser
        public bool validateAdmin(string name, string pw)
        {

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }


            SqlCommand newCmd = new SqlCommand("select UsrName,Password,UsrType from dbo.Login where UsrName='" + name + "' and Password='" + pw + "'", conn);

            SqlDataReader dr = newCmd.ExecuteReader();
            String un = "";
            String p = "";
            
            while (dr.Read())
            {
                un = dr["UsrName"].ToString();
                p = dr["Password"].ToString();
            }
            dr.Close();

            if (un.Equals(name) && p.Equals(pw))
            {
                conn.Close();
                return true;
            }

            else
            {
                conn.Close();
                return false;
            }

        }

        
        //change and update Admin password
        public bool changePW(string cname, string name, string pw, string email, string epw)
        {
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            SqlCommand newCmd1 = conn.CreateCommand();

            newCmd1.Connection = conn;
            newCmd1.CommandType = CommandType.Text;
            newCmd1.CommandText = "UPDATE dbo.Login SET UsrName='" + name + "', Password='" + pw + "', AdminEmail='" + email + "', AdminEPW='" + epw + "' WHERE UsrName='" + cname + "' ";

            newCmd1.ExecuteNonQuery();

            status = true;
            conn.Close();

            return status;
        }

        //get network credentials
        public Tuple<string,string> getEmailDetails(string adminName)
        {

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }


            SqlCommand newCmd3 = new SqlCommand("select AdminEmail,AdminEPW from dbo.Login where UsrName='" + adminName + "'", conn);

            SqlDataReader dr = newCmd3.ExecuteReader();
            String mail = "";
            String epw = "";

            while (dr.Read())
            {
                mail = dr["AdminEmail"].ToString();
                epw = dr["AdminEPW"].ToString();
            }
            dr.Close();

            conn.Close();

            return new Tuple<string, string>(mail, epw);
        }

        //change manager password changeManagerPW
        public bool changeManagerPW(string UsrName, string pw)
        {
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            SqlCommand newCmd1 = conn.CreateCommand();

            newCmd1.Connection = conn;
            newCmd1.CommandType = CommandType.Text;
            newCmd1.CommandText = "UPDATE dbo.Login SET Password='" + pw + "' WHERE UsrName='" + UsrName + "' ";

            newCmd1.ExecuteNonQuery();

            status = true;
            conn.Close();

            return status;
        }

        public List<string> getUsers()
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            List<string> user_ = new List<string>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM tblUsers where UserType='User' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    user_.Add(drd[1].ToString());
                }

            }
            catch
            {
                conn.Close();
                return null;
            }
            conn.Close();
            return user_;
        }

        public bool getUserViews(string username, string viewname )
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            bool status=false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT * FROM USER_VIEWS where USERNAME='" + username + "' and VIEWNAME='" + viewname + "' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    status = true;
                }

            }
            catch
            {
                conn.Close();
                return status;
            }
            conn.Close();
            return status;
        }

        public bool UpdateUser(string username, List<string> views)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            try
            {
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "delete from USER_VIEWS where USERNAME='" + username + "'";

                newCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch
            {
                conn.Close();
            }

            if (views.Count == 0)
            {
                status = true;
                return status;
            }

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            for (int i = 0; i < views.Count; i++)
            {
                try
                {
                    SqlCommand newCmd = conn.CreateCommand();
                    newCmd.Connection = conn;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "insert into USER_VIEWS values('" + username + "','" + views[i].ToString() +
                                         "')";

                    newCmd.ExecuteNonQuery();
                    status = true;
                }
                catch
                {
                    conn.Close();
                    return status;
                }
            }

            conn.Close();
            return status;
        }

        
    }
}
