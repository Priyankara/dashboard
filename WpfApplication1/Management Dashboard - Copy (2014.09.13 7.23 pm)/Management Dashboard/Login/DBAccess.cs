﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Management_Dashboard;

namespace Loginnw.login
{
    internal class DBAccess
    {
        private readonly SqlConnection conn;

        public DBAccess()
        {
            conn = ConnectionManager.GetConnection();
        }

        public List<string> loadViews()
        {
            List<string> views = new List<string>();

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }


            SqlCommand newCmd = new SqlCommand("select viewName from DashBoardView", conn);

            SqlDataReader dr = newCmd.ExecuteReader();

            while (dr.Read())
            {
                views.Add(dr[0].ToString());
            }
            dr.Close();
            return views;
        }

        public bool AuthenticateUser(string username, string password)
        {
            // ConfigurationManager class is in System.Configuration namespace
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            var cmd = new SqlCommand("spAuthenticateUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            // FormsAuthentication is in System.Web.Security
            //string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            // SqlParameter is in System.Data namespace
            var paramUsername = new SqlParameter("@UserName", username);
            var paramPassword = new SqlParameter("@Password", password);

            cmd.Parameters.Add(paramUsername);
            cmd.Parameters.Add(paramPassword);


            var returnCode = (int) cmd.ExecuteScalar();
            return returnCode == 1;
        }

        //Login redirection
        public int getLog(String name)
        {
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }


            var newCmd = new SqlCommand("select UserType from dbo.tblUsers where UserName='" + name + "' ", conn);

            SqlDataReader dr = newCmd.ExecuteReader();
            // newCmd.ExecuteReader();

            String t = "";

            while (dr.Read())
            {
                t = dr["UserType"].ToString();
            }
            dr.Close();

            if (t.Equals("Admin"))
            {
                conn.Close();
                return 1;
            }

            if (t.Equals("Manager"))
            {
                conn.Close();
                return 2;
            }

            conn.Close();
            return 3;
            // return 1;
        }

        public bool AddUser(string username, string email, string userType, string guid, List<string> views)
        {
            // ConfigurationManager class is in System.Configuration namespace
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            var cmd = new SqlCommand("spAAddUser", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            // FormsAuthentication is in System.Web.Security
            //string EncryptedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            // SqlParameter is in System.Data namespace
            var paramUsername = new SqlParameter("@UserName", username);
            var paramPassword = new SqlParameter("@Email", email);
            var usertype = new SqlParameter("@UserType", userType);
            var guidval = new SqlParameter("@Password", guid);

            cmd.Parameters.Add(paramUsername);
            cmd.Parameters.Add(paramPassword);
            cmd.Parameters.Add(usertype);
            cmd.Parameters.Add(guidval);



            var returnCode = (int)cmd.ExecuteNonQuery();

            for (int i = 0; i < views.Count; i++)
            {
                try
                {
                    SqlCommand newCmd = conn.CreateCommand();
                    newCmd.Connection = conn;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "insert into USER_VIEWS values('" + username + "','" + views[i].ToString() + "')";

                    newCmd.ExecuteNonQuery();
                }
                catch
                {

                }
            }

            if (returnCode > 0)
            {

                return returnCode == 1;
            }
            return returnCode == -1;
        }


        public void SendRegistrationEmail(string toEmail, string userName, string guidval)
        {
            MailMessage mailMessage=null;
            // MailMessage class is present is System.Net.Mail namespace
            try
            {
                mailMessage = new MailMessage("sliit2014sepwe06@gmail.com", toEmail);
                // StringBuilder class is present in System.Text namespace
                StringBuilder sbEmailBody = new StringBuilder();
                sbEmailBody.Append("Dear " + userName + ",<br/><br/>");
                sbEmailBody.Append("Please follow instructions to reset your password");
                sbEmailBody.Append("<br/>");
                sbEmailBody.Append("Your auto generaed code is " + guidval + ". Please use this code to log in to your account");


                sbEmailBody.Append("<br/><br/>");
                sbEmailBody.Append("<b>DMS Technologies</b>");

                mailMessage.IsBodyHtml = true;

                mailMessage.Body = sbEmailBody.ToString();
                mailMessage.Subject = "Reset Your Password";
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                smtpClient.Credentials = new System.Net.NetworkCredential()
                {
                    UserName = "sliit2014sepwe06@gmail.com",
                    Password = "sliit@me/"
                };

                smtpClient.EnableSsl = true;
                smtpClient.Send(mailMessage);

                MessageBox.Show(" Email has been sent to Users account Successfully ");
            }
            catch (Exception e)
            {
                MessageBox.Show(" Email could not send.  Please check network connectivity ");
             //  MessageBox.Show(e.ToString());
            }
            


           
        }

        public bool validate_changepw(string name, string code)
        {



            if (name == "" & code == "")
            {
                MessageBox.Show("innvalid Inputs");
                return false;
            }
            else
            {
                string queryText = "SELECT Count(*) FROM tblUsers WHERE UserName = '" + name + "' AND Password = '" + code + "' ";

                SqlCommand cmd = new SqlCommand(queryText, conn);
                cmd.CommandType = CommandType.Text;
                //   newCmd.CommandText = " Update tblUsers set Password = '"+ guidval+ "' where  UserName = '"+ TextBox1.Text+"' ";
                //    newCmd.CommandText = "insert into USER_VIEWS values('" + username + "','" + views[i].ToString() + "')";
                conn.Open();
                cmd.ExecuteNonQuery();
                //  conn.Open();
                //   conn.Parameters.AddWithValue("@Username", name);  // cmd is SqlCommand 
                //   cmd.Parameters.AddWithValue("@Password", code);
                int result = (int)cmd.ExecuteScalar();
                if (result > 0)
                    return true;
                else
                    return false;
            }
           

            

        }

        public string getUserType(string username, string password)
        {
            SqlConnection conn = ApplicationConnectionManager.GetConnection();
            string usertype = "";

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT UserType FROM tblUsers where UserName='" + username + "' and Password='" + password + "' ";

                SqlDataReader drd = Cmd.ExecuteReader();

                while (drd.Read())
                {
                    usertype = drd[0].ToString();
                }

            }
            catch
            {
                conn.Close();
                return usertype;
            }
            conn.Close();
            return usertype;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       
      
    }
}