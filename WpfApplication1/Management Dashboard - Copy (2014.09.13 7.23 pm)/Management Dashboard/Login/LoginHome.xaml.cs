﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Loginnw.login;
using Management_Dashboard;
using WpfToolkitChart.Login;

namespace Loginnw.login
{
    /// <summary>
    /// Interaction logic for LoginHome.xaml
    /// </summary>

    public partial class LoginHome : Window
    {
        DBAccess dba = new DBAccess();

        public LoginHome()
        {
            InitializeComponent();
            
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (txtUserName.Text.Length == 0 || txtPassword.Password.Length == 0)
            {
                MessageBox.Show("Plese Enter username and Password");
            }
            else
            {


                if (txtPassword.Password.Length != 36)
                {
                    if (dba.AuthenticateUser(txtUserName.Text, txtPassword.Password))
                    {
                        // FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, chkBoxRememberMe.Checked);
                        //  MessageBox.Show("ok");

                        string userType = "";
                        userType = dba.getUserType(txtUserName.Text, txtPassword.Password);

                        if (userType=="Admin")
                        {
                            //  MessageBox.Show("Admin ");
                            AdminMain lc = new AdminMain("Admin");
                            lc.Show();
                            this.Close();
                        }
                        else if (userType == "Manager")
                        {
                            AdminMain lc = new AdminMain("Manager");
                            lc.Show();
                            this.Close();
                            //MessageBox.Show("Manager login");
                          //  DashBoardWindow dbw = new DashBoardWindow(txtUserName.Text.ToString(), 2);
                           // dbw.Show();
                        }
                        else
                        {
                            DashBoardWindow dbw = new DashBoardWindow("User",txtUserName.Text.ToString());
                            dbw.Show();
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid User Name or Password");


                    }

                }
                else
                {
                    if (dba.AuthenticateUser(txtUserName.Text, txtPassword.Password))
                    {
                        // FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, chkBoxRememberMe.Checked);
                        //  MessageBox.Show("ok");

                        Change_Password cp = new Change_Password();
                        cp.Show();


                    }
                    else
                    {
                        MessageBox.Show("Invalid code");

                    }


                }

            }

        }

        private void CheckBox1_Checked(object sender, RoutedEventArgs e)
        {
           
           ChangePasswordDefault cp = new ChangePasswordDefault();
            cp.Show();

               
            
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
