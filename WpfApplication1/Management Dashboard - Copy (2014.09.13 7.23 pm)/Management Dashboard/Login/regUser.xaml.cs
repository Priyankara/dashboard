﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Loginnw.login
{
    /// <summary>
    /// Interaction logic for regUser.xaml
    /// </summary>
    public partial class regUser : Window
    {
        DBAccess dba = new DBAccess();
        public regUser()
        {
            InitializeComponent();
            loadViews();
        }

        public void loadViews()
        {
            List<string> views = new List<string>();
            views= dba.loadViews();

            for (int b = 0; b < views.Count; b++)
            {
                TreeViewItem treeItem = new TreeViewItem();// for tabel name 
                String View = views[b].ToString();
                CheckBox cbt = new CheckBox();
                cbt.Content = View;
               // cbt.Checked += cbt_Checked;
               // cbt.Unchecked += cbt_UnChecked;
                treeItem.Header = cbt;
                tree_views.Items.Add(treeItem);
            }
        }

   
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (TextBox2.Text.Length == 0 || TextBox1.Text.Length == 0)
            {
                TextBox2.Text = "Enter an email.";
                TextBox2.Text = "Please Enter User Name";
                TextBox2.Focus();
            }
            else if (
                !Regex.IsMatch(TextBox2.Text,
                    @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                TextBox2.Text = "Enter a valid email.";
           //     TextBox2.
                TextBox2.Select(0, TextBox2.Text.Length);
                TextBox2.Focus();
            }
            else
            {


                string guidval = Guid.NewGuid().ToString();

                List<string> views_ = new List<string>();

                foreach (TreeViewItem tv in tree_views.Items)
                {
                    CheckBox cb = (CheckBox) (tv.Header);

                    if (cb.IsChecked == true)
                    {
                        views_.Add(cb.Content.ToString());
                    }
                }

                if (dba.AddUser(TextBox1.Text, TextBox2.Text, ComboBox1.Text, guidval, views_))
                {
                    // FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, chkBoxRememberMe.Checked);
                    //  MessageBox.Show("ok");


                    {
                        MessageBox.Show(" user registered Successfully");
                    }
                }
                else
                {
                    MessageBox.Show(" Error occured ");
                }

                String val = TextBox1.Text;

                try
                {
                    dba.SendRegistrationEmail(TextBox2.Text, TextBox1.Text, guidval);
                }
                catch
                {
                }

            }
        }

        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
      
    }

   
}
