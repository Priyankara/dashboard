﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for AddNewConnection.xaml
    /// </summary>
    public partial class AddNewConnection : Page
    {
        public AddNewConnection()
        {
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //theard open and waiting window show
            WaitThread wt = new WaitThread();
            wt.WaitWindowShow();
      
            if (cmb_connType.SelectedIndex == 0)
            {
                //sql
                CreateSQLConnections CSQLC = new CreateSQLConnections();
                frm_conType.Content = CSQLC;
            }
            else if (cmb_connType.SelectedIndex == 1)
            {
                //my sql
                CreateMYSQLConnection CmsQLC = new CreateMYSQLConnection();
                frm_conType.Content = CmsQLC;
            }
            else if (cmb_connType.SelectedIndex == 2)
            {
                //excel
                CreateExcelConnection CEC = new CreateExcelConnection();
                frm_conType.Content = CEC;
            }

            wt.ThreadAbort();            
        } 
    }
}
