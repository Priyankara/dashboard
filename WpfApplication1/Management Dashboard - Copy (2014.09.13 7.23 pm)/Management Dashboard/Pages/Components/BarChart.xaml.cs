﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for BarChart.xaml
    /// </summary>
   
    partial class BarChart : Page
    {
        public string chartName="";
        public int height = 0;
        public int width = 0;
        public int barWidth = 0;
        public string bar_Color = "";
        public string back_Color = "";
        public Color barColor = Colors.Blue;
        public int refreshPeriod = 0;
        public string xAxis = "";
        public string yAxis = "";
        public List<string[]> multipleBars = new List<string[]>();
        public string singelQueryName = "";
        int type = 0;
        public string chart_description = "";
        
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        SQLConnections sqlcon = new SQLConnections();
        MYSQlConnections mysqlcon = new MYSQlConnections();
        ExcelConnections excelcon = new ExcelConnections();
        List<string[]> queries = new List<string[]>();

        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();

        public BarChart()
        {
            InitializeComponent();    

            try
            {
                queries = ACM.getQueries();
            }
            catch { }    
      
            Timer.Tick += new EventHandler(Timer_Tick);      
        }

        public void StopUpdate()
        {
            try
            {
                Timer.Stop();
            }
            catch { }
        }
        public void StartUpdate()
        {
            if(this.Timer != null)
            {
                this.Timer.Start();
            }         
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            grd_barChart.Children.Clear();
            generateChart(chartName, width, height, barWidth, bar_Color, back_Color, refreshPeriod, xAxis, yAxis, multipleBars, singelQueryName, type, chart_description);            
        }

        public void generateChart(string chart_name, DataTable dt, string component_type, string x, string y)
        {
            barWidth = 20;

            lbl_chartName.Content = chart_name;
            lbl_x.Content = y;
            lbl_y.Content = x;

            lbl_key.Background = new SolidColorBrush(Colors.LightBlue);
            //  lbl_x.MouseEnter += lbl_x_MouseEnter;
            lbl_key.Content = "Key";
            lbl_value.Background = new SolidColorBrush(Colors.LightBlue);
            //  lbl_y.MouseEnter += lbl_x_MouseEnter;
            lbl_value.Content = "Value";

            lbl_deleteX.Background = new SolidColorBrush(Colors.White);
            lbl_deleteY.Background = new SolidColorBrush(Colors.White);
            lbl_deleteX.Content = "X";
            lbl_deleteY.Content = "X";
            // lbl_deleteX.MouseLeftButtonDown += lbl_deletex_MouseLeftButtonDown;
            // lbl_deleteY.MouseLeftButtonDown += lbl_deletey_MouseLeftButtonDown;

            if (x == "" || x == null)
            {
                lbl_x.Content = lbl_key.Content.ToString(); ;
            }
            else
            {
                lbl_x.Content = x;
            }

            if (y == "" || y == null)
            {
                lbl_y.Content = lbl_value.Content.ToString(); ;
            }
            else
            {
                lbl_y.Content = y;
            }

            try
            {

                double max = 0;
                double rate = 1;
                DataTable data_table = new DataTable();
                data_table = dt;

                double chartWidth = 600;
                chartWidth = ((barWidth + 10) * data_table.Rows.Count) + 100; 
                this.Width = chartWidth + 200;
                line_x.Width = chartWidth;
                grid_scale.Width = chartWidth;

                lbl_x.Margin = new Thickness(chartWidth + 50, 390, 0, 0);

                Random r = new Random();

                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    double v = 0;
                    try
                    {
                        v = Double.Parse(data_table.Rows[i][1].ToString());
                    }
                    catch { }

                    if (v > max)
                    {
                        max = v;
                    }
                }

                rate = 310.0 / ((Math.Ceiling((Double)(max / 10.0))) * 10);
                setScale(((Math.Ceiling((Double)(max / 10.0))) * 10));

                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    colDef1.Width = new GridLength(barWidth + 10);
                    grd_barChart.ColumnDefinitions.Add(colDef1);

                    Label col = new Label();
                    col.Width = barWidth;
                    col.Height = 0;

                    try
                    {
                        col.Height = (Double.Parse(data_table.Rows[i][1].ToString())) * rate;
                    }
                    catch { }

                    col.Content = data_table.Rows[i][1].ToString();

                    barColor = new Color() { R = (Byte)r.Next(0, 256), B = (Byte)r.Next(0, 256), G = (Byte)r.Next(0, 256), A = 128 };
                    col.Background = new SolidColorBrush(barColor);
                    col.VerticalAlignment = VerticalAlignment.Bottom;

                    Label col_name = new Label();
                    col_name.Content = data_table.Rows[i][0].ToString();
                    col_name.VerticalAlignment = VerticalAlignment.Top;

                    grd_barChart.Children.Add(col);
                    Grid.SetRow(col, 0);
                    Grid.SetColumn(col, i);

                    grd_barChart.Children.Add(col_name);
                    Grid.SetRow(col_name, 1);
                    Grid.SetColumn(col_name, i);
                }
            }
            catch { }
        }

        public void generateChart(string chart_name, int width_, int height_, int bar_width, string bar_color, string back_color, int refresh_period, string x_axis, string y_axis, List<string[]> multiple_bars, string singel_uery_ame, int type_, string description)
        {
            double max = 0;
            double rate = 1;
            
            chartName = chart_name;
            height = height_;
            width = width_;
            barWidth = bar_width;
            bar_Color = bar_color;
            back_Color = back_color;
            refreshPeriod = refresh_period;
            type = type_;
            chart_description = description;

            xAxis = x_axis;
            yAxis = y_axis;
            multipleBars = multiple_bars;
            singelQueryName = singel_uery_ame;

            if (bar_Color == "Red")
            {
                barColor = Colors.Red;
            }
            else if (bar_Color == "Blue")
            {
                barColor = Colors.Blue;
            }

            lbl_chartName.Content = chartName;

            if (type==1)
            {
                max = 0;
                double chartWidth = 600;
                chartWidth = ((barWidth + 10) * multipleBars.Count) + 100; ;
                this.Width = chartWidth+200;
                line_x.Width = chartWidth;
                grid_scale.Width = chartWidth;

                lbl_x.Margin = new Thickness(chartWidth+50, 390, 0, 0);
                lbl_x.Content = xAxis;
                lbl_y.Content = yAxis;


                for (int i = 0; i < multipleBars.Count; i++)
                {
                    double Barheight = 0;
                    Barheight = getHeight(multipleBars[i][1].ToString());

                    if (Barheight > max)
                    {
                        max = Barheight;
                    }
                }

                rate = 310.0/((Math.Ceiling((Double)(max/10.0)))*10);
                setScale(((Math.Ceiling((Double)(max / 10.0))) * 10));
               
                Random r = new Random();

                for (int i = 0; i < multipleBars.Count; i++)
                {                    
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    colDef1.Width = new GridLength(barWidth+10);
                    grd_barChart.ColumnDefinitions.Add(colDef1);

                    Label col = new Label();
                    col.Width = barWidth;
                    col.Height = (getHeight(multipleBars[i][1].ToString())) * rate; ;
                  
                    if (bar_Color == "Multiple")
                    {
                        
                        barColor = new Color() { R = (Byte)r.Next(0, 256), G = (Byte)r.Next(0, 256), B = (Byte)r.Next(0, 256), A = 128 };
                    }
                    col.Background = new SolidColorBrush(barColor);
                    col.VerticalAlignment = VerticalAlignment.Bottom;

                    Label col_name = new Label();
                    col_name.Content = multipleBars[i][0].ToString();
                    col.VerticalAlignment = VerticalAlignment.Bottom;

                    grd_barChart.Children.Add(col);
                    Grid.SetRow(col, 0);
                    Grid.SetColumn(col, i);

                    grd_barChart.Children.Add(col_name);
                    Grid.SetRow(col_name, 1);
                    Grid.SetColumn(col_name, i); 
                }
                
            }
            else if (type == 2)
            {
                max = 0;
                rate = 1;
                DataTable data_table=new DataTable();
                data_table = getDataTable(singelQueryName);
                
                double chartWidth = 600;
                chartWidth = ((barWidth + 10) * data_table.Rows.Count) + 100; ;
                this.Width = chartWidth + 200;
                line_x.Width = chartWidth;
                grid_scale.Width = chartWidth;

                lbl_x.Margin = new Thickness(chartWidth + 50, 390, 0, 0);
                lbl_x.Content = data_table.Columns[0].ToString();
                lbl_y.Content = data_table.Columns[1].ToString();
                
                Random r = new Random();

                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    double v = 0;   
                    try
                    {
                        v = Double.Parse(data_table.Rows[i][1].ToString());
                    }
                    catch { }
                    
                    if (v > max)
                    {
                        max = v;
                    }
                }
                
                rate = 310.0 / ((Math.Ceiling((Double)(max / 10.0))) * 10);
                setScale(((Math.Ceiling((Double)(max / 10.0))) * 10));
                
                for(int i=0;i<data_table.Rows.Count;i++)
                {
                    ColumnDefinition colDef1 = new ColumnDefinition();
                    colDef1.Width = new GridLength(barWidth+10);
                    grd_barChart.ColumnDefinitions.Add(colDef1);
                   
                    Label col = new Label();
                    col.Width = barWidth;
                    col.Height = 0;
                    
                    try
                    {
                        col.Height = (Double.Parse(data_table.Rows[i][1].ToString())) * rate;
                    }
                    catch {  }

                    col.Content = data_table.Rows[i][1].ToString();

                    if (bar_Color == "Multiple")
                    {                        
                        barColor = new Color() { R = (Byte)r.Next(0, 256),B = (Byte)r.Next(0, 256), G = (Byte)r.Next(0, 256), A=128 };
                    }
                    col.Background = new SolidColorBrush(barColor);
                    col.VerticalAlignment = VerticalAlignment.Bottom;

                    Label col_name = new Label();
                    col_name.Content = data_table.Rows[i][0].ToString();
                    col_name.VerticalAlignment = VerticalAlignment.Top;
               
                    grd_barChart.Children.Add(col);
                    Grid.SetRow(col, 0);
                    Grid.SetColumn(col, i);

                    grd_barChart.Children.Add(col_name);
                    Grid.SetRow(col_name, 1);
                    Grid.SetColumn(col_name, i); 
                }
            }

            if (refreshPeriod != 0)
            {
                Timer.Interval = new TimeSpan(0, 0, refreshPeriod);
                StartUpdate();               
            }
            else
            {
                Timer = null;
            }            
        }

        public void setScale(double scaleMax)
        {
            double r = scaleMax / 10;

            lbl_1.Content = (1 * r).ToString();
            lbl_2.Content = (2 * r).ToString();
            lbl_3.Content = (3 * r).ToString();
            lbl_4.Content = (4 * r).ToString();
            lbl_5.Content = (5 * r).ToString();
            lbl_6.Content = (6 * r).ToString();
            lbl_7.Content = (7 * r).ToString();
            lbl_8.Content = (8 * r).ToString();
            lbl_9.Content = (9 * r).ToString();
            lbl_10.Content = (10 * r).ToString();

        }

        public DataTable getDataTable(string queryName)
        {
            string queryType = "";
            string query = "";
            string systemName = "";
            DataTable dt = new DataTable();

            for(int i=0;i<queries.Count;i++)
            {
                if(queryName==queries[i][0].ToString())
                {
                    queryType = queries[i][2].ToString();
                    query = queries[i][1].ToString();
                    systemName = queries[i][3].ToString();
                    break;
                }
            }

            if (queryType=="SQL")
            {
                dt=sqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "MYSQL")
            {
                dt= mysqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "Excel")
            {
                dt = excelcon.RunQuery(systemName, query);
            }
            return dt;
        }

        public double getHeight(string queryName)
        {
            double height = 0;
            string queryType = "";
            string query = "";
            string systemName = "";
            DataTable dt = new DataTable();

            for(int i=0;i<queries.Count;i++)
            {
                if(queryName==queries[i][0].ToString())
                {
                    queryType = queries[i][2].ToString();
                    query = queries[i][1].ToString();
                    systemName = queries[i][3].ToString();
                    break;
                }
            }

            if (queryType=="SQL")
            {
                dt=sqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "MYSQL")
            {
                dt= mysqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "Excel")
            {
                dt = excelcon.RunQuery(systemName, query);
            }

            try
            {
                height = Double.Parse( dt.Rows[0][0].ToString());
            }
            catch
            {
                
                height = 0;
            }
           
            return height;
        }
		
    }
}
