﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard.Pages.Components
{
    /// <summary>
    /// Interaction logic for ComponentFram.xaml
    /// </summary>
    partial class ComponentFram : Page
    {
        public string chartName = "";
        public int height = 0;
        public int width = 0;
        public string back_Color = "";
        public int refreshPeriod = 0;
        public List<string[]> multipleQueries = new List<string[]>();
        public string singelQueryName = "";
        int type = 0;
        public string componentType = "";
        public string Xaxis = "";
        public string Yaxis = "";
        public string chart_description = "";

        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        SQLConnections sqlcon = new SQLConnections();
        MYSQlConnections mysqlcon = new MYSQlConnections();
        ExcelConnections excelcon = new ExcelConnections();
        List<string[]> queries = new List<string[]>();
        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();

        public ComponentFram(string type)
        {
            InitializeComponent();
            try
            {
                queries = ACM.getQueries();
            }
            catch { }

            Timer.Tick += new EventHandler(Timer_Tick);

            componentType = type;              
        }

        public void StopUpdate()
        {
            try
            {
                Timer.Stop();
            }
            catch { }
        }
        public void StartUpdate()
        {
            if (this.Timer != null)
            {
                this.Timer.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            generateChart(chartName, width, height, back_Color, refreshPeriod, multipleQueries, singelQueryName, type, componentType, Xaxis, Yaxis, chart_description);
        }

        public void generateChart(string chart_name, DataTable dt, string component_type, string x, string y)
        {
            List<KeyValuePair<string, double>> valueList = new List<KeyValuePair<string, double>>();

            chartName = chart_name;  
            chart.Title = chartName;
            
            Y_axis.Title = y;          

            lbl_x.Background = new SolidColorBrush(Colors.LightBlue);
          //  lbl_x.MouseEnter += lbl_x_MouseEnter;
            lbl_x.Content = "Key";
            lbl_y.Background = new SolidColorBrush(Colors.LightBlue);
          //  lbl_y.MouseEnter += lbl_x_MouseEnter;
            lbl_y.Content = "Value";

            lbl_deleteX.Background = new SolidColorBrush(Colors.White);
            lbl_deleteY.Background = new SolidColorBrush(Colors.White);
            lbl_deleteX.Content = "X";
            lbl_deleteY.Content = "X";
           // lbl_deleteX.MouseLeftButtonDown += lbl_deletex_MouseLeftButtonDown;
           // lbl_deleteY.MouseLeftButtonDown += lbl_deletey_MouseLeftButtonDown;

            if (x == "" || x == null)
            {
                X_axis.Title = lbl_x.Content.ToString(); ;
            }
            else
            {
                X_axis.Title = x;
            }

            if (y == "" || y == null)
            {
                Y_axis.Title = lbl_y.Content.ToString(); ;
            }
            else
            {
                Y_axis.Title = y;
            }

            DataTable data_table = new DataTable();
            data_table = dt;

            try
            {
                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    double value = 0.0;
                    try
                    {
                        value = Double.Parse(data_table.Rows[i][1].ToString());
                    }
                    catch { }

                    valueList.Add(new KeyValuePair<string, double>((data_table.Rows[i][0].ToString()), value));
                }
            }
            catch { }
            

            if (component_type == "PieChart")
            {
                PieSeries PieSeries = new PieSeries();
                // PieSeries.Title = chartName;
                PieSeries.IndependentValueBinding = new Binding("Key");
                PieSeries.DependentValueBinding = new Binding("Value");
                PieSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(PieSeries);
            }
            else if (component_type == "LineChart")
            {
                LineSeries LineSeries = new LineSeries();
                // PieSeries.Title = chartName;
                LineSeries.IndependentValueBinding = new Binding("Key");
                LineSeries.DependentValueBinding = new Binding("Value");
                LineSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(LineSeries);
            }
            else if (component_type == "AreaChart")
            {
                AreaSeries AreaSeries = new AreaSeries();
                // PieSeries.Title = chartName;
                AreaSeries.IndependentValueBinding = new Binding("Key");
                AreaSeries.DependentValueBinding = new Binding("Value");
                AreaSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(AreaSeries);

            }
            else if (component_type == "ScatterChart")
            {
                ScatterSeries ScatterSeries = new ScatterSeries();
                // PieSeries.Title = chartName;
                ScatterSeries.IndependentValueBinding = new Binding("Key");
                ScatterSeries.DependentValueBinding = new Binding("Value");
                ScatterSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(ScatterSeries);
            }
        }

       /* void lbl_x_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ((Label)sender).Background = new SolidColorBrush(Colors.LawnGreen);
            }
        }

        void lbl_deletex_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbl_x.Content = "Key";
            lbl_x.Background = new SolidColorBrush(Colors.DarkBlue);
        }

        void lbl_deletey_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbl_y.Content = "Value";
            lbl_y.Background = new SolidColorBrush(Colors.DarkBlue);
        }*/

        public void generateChart(string chart_name, int width_, int height_, string back_color, int refresh_period, List<string[]> multiple_queries, string singel_uery_ame, int type_, string component_type, string x, string y, string description)
        {
            List<KeyValuePair<string, double>> valueList = new List<KeyValuePair<string, double>>();

            chartName = chart_name;
            height = height_;
            width = width_;
            back_Color = back_color;
            refreshPeriod = refresh_period;
            type = type_;
            componentType = component_type;
            Xaxis = x;
            Yaxis = y;

            multipleQueries = multiple_queries;
            singelQueryName = singel_uery_ame;

            chart.Title = chartName;
            X_axis.Title = x;
            Y_axis.Title = y;
            chart_description = description;

            if (type == 1)
            {
                for (int i = 0; i < multipleQueries.Count; i++)
                {
                    valueList.Add(new KeyValuePair<string, double>((multipleQueries[i][0].ToString()), (getValue(multipleQueries[i][1].ToString()))));
                }
            }
            else if (type == 2)
            {
                DataTable data_table = new DataTable();
                data_table = getDataTable(singelQueryName);

                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    double value = 0.0;
                    try
                    {
                        value = Double.Parse(data_table.Rows[i][1].ToString());
                    }
                    catch { }

                    valueList.Add(new KeyValuePair<string, double>((data_table.Rows[i][0].ToString()), value));
                }
            }

            if (component_type == "PieChart")
            {
                PieSeries PieSeries = new PieSeries();
               // PieSeries.Title = chartName;
                PieSeries.IndependentValueBinding = new Binding("Key");
                PieSeries.DependentValueBinding = new Binding("Value");
                PieSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(PieSeries);
            }
            else if (component_type == "LineChart")
            {
                LineSeries LineSeries = new LineSeries();
                // PieSeries.Title = chartName;
                LineSeries.IndependentValueBinding = new Binding("Key");
                LineSeries.DependentValueBinding = new Binding("Value");
                LineSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(LineSeries);
            }
            else if (component_type == "AreaChart")
            {
                AreaSeries AreaSeries = new AreaSeries();
                // PieSeries.Title = chartName;
                AreaSeries.IndependentValueBinding = new Binding("Key");
                AreaSeries.DependentValueBinding = new Binding("Value");
                AreaSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(AreaSeries);
                
            }
            else if (component_type == "ScatterChart")
            {
                ScatterSeries ScatterSeries = new ScatterSeries();
                // PieSeries.Title = chartName;
                ScatterSeries.IndependentValueBinding = new Binding("Key");
                ScatterSeries.DependentValueBinding = new Binding("Value");
                ScatterSeries.ItemsSource = valueList;
                chart.Series.Clear();
                chart.Series.Add(ScatterSeries);
            }

            if (refreshPeriod != 0)
            {
                Timer.Interval = new TimeSpan(0, 0, refreshPeriod);
                StartUpdate();
            }
            else
            {
                Timer = null;
            }
        }      

        public DataTable getDataTable(string queryName)
        {
            string queryType = "";
            string query = "";
            string systemName = "";
            DataTable dt = new DataTable();

            for (int i = 0; i < queries.Count; i++)
            {
                if (queryName == queries[i][0].ToString())
                {
                    queryType = queries[i][2].ToString();
                    query = queries[i][1].ToString();
                    systemName = queries[i][3].ToString();
                    break;
                }
            }

            if (queryType == "SQL")
            {
                dt = sqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "MYSQL")
            {
                dt = mysqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "Excel")
            {
                dt = excelcon.RunQuery(systemName, query);
            }
            return dt;
        }

        public double getValue(string queryName)
        {
            double height = 0;
            string queryType = "";
            string query = "";
            string systemName = "";
            DataTable dt = new DataTable();

            for (int i = 0; i < queries.Count; i++)
            {
                if (queryName == queries[i][0].ToString())
                {
                    queryType = queries[i][2].ToString();
                    query = queries[i][1].ToString();
                    systemName = queries[i][3].ToString();
                    break;
                }
            }

            if (queryType == "SQL")
            {
                dt = sqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "MYSQL")
            {
                dt = mysqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "Excel")
            {
                dt = excelcon.RunQuery(systemName, query);
            }

            try
            {
                height = Double.Parse(dt.Rows[0][0].ToString());
            }
            catch
            {
                height = 0;
            }

            return height;
        }
    }
}
