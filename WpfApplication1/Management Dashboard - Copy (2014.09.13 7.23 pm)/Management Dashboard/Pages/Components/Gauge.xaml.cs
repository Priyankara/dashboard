﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard.Pages.Components
{
    /// <summary>
    /// Interaction logic for Gauge.xaml
    /// </summary>
    public partial class Gauge : Page
    {
        public string chartName="";
        public int height = 0;
        public int width = 0;       
        public string back_Color = ""; 
        public int refreshPeriod = 0;      
        public List<string[]> multipleBars = new List<string[]>();
        public string singelQueryName = "";
        public string chart_description = ""; 
                
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        SQLConnections sqlcon = new SQLConnections();
        MYSQlConnections mysqlcon = new MYSQlConnections();
        ExcelConnections excelcon = new ExcelConnections();
        List<string[]> queries = new List<string[]>();

        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();

        public Gauge()
        {
            InitializeComponent();

            Initialize();

             try
            {
                queries = ACM.getQueries();
            }
            catch { }    
      
            Timer.Tick += new EventHandler(Timer_Tick); 
        
        }
        public void Initialize()
        {
            canvas_gauge1.Visibility = Visibility.Collapsed;
            canvas_gauge2.Visibility = Visibility.Collapsed;
            canvas_gauge3.Visibility = Visibility.Collapsed;

            RotateTransform rt = new RotateTransform(-133, 0.506, 0.919);
            line_indicator1.RenderTransform = rt;
            line_indicator2.RenderTransform = rt;
            line_indicator3.RenderTransform = rt;

            lbl_11.Content = 0;
            lbl_12.Content = 0;
            lbl_13.Content = 0;
            lbl_14.Content = 0;
            lbl_15.Content = 0;
            lbl_16.Content = 0;
            lbl_17.Content = 0;
            lbl_18.Content = 0;
            lbl_19.Content = 0;
            lbl_110.Content = 0;

            lbl_20.Content = 0;
            lbl_21.Content = 0;
            lbl_22.Content = 0;
            lbl_23.Content = 0;
            lbl_24.Content = 0;
            lbl_25.Content = 0;
            lbl_26.Content = 0;
            lbl_27.Content = 0;
            lbl_28.Content = 0;
            lbl_29.Content = 0;
            lbl_210.Content = 0;

            lbl_30.Content = 0;
            lbl_31.Content = 0;
            lbl_32.Content = 0;
            lbl_33.Content = 0;
            lbl_34.Content = 0;
            lbl_35.Content = 0;
            lbl_36.Content = 0;
            lbl_37.Content = 0;
            lbl_38.Content = 0;
            lbl_39.Content = 0;
            lbl_310.Content = 0;

            lbl_chartName1.Content = "";
            lbl_chartName2.Content = "";
            lbl_chartName3.Content = "";
            lbl_value1.Content = "";
            lbl_value2.Content = "";
            lbl_value3.Content = "";
        }

        public void StopUpdate()
        {
            try
            {
                Timer.Stop();
            }
            catch { }
        }
        public void StartUpdate()
        {
            if(this.Timer != null)
            {
                this.Timer.Start();
            }         
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            generateChart(chartName, width, height, back_Color, refreshPeriod, singelQueryName, chart_description);            
        }

        public void generateChart(string chart_name, DataTable dt, string component_type)
        {
            lbl_chartName.Content = chart_name;

            lbl_key.Background = new SolidColorBrush(Colors.LightBlue);
            //  lbl_x.MouseEnter += lbl_x_MouseEnter;
            lbl_key.Content = "Key";
            lbl_value.Background = new SolidColorBrush(Colors.LightBlue);
            //  lbl_y.MouseEnter += lbl_x_MouseEnter;
            lbl_value.Content = "Value";

            lbl_deleteX.Background = new SolidColorBrush(Colors.White);
            lbl_deleteY.Background = new SolidColorBrush(Colors.White);
            lbl_deleteX.Content = "X";
            lbl_deleteY.Content = "X";
            // lbl_deleteX.MouseLeftButtonDown += lbl_deletex_MouseLeftButtonDown;
            // lbl_deleteY.MouseLeftButtonDown += lbl_deletey_MouseLeftButtonDown;

            try
            {
                Initialize();
                double value = 0;
                double rate = 0;
                DataTable data_table = new DataTable();
                data_table = dt;

                lbl_chartName.Content = chart_name;
                canvas_gauge1.Visibility = Visibility.Visible;

                for (int i = 0; i < data_table.Rows.Count; i++)
                {
                    try
                    {
                        value = Double.Parse(data_table.Rows[i][1].ToString());
                    }
                    catch { }

                    rate = (Math.Ceiling((Double)(value / 10.0)));

                    RotateTransform rt = new RotateTransform((-133 + ((27 / rate) * value)), 0.506, 0.919);

                    if (i == 0)
                    {
                        this.Width = 400;
                        canvas_gauge1.Visibility = Visibility.Visible;
                        line_indicator1.RenderTransform = rt;
                        lbl_chartName1.Content = data_table.Rows[i][0].ToString();
                        lbl_value1.Content = value.ToString();

                        lbl_10.Content = (0 * rate).ToString();
                        lbl_11.Content = (1 * rate).ToString();
                        lbl_12.Content = (2 * rate).ToString();
                        lbl_13.Content = (3 * rate).ToString();
                        lbl_14.Content = (4 * rate).ToString();
                        lbl_15.Content = (5 * rate).ToString();
                        lbl_16.Content = (6 * rate).ToString();
                        lbl_17.Content = (7 * rate).ToString();
                        lbl_18.Content = (8 * rate).ToString();
                        lbl_19.Content = (9 * rate).ToString();
                        lbl_110.Content = (10 * rate).ToString();
                    }
                    else if (i == 1)
                    {
                        this.Width = 575;
                        canvas_gauge2.Visibility = Visibility.Visible;
                        line_indicator2.RenderTransform = rt;
                        lbl_chartName2.Content = data_table.Rows[i][0].ToString();
                        lbl_value2.Content = value.ToString();

                        lbl_20.Content = (0 * rate).ToString();
                        lbl_21.Content = (1 * rate).ToString();
                        lbl_22.Content = (2 * rate).ToString();
                        lbl_23.Content = (3 * rate).ToString();
                        lbl_24.Content = (4 * rate).ToString();
                        lbl_25.Content = (5 * rate).ToString();
                        lbl_26.Content = (6 * rate).ToString();
                        lbl_27.Content = (7 * rate).ToString();
                        lbl_28.Content = (8 * rate).ToString();
                        lbl_29.Content = (9 * rate).ToString();
                        lbl_210.Content = (10 * rate).ToString();
                    }
                    else if (i == 2)
                    {
                        this.Width = 762;
                        canvas_gauge3.Visibility = Visibility.Visible;
                        line_indicator3.RenderTransform = rt;
                        lbl_chartName3.Content = data_table.Rows[i][0].ToString();
                        lbl_value3.Content = value.ToString();

                        lbl_30.Content = (0 * rate).ToString();
                        lbl_31.Content = (1 * rate).ToString();
                        lbl_32.Content = (2 * rate).ToString();
                        lbl_33.Content = (3 * rate).ToString();
                        lbl_34.Content = (4 * rate).ToString();
                        lbl_35.Content = (5 * rate).ToString();
                        lbl_36.Content = (6 * rate).ToString();
                        lbl_37.Content = (7 * rate).ToString();
                        lbl_38.Content = (8 * rate).ToString();
                        lbl_39.Content = (9 * rate).ToString();
                        lbl_310.Content = (10 * rate).ToString();
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch { }

        }

        public void generateChart(string chart_name, int width_, int height_, string back_color, int refresh_period, string singel_uery_ame, string description)
        {
            Initialize();
            double value = 0;
            double rate = 0;
            DataTable data_table = new DataTable();

            chartName = chart_name;
            height = height_;
            width = width_;
            back_Color = back_color;
            refreshPeriod = refresh_period;
            singelQueryName = singel_uery_ame;
            chart_description = description;
            
            data_table = getDataTable(singelQueryName);
            lbl_chartName.Content = chartName;

            for (int i = 0; i < data_table.Rows.Count; i++)
            {
                try
                {
                    value = Double.Parse(data_table.Rows[i][1].ToString());
                }
                catch { }

                rate = (Math.Ceiling((Double)(value / 10.0)));

                RotateTransform rt = new RotateTransform((-133 + ((27/rate) * value)), 0.506, 0.919);

                if (i == 0)
                {
                    this.Width = 400;
                    canvas_gauge1.Visibility = Visibility.Visible;
                    line_indicator1.RenderTransform = rt;
                    lbl_chartName1.Content = data_table.Rows[i][0].ToString();
                    lbl_value1.Content = value.ToString();

                    lbl_10.Content = (0 * rate).ToString();
                    lbl_11.Content = (1 * rate).ToString();
                    lbl_12.Content = (2 * rate).ToString();
                    lbl_13.Content = (3 * rate).ToString();
                    lbl_14.Content = (4 * rate).ToString();
                    lbl_15.Content = (5 * rate).ToString();
                    lbl_16.Content = (6 * rate).ToString();
                    lbl_17.Content = (7 * rate).ToString();
                    lbl_18.Content = (8 * rate).ToString();
                    lbl_19.Content = (9 * rate).ToString();
                    lbl_110.Content = (10 * rate).ToString();
                }
                else if (i == 1)
                {
                    this.Width = 575;
                    canvas_gauge2.Visibility = Visibility.Visible;
                    line_indicator2.RenderTransform = rt;
                    lbl_chartName2.Content = data_table.Rows[i][0].ToString();
                    lbl_value2.Content = value.ToString();

                    lbl_20.Content = (0 * rate).ToString();
                    lbl_21.Content = (1 * rate).ToString();
                    lbl_22.Content = (2 * rate).ToString();
                    lbl_23.Content = (3 * rate).ToString();
                    lbl_24.Content = (4 * rate).ToString();
                    lbl_25.Content = (5 * rate).ToString();
                    lbl_26.Content = (6 * rate).ToString();
                    lbl_27.Content = (7 * rate).ToString();
                    lbl_28.Content = (8 * rate).ToString();
                    lbl_29.Content = (9 * rate).ToString();
                    lbl_210.Content = (10 * rate).ToString();
                }
                else if (i == 2)
                {
                    this.Width = 762;
                    canvas_gauge3.Visibility = Visibility.Visible;
                    line_indicator3.RenderTransform = rt;
                    lbl_chartName3.Content = data_table.Rows[i][0].ToString();
                    lbl_value3.Content = value.ToString();

                    lbl_30.Content = (0 * rate).ToString();
                    lbl_31.Content = (1 * rate).ToString();
                    lbl_32.Content = (2 * rate).ToString();
                    lbl_33.Content = (3 * rate).ToString();
                    lbl_34.Content = (4 * rate).ToString();
                    lbl_35.Content = (5 * rate).ToString();
                    lbl_36.Content = (6 * rate).ToString();
                    lbl_37.Content = (7 * rate).ToString();
                    lbl_38.Content = (8 * rate).ToString();
                    lbl_39.Content = (9 * rate).ToString();
                    lbl_310.Content = (10 * rate).ToString();
                }
                else
                {
                    break;
                }
            }

            if (refreshPeriod != 0)
            {
                Timer.Interval = new TimeSpan(0, 0, refreshPeriod);
                StartUpdate();
            }
            else
            {
                Timer = null;
            }
        }        

        public DataTable getDataTable(string queryName)
        {
            string queryType = "";
            string query = "";
            string systemName = "";
            DataTable dt = new DataTable();

            for(int i=0;i<queries.Count;i++)
            {
                if(queryName==queries[i][0].ToString())
                {
                    queryType = queries[i][2].ToString();
                    query = queries[i][1].ToString();
                    systemName = queries[i][3].ToString();
                    break;
                }
            }

            if (queryType=="SQL")
            {
                dt=sqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "MYSQL")
            {
                dt= mysqlcon.RunQuery(systemName, query);
            }
            else if (queryType == "Excel")
            {
                dt = excelcon.RunQuery(systemName, query);
            }
            return dt;
        }       
    }
}
