﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for ConnectionManagerHome1.xaml
    /// </summary>
    public partial class ConnectionManagerHome1 : Page
    {       
        List<List<string>> sysList = new List<List<string>>();

        public ConnectionManagerHome1()
        {           
            InitializeComponent();
            loadSystems();            
        }
        public void loadSystems()
        {
            //read data about connected systems
            SqlConnection conn = ApplicationConnectionManager.GetConnection();             
            if (conn.State.ToString() == "Closed")
            {
                try
                {
                    conn.Open();
                }
                catch (Exception e)
                {
                    if (System.Windows.MessageBox.Show("Database connection fail.\nTry to restart by giving full permission for the installation folder \n(" + System.Windows.Forms.Application.StartupPath + ").\nClick Yes for more information.", "", MessageBoxButton.YesNoCancel) == MessageBoxResult.Yes)
                    {
                        System.Windows.MessageBox.Show(e.ToString());
                    }
                    else
                    {
                    }
                }
            }

            DataTable dt = new DataTable("Data");
            try
            {
                SqlCommand Cmd = conn.CreateCommand();
                Cmd.Connection = conn;
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = "SELECT systemName 'System Name',DBName 'Database',serverName 'Server',instanceName 'Instance',type 'Type' FROM ConectedSystem";

                SqlDataAdapter sda = new SqlDataAdapter(Cmd);
                sda.Fill(dt);
            }
            catch(Exception e)
            {
                
            }
            conn.Close();

            dgv.ItemsSource = dt.DefaultView;        
        }
    }
}
