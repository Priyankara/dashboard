﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for CreateDataBind.xaml
    /// </summary>
    public partial class CreateDataBind : Page
    {
        Label rightClicked = null;//for right clicked
        string system = "";//for right clicked
        string table = "";//for right clicked
        string column = "";//for right clicked

        List<string> SELECT = new List<string>();
        List<string> FROM = new List<string>();
        string WHERE = "";
        string ORDERBY = "NON";
        string HAVING = "";
        string GROUPBY = "NON";
        string QUERY = "";

        int tabIndex = 0;

        List<string[]> sysList;
        List<string[]> queryList;

        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        SQLConnections sqlc = new SQLConnections();
        MYSQlConnections mysqlc = new MYSQlConnections();
        ExcelConnections excelc = new ExcelConnections();

        public CreateDataBind()
        {
            InitializeComponent();
            sysList = ACM.getsystems();

            for (int i = 0; i < sysList.Count; i++)
            {
                cmb_dbType.Items.Add(sysList[i][0].ToString());
            }            
        }

        public void LoadData()
        {            
            queryList = ACM.getQueries();           
            cmb_query.Items.Clear();

            for (int i = 0; i < queryList.Count; i++)
            {
                cmb_query.Items.Add(queryList[i][0].ToString());
            } 
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_buildQuery.SelectedIndex;
            btn_next.IsEnabled = true;

            btn_finish.IsEnabled = false;

            if (i == 1 || i == 5 || i == 6)
            {
                tabIndex=0;
                btn_back.IsEnabled = false;
                tab_buildQuery.SelectedIndex = 0;
                btn_finish.Content = "Finish";
            }           
            else
            {
                tabIndex--;
                tab_buildQuery.SelectedIndex = i-1;
            }
            
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {            
            int i = tab_buildQuery.SelectedIndex;
            btn_back.IsEnabled = true;            

            if(rbtn_buildQuery.IsChecked==true)
            {
                tabIndex++;

                if (i == 2)
                {
                    btn_next.IsEnabled = false;
                    btn_finish.IsEnabled = true;

                    cmb_orderBy.Items.Add("NON");
                    cmb_groupBy.Items.Add("NON");

                    for (int j = 0; j < SELECT.Count; j++)
                    {
                        try
                        {
                            cmb_orderBy.Items.Add(SELECT[j].Remove(SELECT[j].IndexOf(" ")));
                            cmb_groupBy.Items.Add(SELECT[j].Remove(SELECT[j].IndexOf(" ")));
                        }
                        catch { }
                    }
                }

                tab_buildQuery.SelectedIndex = i+1;
            }
            else if (rbtn_openQuery.IsChecked == true)
            {
                LoadData();
                tabIndex = 6;
                tab_buildQuery.SelectedIndex = 6;
                btn_next.IsEnabled = false;
                btn_finish.IsEnabled = false;
                btn_finish.Content = "Save";
            }
        }

        public void initialize()
        {
            wp_t.Children.Clear();
            wrap_select.Children.Clear();
            rtb_where.Document.Blocks.Clear();
            rtb_where.AppendText("WHERE");
            btn_add.IsEnabled = false;
            btn_view.IsEnabled = false;
            dg_datView.ItemsSource = null;

            cmb_groupBy.Items.Clear();
            cmb_orderBy.Items.Clear();

            lbl_queryType.Content = "";
            lbl_system.Content = "";
            tb_query.Text = "";

            SELECT.Clear();
            FROM.Clear();
            WHERE = "";
            ORDERBY = "NON";
            HAVING = "";
            GROUPBY = "NON";

            wrap_selected.Children.Clear();
            wrap_from.Children.Clear();
            wrap_where.Children.Clear();
            wrap_orderby.Children.Clear();
            wrap_having.Children.Clear();
            wrap_groupby.Children.Clear();
        }

        private void cmb_dbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            initialize();
            fillWrapPanel(cmb_dbType.SelectedItem.ToString(), sysList[cmb_dbType.SelectedIndex][2]);
            btn_viewQuery.IsEnabled = true;
        }

        private void fillWrapPanel(string system, string sysType )
        {
            ApplicationConnectionManager ACM = new ApplicationConnectionManager();           
            
                List<List<string>> tables=new List<List<string>>();

                if (sysType == "Excel")
                {
                    tables = ACM.getexceltabelcolumn(ApplicationConnectionManager.GetConnection(), system);
                }
                else
                {
                    tables = ACM.getsystabelcolumn(ApplicationConnectionManager.GetConnection(), system);
                }
            
                
                genarateTree(tables, system);                
            
        }

        public void select_column(Label sender)
        {
            TreeViewItem col_label = (TreeViewItem)sender.Parent;
            TreeViewItem tbl_label = (TreeViewItem)col_label.Parent;

            Label colum = new Label();
            colum.Width = 130;
            TextBlock c = new TextBlock();
            c.Text = col_label.Header.ToString().Substring(31);
            colum.Content = c;

            if (tabIndex == 1)
            {
                WrapPanel wp = new WrapPanel();
                wp.Width = 500;  

                TextBox columnName = new TextBox();
                columnName.Width = 130;
                columnName.Text = col_label.Header.ToString().Substring(31);
                columnName.TextChanged += columnName_TextChanged;

                Button delete = new Button();
                delete.Content = "x";
                delete.Width = 30;
                delete.Margin = new Thickness() { Left = 1 };
                delete.Click += delete_Click;

                ComboBox options = new ComboBox();
                options.Width = 75;
                options.SelectedIndex = 0;
                options.Items.Add("column");
                options.Items.Add("sum of");
                options.Items.Add("count of");
                options.Items.Add("AVG");
                options.Items.Add("MAX");
                options.Items.Add("MIN");
                options.Items.Add("DISTINCT");
                options.SelectionChanged += options_SelectionChanged;

                string selectStatement = "";
                switch (options.SelectedIndex)
                {
                    case 0: selectStatement = tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31)+" as "+columnName.Text.ToString(); break;
                    case 1: selectStatement = "SUM(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                    case 2: selectStatement = "COUNT(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                    case 3: selectStatement = "AVG(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                    case 4: selectStatement = "MAX(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                    case 5: selectStatement = "MIN(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                    case 6: selectStatement = "DISTINCT(" + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31) + ")" + " as " + columnName.Text.ToString(); break;
                }
                SELECT.Add(selectStatement);
                FROM.Add(tbl_label.Header.ToString().Substring(31));

                wp.Children.Add(options);
                wp.Children.Add(colum);
                wp.Children.Add(columnName);
                wp.Children.Add(delete);

                wrap_select.Children.Add(wp);
                queryShow();
            }
            else if (tabIndex == 2)
            {
                rtb_where.AppendText(" " + tbl_label.Header.ToString().Substring(31) + "." + col_label.Header.ToString().Substring(31));
            }
        }

        public void queryShow()
        {
             wrap_selected.Children.Clear();
             wrap_from.Children.Clear();
             wrap_groupby.Children.Clear();
             wrap_having.Children.Clear();
             wrap_orderby.Children.Clear();
             wrap_where.Children.Clear();

             for (int i = 0; i < SELECT.Count; i++)
             {
                 TextBlock tb_select = new TextBlock();
                 tb_select.FontSize = 10;
                 TextBlock tb_from = new TextBlock();
                 tb_from.FontSize = 10;

                 if (i == 0)
                 {
                     tb_select.Text = SELECT[i];
                     tb_from.Text = FROM[i];
                     wrap_from.Children.Add(tb_from);
                 }
                 else
                 {
                     tb_select.Text = ", " + SELECT[i];
                     tb_from.Text = ", "+FROM[i];
                     for (int j = 0; j < i ; j++)
                     {
                         if (FROM[j] == FROM[i])
                         {
                             break;
                         }
                         else
                         {
                             if (j == i - 1)
                             {
                                 wrap_from.Children.Add(tb_from);
                             }
                             else
                             {
                                 continue;
                             }
                         }
                        
                     }
                    
                 }
                
                 wrap_selected.Children.Add(tb_select);
                             
             }

             TextBlock tb_where = new TextBlock();
             tb_where.FontSize = 15;
             tb_where.Text = WHERE;

             TextBlock tb_orderby = new TextBlock();
             tb_orderby.FontSize = 15;
             tb_orderby.Text = ORDERBY;

             TextBlock tb_having = new TextBlock();
             tb_having.FontSize = 15;
             tb_having.Text = HAVING;

             TextBlock tb_groupby = new TextBlock();
             tb_groupby.FontSize = 15;
             tb_groupby.Text = GROUPBY;

             wrap_where.Children.Add(tb_where);
             wrap_orderby.Children.Add(tb_orderby);
             wrap_having.Children.Add(tb_having);
             wrap_groupby.Children.Add(tb_groupby);
        }

        public void selectStatementChanged(WrapPanel wp)
        {
            ComboBox cmb = (ComboBox)wp.Children[0];
            Label lbl = (Label)wp.Children[1];
            TextBlock tb = (TextBlock)lbl.Content;
            TextBox txtb = (TextBox)wp.Children[2];

            for (int i = 0; i < wrap_select.Children.Count; i++)
            {
                WrapPanel n = (WrapPanel)wrap_select.Children[i];
                if (n == wp)
                {
                    string selectStatement = "";
                    switch (cmb.SelectedIndex)
                    {
                        case 0: selectStatement = FROM[i].ToString() + "." + tb.Text.ToString()+ " as " + txtb.Text.ToString(); break;
                        case 1: selectStatement = "SUM(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                        case 2: selectStatement = "COUNT(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                        case 3: selectStatement = "AVG(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                        case 4: selectStatement = "MAX(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                        case 5: selectStatement = "MIN(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                        case 6: selectStatement = "DISTINCT(" + FROM[i].ToString() + "." + tb.Text.ToString() + ")" + " as " + txtb.Text.ToString(); break;
                    }
                    SELECT[i] = selectStatement;
                }
            }
        }

        void columnName_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtb = (TextBox)sender;
            WrapPanel wp = (WrapPanel)txtb.Parent;
                      
            selectStatementChanged(wp);
            queryShow();
        }

        void options_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            WrapPanel wp = (WrapPanel)cmb.Parent;
           
            selectStatementChanged(wp);
            queryShow();
        }

        public void selectColumn(object sender, EventArgs e)
        {            
            Label sent = (Label)sender;
            select_column(sent);
           
        }
        void add_Click(object sender, RoutedEventArgs e)
        {
            select_column(rightClicked);
        }

        public void select_all(Label sender)
        {
            if(tabIndex==1)
            {
                WrapPanel wp = new WrapPanel();
                wp.Width = 500;

                TreeViewItem tbl_label = (TreeViewItem)sender.Parent;

                Label all = new Label();
                all.Content = "All columns(*)";

                Label table = new Label();
                table.Width = 152;
                TextBlock c = new TextBlock();
                c.Text = tbl_label.Header.ToString().Substring(31);
                table.Content = c;

                SELECT.Add(tbl_label.Header.ToString().Substring(31) + ".*");
                FROM.Add(tbl_label.Header.ToString().Substring(31));

                Button delete = new Button();
                delete.Content = "x";
                delete.Width = 30;
                delete.Margin = new Thickness() { Left = 100 };
                delete.Click += delete_Click;

                wp.Children.Add(all);
                wp.Children.Add(table);
                wp.Children.Add(delete);

                wrap_select.Children.Add(wp);
                queryShow();
            }
            else if(tabIndex==2)
            {

            }
            
        }

        public void selectAll(object sender, EventArgs e)
        {            
            Label sent = (Label)sender;
            select_all(sent);
        }

        void add_all_click(object sender, RoutedEventArgs e)
        {
            select_all(rightClicked);
        }

        void delete_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            WrapPanel wp = (WrapPanel)btn.Parent;
            
            
            for (int i = 0; i < wrap_select.Children.Count;i++ )
            {
                WrapPanel n=(WrapPanel)wrap_select.Children[i];
                if(n==wp)
                {
                    SELECT.RemoveAt(i);
                    FROM.RemoveAt(i);                    
                }
            }
            wrap_select.Children.Remove(wp);
            queryShow();
        }
        
        public void genarateTree(List<List<string>> tables,string db)
        {
            TreeView tree = new TreeView();
            tree.Width = 350;

            tree.SelectedItemChanged += tree_SelectedItemChanged;

            TreeViewItem treeItems = new TreeViewItem(); 
            String DBName = db;
            TextBlock cbts = new TextBlock();
            cbts.Text = DBName;                      
            treeItems.Header = cbts;            

            for (int b = 0; b < tables.Count; b++)
            {
                ContextMenu mnu_all = new ContextMenu();
                MenuItem view_all = new MenuItem();
                MenuItem add_all = new MenuItem();
                mnu_all.Items.Add(add_all);
                add_all.Header = "Add All";
                mnu_all.Items.Add(view_all);
                view_all.Header = "View All";
                view_all.Click += view_Click;
                add_all.Click +=add_all_click;

                TreeViewItem treeItem = new TreeViewItem();
                String TableName = tables[b][0].ToString();
                Label cbt = new  Label();
                TextBlock cbtt = new TextBlock();
                cbtt.Text = TableName;
                cbt.ContextMenu = mnu_all;
       
                cbt.Content = cbtt;
                //cbt.Name = TableName;
                cbt.MouseDoubleClick +=selectAll;
                cbt.MouseRightButtonDown += cbt_MouseRightButtonDown;
                
                treeItem.Header = cbt;

                for (int c = 1; c < tables[b].Count ; c++)
                {
                    ContextMenu mnu = new ContextMenu();                    
                    MenuItem view = new MenuItem();
                    MenuItem add = new MenuItem();
                    mnu.Items.Add(add);
                    add.Header = "Add";
                    mnu.Items.Add(view);
                    view.Header = "View";
                    view.Click += view_Click;
                    add.Click += add_Click;

                    TextBlock cbcc = new TextBlock();
                    cbcc.Text = tables[b][c].ToString();
                    Label cbc = new Label();
                    cbc.Content = cbcc;
                    //cbc.Name = tables[b][c].ToString();
                    cbc.MouseDoubleClick +=selectColumn;
                    cbc.ContextMenu = mnu;                   
                    cbc.MouseRightButtonDown+=cbc_MouseRightButtonDown;
                    treeItem.Items.Add(new TreeViewItem() { Header = cbc });
                }

                treeItems.Items.Add(new TreeViewItem() { Header = treeItem });

            }
            tree.Items.Add(treeItems);
            wp_t.Children.Add(tree);
        }      

        void cbc_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Label co = (Label)sender;
            rightClicked = co;
            
            TreeViewItem col_label = (TreeViewItem)co.Parent;
            TreeViewItem tbl_label = (TreeViewItem)col_label.Parent;
            TreeViewItem sys = (TreeViewItem)tbl_label.Parent;
            TreeViewItem sys_header = (TreeViewItem)sys.Parent;
            TextBlock sys_textbock = (TextBlock)sys_header.Header;
            col_label.IsSelected = true;

            system = sys_textbock.Text.ToString();
            table = tbl_label.Header.ToString().Substring(31);
            column = col_label.Header.ToString().Substring(31);
           
        }

        void cbt_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Label co = (Label)sender;
            rightClicked = co;

            TreeViewItem tbl_label = (TreeViewItem)co.Parent;
            TreeViewItem sys_header = (TreeViewItem)tbl_label.Parent;
            TreeViewItem sys = (TreeViewItem)sys_header.Parent;
           
            TextBlock sys_textbock = (TextBlock)sys.Header;
            tbl_label.IsSelected = true;

            system = sys_textbock.Text.ToString();
            table = tbl_label.Header.ToString().Substring(31);
            column = "";

        }
        
        

        void view_Click(object sender, RoutedEventArgs e)
        {
            DataTable dt = new DataTable();

            string selectedSystem=cmb_dbType.SelectedItem.ToString();
            string selectedDbType = "";
            for (int i = 0; i < sysList.Count;i++)
            {
                if (sysList[i][0].ToString() == selectedSystem)
                {
                    selectedDbType = sysList[i][2].ToString();
                }
            }

            if (selectedDbType == "SQL")
            {                
                dt = sqlc.getSQLdata(system, table, column);
            }
            else if(selectedDbType == "MYSQL")
            {                
                dt = mysqlc.getMYSQLData(system, table, column);
            }
            else if (selectedDbType == "Excel")
            {
                dt = excelc.getExcelData(system, table, column);
            }            
            dg_datView.ItemsSource = dt.DefaultView;
        }

        void tree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            foreach (TreeView tree in wp_t.Children)
            {
                if (tree.SelectedItem != null)
                {
                    btn_add.IsEnabled = true;
                    btn_view.IsEnabled = true;
                }
                else
                {
                    btn_add.IsEnabled = false;
                    btn_view.IsEnabled = false;
                }
            }
          
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < SELECT.Count;i++ )
            {
                MessageBox.Show(FROM[i]);
            }
        }

        private void btn_view_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected=null;
            try
            {
                foreach (TreeView tree in wp_t.Children)
                {
                    selected = (TreeViewItem)tree.SelectedItem;
                }

                Label h = (Label)selected.Header;
                if (selected.HasItems)
                {
                    cbt_MouseRightButtonDown(h,null);
                }
                else
                {
                    cbc_MouseRightButtonDown(h, null);
                }

                view_Click(null,null);
            }
            catch
            {
            }
            

           
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = null;
            try
            {
                foreach (TreeView tree in wp_t.Children)
                {
                    selected = (TreeViewItem)tree.SelectedItem;
                }

                Label h = (Label)selected.Header;
                if (selected.HasItems)
                {
                    select_all(h);
                }
                else
                {
                    select_column(h);
                }
            }
            catch
            {
            }
        }

        public void generateQuery()
        {
            QUERY = "SELECT";

            for (int i = 0; i < SELECT.Count; i++)
            {
                if (i == 0)
                {
                    QUERY = QUERY + " " + SELECT[i];
                    continue;
                }
                QUERY = QUERY + ", " + SELECT[i];

            }

            List<string> FROM_distinct = new List<string>();
            for (int j = 0; j < FROM.Count; j++)
            {
                if (j == 0)
                {
                    FROM_distinct.Add(FROM[j]);
                    QUERY = QUERY + " FROM " + FROM[0];
                    continue;
                }

                if (!FROM_distinct.Contains(FROM[j]))
                {
                    FROM_distinct.Add(FROM[j]);
                    QUERY = QUERY + ", " + FROM[j];
                }

            }

            QUERY = QUERY + " " + WHERE;
            if (ORDERBY != "NON")
            {
                QUERY = QUERY + " ORDER BY " + ORDERBY;
            }
            if (GROUPBY != "NON")
            {
                QUERY = QUERY + " GROUP BY " + GROUPBY;
            }
            
        }

        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {            
            if (btn_finish.Content.ToString() == "Finish")
            {
                generateQuery();

                try
                {
                    lbl_system.Content = sysList[cmb_dbType.SelectedIndex][0].ToString();
                    lbl_queryType.Content = sysList[cmb_dbType.SelectedIndex][2].ToString();
                }
                catch
                {
                }
                tb_query.Text = QUERY;

                string status = ACM.saveQuery(txt_queryName.Text.ToString(),lbl_system.Content.ToString(),lbl_queryType.Content.ToString(),QUERY);
                if (status == "exist")
                {
                    MessageBox.Show("Query Name already exist. Try anothry one");
                }
                else if (status == "fail")
                {
                    MessageBox.Show("Query saving failed. Check database connectivity");
                }
                else if (status == "saved")
                {
                    tab_buildQuery.SelectedIndex = 4;
                    tabIndex = 4;
                    lbl_queryName.Content = txt_queryName.Text.ToString();
                    btn_finish.Content = "New";
                    btn_back.IsEnabled = false;
                }
                   
            }
            else if (btn_finish.Content.ToString() == "Save")
            {
                TextRange textRange = new TextRange(rtxt_query.Document.ContentStart, rtxt_query.Document.ContentEnd); 
                MessageBox.Show(ACM.EditDeleteQuery(queryList[cmb_query.SelectedIndex][0], textRange.Text.ToString(), 2));
                btn_finish.IsEnabled = false;
                LoadData();
                //rtxt_query.Document.Blocks.Clear();
            }
            else
            {
                tab_buildQuery.SelectedIndex = 0;
                tabIndex = 0;
                btn_finish.Content = "Finish";
                btn_finish.IsEnabled = false;
                btn_next.IsEnabled = true;
                btn_viewQuery.IsEnabled = false;
                initialize();
            }
        }

        private void btn_viewQuery_Click(object sender, RoutedEventArgs e)
        {
            DataTable dt = new DataTable();
            string query = "";
            string system = "";
            string type = "";

            if (rbtn_buildQuery.IsChecked == true)
            {
                generateQuery();
                query = QUERY;
                system=sysList[cmb_dbType.SelectedIndex][0].ToString();
                type=sysList[cmb_dbType.SelectedIndex][2].ToString();
            }
            else if (rbtn_openQuery.IsChecked == true)
            {
                TextRange textRange = new TextRange(rtxt_query.Document.ContentStart, rtxt_query.Document.ContentEnd);
                query = textRange.Text.ToString();
                system = queryList[cmb_query.SelectedIndex][3];
                type = queryList[cmb_query.SelectedIndex][2];
            }

            if(type=="SQL")
            {
                dt = sqlc.RunQuery(system, query);                
            }
            else if (type == "MYSQL")
            {
                dt = mysqlc.RunQuery(system, query);  
            }
            else if (type == "Excel")
            {
                dt = excelc.RunQuery(system, query);
            }

            dg_datView.ItemsSource = dt.DefaultView;            
        }

        private void cmb_operators_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rtb_where.AppendText(" "+cmb_operators.SelectedItem.ToString().Substring(38));
        }

        private void cmb_keyword_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rtb_where.AppendText(" " + cmb_keyword.SelectedItem.ToString().Substring(38));
        }

        private void cmb_others_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rtb_where.AppendText(" " + cmb_others.SelectedItem.ToString().Substring(38));
        }

        private void rtb_where_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextRange textRange = new TextRange(rtb_where.Document.ContentStart, rtb_where.Document.ContentEnd);
            WHERE = textRange.Text.ToString();
            wrap_where.Children.Clear();
            TextBlock tb_where = new TextBlock();
            tb_where.FontSize = 10;
            tb_where.Text = WHERE;
            wrap_where.Children.Add(tb_where);
        }

        private void cmb_groupBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                GROUPBY = cmb_groupBy.SelectedItem.ToString();
                wrap_groupby.Children.Clear();
                TextBlock tb_groupBy = new TextBlock();
                tb_groupBy.Text = GROUPBY;
                wrap_groupby.Children.Add(tb_groupBy);
            }
            catch { }
        }

        private void cmb_orderBy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ORDERBY = cmb_orderBy.SelectedItem.ToString(); 
                wrap_orderby.Children.Clear();
                TextBlock tb_orderBy = new TextBlock();
                tb_orderBy.Text = ORDERBY;
                wrap_orderby.Children.Add(tb_orderBy);
            }
            catch { }
        }

        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
           string status = "";
           status=ACM.EditDeleteQuery(queryList[cmb_query.SelectedIndex][0], null, 1);
           MessageBox.Show(status);
            if(status=="Deleted")
            {
                btn_viewQuery.IsEnabled = false;
                rtxt_query.Document.Blocks.Clear();
                LoadData();
            }
        }

        private void cmb_query_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rtxt_query.Document.Blocks.Clear();
            try
            {
                rtxt_query.AppendText(queryList[cmb_query.SelectedIndex][1]);
            }
            catch { }

            if (rtxt_query.Document.Blocks.Count > 0)
            {
                btn_finish.IsEnabled = true;
                btn_viewQuery.IsEnabled = true;
            }
            else
            {
                btn_finish.IsEnabled = false;
                btn_viewQuery.IsEnabled = false;
            }
        }
    }
}
