﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for CreateExcelConnection.xaml
    /// </summary>
    public partial class CreateExcelConnection : Page
    {
        List<List<List<string>>> excelSys = new List<List<List<string>>>();

        ExcelConnections EXC = new ExcelConnections();

        public CreateExcelConnection()
        {
            InitializeComponent();
            txt_filePath.TextChanged += txt_filePath_TextChanged;
            txt_folderPath.TextChanged += txt_folderPath_TextChanged;

            ckbx_all.Checked +=ckbx_all_Checked;
            ckbx_non.Checked +=ckbx_non_Checked;
        }

       // event handlers--------------------------------------------------------------------------------------------------

        void txt_folderPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            btn_addFolder.IsEnabled = true;
        }

        void txt_filePath_TextChanged(object sender, TextChangedEventArgs e)
        {
            btn_addFile.IsEnabled = true;
        }

        //button clicks----------------------------------------------------------------------------------------------------

        private void btn_fileBrows_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFile = new System.Windows.Forms.OpenFileDialog();
            openFile.Filter="Office Files|*.xlsx";
            
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                txt_filePath.Text = openFile.FileName;

            }
        }

        private void btn_folderBrows_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog openFolder = new FolderBrowserDialog();
            openFolder.ShowNewFolderButton = false;

            if (openFolder.ShowDialog() == DialogResult.OK)
            {
                txt_folderPath.Text = openFolder.SelectedPath;

            }
        }
        
        private void btn_addFolder_Click(object sender, RoutedEventArgs e)
        {
            List<string> filePaths = new List<string>();
            filePaths = EXC.getFolder(txt_folderPath.Text.ToString());

            for (int i = 0; i < filePaths.Count; i++)
            {
                List<List<string>> excelFile = new List<List<string>>();
                excelFile = EXC.getFiles(filePaths[i].ToString());
                generateTree(excelFile);
            }
        }

        private void btn_addFile_Click(object sender, RoutedEventArgs e)
        {
            List<List<string>> excelFile = new List<List<string>>();
            excelFile = EXC.getFiles(txt_filePath.Text.ToString());

            generateTree(excelFile);

        }

        private void btn_makeConn_Click(object sender, RoutedEventArgs e)
        {
            ExcelConnections EXC = new ExcelConnections();
            List<List<List<string>>> save_items = new List<List<List<string>>>();
            save_items = getChecked();
            string status = "";

            string sysName = txt_sysName.Text.ToString();

            status = EXC.create_Excelconnection(sysName, save_items);


            if (status == "saved")
            {
                System.Windows.MessageBox.Show("Successfully Saved");
            }
            else if (status == "fail")
            {
                System.Windows.MessageBox.Show("Saving Failed");
            }
            else if (status == "exist")
            {
                System.Windows.MessageBox.Show("The system name is already exist");
            }
        }


        //-------------------------------------------functions--------------------------------------------------------

        public void generateTree(List<List<string>> excelFile)
        {
            if (excelFile.Capacity > 0 && excelFile != null)
            {
                grd_tables.IsEnabled = true;
                btn_makeConn.IsEnabled = true;
                tree_main.IsEnabled = true;
                // tree_main.Items.Clear();
                //lbl_status.Content = "Successful";
                ckbx_all.IsEnabled = true;
                ckbx_non.IsEnabled = true;


                TreeViewItem treeItem = new TreeViewItem();// for tabel name 
                String fileName = excelFile[0][0].ToString();
                System.Windows.Controls.CheckBox cbt = new System.Windows.Controls.CheckBox();
                cbt.Content = fileName;
                //cbt.Name = fileName;
                cbt.Checked += cb_file_Checked;
                cbt.Unchecked += cb_file_UnChecked;
                treeItem.Header = cbt;

                for (int b = 0; b < excelFile.Count; b++)
                {
                    //get sheet name
                    TreeViewItem treeItem2 = new TreeViewItem();// for tabel name 
                    String sheetName = excelFile[b][1].ToString();
                    System.Windows.Controls.CheckBox cbt2 = new System.Windows.Controls.CheckBox();
                    cbt2.Content = sheetName;
                    //cbt2.Name = sheetName;
                    cbt2.Checked += cb_sheet_Checked;
                    cbt2.Unchecked += cb_sheet_UnChecked;
                    treeItem2.Header = cbt2;

                    //get columns
                    for (int c = 2; c < excelFile[b].Count ; c++)
                    {
                        System.Windows.Controls.CheckBox cbc = new System.Windows.Controls.CheckBox();
                        cbc.Content = excelFile[b][c].ToString();
                        // cbc.Name = excelFile[b][c].ToString();
                        cbc.Checked += cb_column_Checked;
                        cbc.Unchecked += cb_column_UnChecked;
                        treeItem2.Items.Add(new TreeViewItem() { Header = cbc });
                    }

                    treeItem.Items.Add(treeItem2);

                }
                tree_main.Items.Add(treeItem);
            }
            else
            {
                //lbl_status.Content = "Connection Fail";
                grd_tables.IsEnabled = false;
                btn_makeConn.IsEnabled = false;
                tree_main.IsEnabled = false;
                ckbx_all.IsEnabled = false;
                ckbx_non.IsEnabled = false;
            }
        }

        public List<List<List<string>>> getChecked()
        {
            List<List<List<string>>> save_items = new List<List<List<string>>>();

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                System.Windows.Controls.CheckBox file_checked = (System.Windows.Controls.CheckBox)tree_items.Header;

                if (file_checked.IsChecked == true)
                {
                    List<List<string>> save_file = new List<List<string>>();

                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        System.Windows.Controls.CheckBox sheet_checked = (System.Windows.Controls.CheckBox)tree_item.Header;

                        if (sheet_checked.IsChecked == true)
                        {
                            List<string> save_column = new List<string>();
                            save_column.Add(file_checked.Content.ToString());
                            save_column.Add(sheet_checked.Content.ToString());

                            foreach (TreeViewItem item in tree_item.Items)
                            {
                                System.Windows.Controls.CheckBox column_checked = (System.Windows.Controls.CheckBox)item.Header;

                                if (column_checked.IsChecked == true)
                                {
                                    save_column.Add(column_checked.Content.ToString());
                                }
                            }

                            save_file.Add(save_column);
                        }

                    }
                    save_items.Add(save_file);

                }

            }
            return save_items;
        }

    
        //--------------tree event handlers-----------------------------------


        public void cb_file_Checked(object sender, EventArgs e)
        {
            System.Windows.Controls.CheckBox clicked_cb = (System.Windows.Controls.CheckBox)sender;

            ckbx_non.IsChecked = false;
            bool checkFull = true;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb)
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        System.Windows.Controls.CheckBox clicked_cb_sheet = (System.Windows.Controls.CheckBox)tree_item.Header;

                        if (clicked_cb_sheet.IsChecked == true)
                        {
                            checkFull = false;
                        }
                       // clicked_cb_sheet.IsChecked = true;
                    }
                }

            }

            if (checkFull==true)
            {
                foreach (TreeViewItem tree_items in tree_main.Items)
                {
                    if ((tree_items.Header) == clicked_cb)
                    {
                        foreach (TreeViewItem tree_item in tree_items.Items)
                        {
                            System.Windows.Controls.CheckBox clicked_cb_sheet = (System.Windows.Controls.CheckBox)tree_item.Header;
                            clicked_cb_sheet.IsChecked = true;
                        }
                    }

                }
            }
        }

        public void cb_file_UnChecked(object sender, EventArgs e)
        {
            System.Windows.Controls.CheckBox clicked_cb = (System.Windows.Controls.CheckBox)sender;

            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb)
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        System.Windows.Controls.CheckBox clicked_cb_sheet = (System.Windows.Controls.CheckBox)tree_item.Header;
                        clicked_cb_sheet.IsChecked = false;
                    }
                }

            }
        }

        private void cb_sheet_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox clicked_cb = (System.Windows.Controls.CheckBox)sender;

            ckbx_non.IsChecked = false;
            bool checkFull = true;
            
            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                System.Windows.Controls.CheckBox clicked_cb_file = (System.Windows.Controls.CheckBox)tree_items.Header;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    if ((tree_item.Header) == clicked_cb)
                    {
                        clicked_cb_file.IsChecked = true;

                        foreach (TreeViewItem item in tree_item.Items)
                        {
                            System.Windows.Controls.CheckBox clicked_cb_column = (System.Windows.Controls.CheckBox)item.Header;

                            if (clicked_cb_column.IsChecked==true)
                            {
                                checkFull = false;
                            }
                           
                            //clicked_cb_column.IsChecked = true;
                        }
                    }

                }
 
            }


            if (checkFull == true)
            {
                foreach (TreeViewItem tree_items in tree_main.Items)
                {
                    System.Windows.Controls.CheckBox clicked_cb_file = (System.Windows.Controls.CheckBox)tree_items.Header;

                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        if ((tree_item.Header) == clicked_cb)
                        {
                            clicked_cb_file.IsChecked = true;

                            foreach (TreeViewItem item in tree_item.Items)
                            {
                                System.Windows.Controls.CheckBox clicked_cb_column = (System.Windows.Controls.CheckBox)item.Header;
                                clicked_cb_column.IsChecked = true;
                            }
                        }

                    }

                }
            }
            else
            {

            }


        }

        private void cb_sheet_UnChecked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox clicked_cb = (System.Windows.Controls.CheckBox)sender;

            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    if ((tree_item.Header) == clicked_cb)
                    {
                        foreach (TreeViewItem item in tree_item.Items)
                        {
                            System.Windows.Controls.CheckBox clicked_cb_column = (System.Windows.Controls.CheckBox)item.Header;
                            clicked_cb_column.IsChecked = false;
                        }
                    }

                }

            }
        }



        private void cb_column_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox clicked_cb = (System.Windows.Controls.CheckBox)sender;

            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                System.Windows.Controls.CheckBox clicked_file = (System.Windows.Controls.CheckBox)tree_items.Header;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    System.Windows.Controls.CheckBox clicked_sheet = (System.Windows.Controls.CheckBox)tree_item.Header;

                    foreach (TreeViewItem item in tree_item.Items)
                    {
                        if ((item.Header) == clicked_cb)
                        {
                            clicked_sheet.IsChecked = true;
                        }
                        
                    }
                    

                }

            }
        }

        private void cb_column_UnChecked(object sender, RoutedEventArgs e)
        {
            ckbx_all.IsChecked = false;
        }


        public void ckbx_all_Checked(object sender, EventArgs e)
        {
            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                System.Windows.Controls.CheckBox cbt = (System.Windows.Controls.CheckBox)tree_items.Header;
                cbt.IsChecked = true;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    System.Windows.Controls.CheckBox cb = (System.Windows.Controls.CheckBox)tree_item.Header;
                    cb.IsChecked = true;
                    foreach (TreeViewItem item in tree_item.Items)
                    {
                        System.Windows.Controls.CheckBox cbi = (System.Windows.Controls.CheckBox)item.Header;
                        cbi.IsChecked = true;
                    }
                }

            }
        }

        public void ckbx_non_Checked(object sender, EventArgs e)
        {
            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                System.Windows.Controls.CheckBox cbt = (System.Windows.Controls.CheckBox)tree_items.Header;
                cbt.IsChecked = false;
                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    System.Windows.Controls.CheckBox cb = (System.Windows.Controls.CheckBox)tree_item.Header;
                    cb.IsChecked = false; ;
                    foreach (TreeViewItem item in tree_item.Items)
                    {
                        System.Windows.Controls.CheckBox cbi = (System.Windows.Controls.CheckBox)item.Header;
                        cbi.IsChecked = false; ;
                    }
                }

            }
        }    

    }
}
