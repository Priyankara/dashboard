﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for CreateMYSQLConnection.xaml
    /// </summary>
    public partial class CreateMYSQLConnection : Page
    {
        List<List<string>> tables = new List<List<string>>();

        public CreateMYSQLConnection()
        {
            InitializeComponent();
            load();
            txt_serverName.TextChanged += txt_serverName_TextChanged;
            cmb_serverIp.SelectionChanged += cmb_serverIp_SelectionChanged;
            cmb_serverIp.MouseDoubleClick += cmb_serverIp_SelectionChanged;
            ckbox_ip.Checked += ckbox_ip_Checked;
            ckbox_name.Checked += ckbox_name_Checked;
            ckbox_ip.Unchecked += ckbox_ip_Unchecked;
            ckbox_name.Unchecked += ckbox_name_Unchecked;
            cmb_database.SelectionChanged += cmb_database_SelectionChanged;
            
            ckbx_all.Checked += ckbx_all_Checked;
            ckbx_non.Checked += ckbx_non_Checked;
        }

        //event handlers-----------------------------------------------------------------------------------------------

        void cmb_database_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txt_sysName.Text.ToString() == "" || txt_sysName.Text.ToString() == null)
            {
                txt_sysName.Text = cmb_database.SelectedItem.ToString();
            }
        }

        void ckbox_name_Unchecked(object sender, RoutedEventArgs e)
        {
            ckbox_ip.IsChecked = true;
        }

        void ckbox_ip_Unchecked(object sender, RoutedEventArgs e)
        {
            ckbox_name.IsChecked = true;
        }

        void ckbox_name_Checked(object sender, RoutedEventArgs e)
        {
            ckbox_ip.IsChecked = false;
            grd_serverIp.IsEnabled = false;
            grd_serverName.IsEnabled = true;
        }

        void ckbox_ip_Checked(object sender, RoutedEventArgs e)
        {
            ckbox_name.IsChecked = false;
            grd_serverName.IsEnabled = false;
            grd_serverIp.IsEnabled = true;
        }

        void cmb_serverIp_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                txt_serverName.Text = cmb_serverIp.SelectedItem.ToString();
            }
            catch
            {
            }
        }

        void txt_serverName_TextChanged(object sender, TextChangedEventArgs e)
        {
            msktxt_serverPort.Text = "3306";        
        }       

        //button clicks-----------------------------------------------------------------------------------------------

        private void register_connection_Click(object sender, RoutedEventArgs e)
        {
            MYSQlConnections MYSQLC = new MYSQlConnections();
            List<List<string>> save_items = new List<List<string>>();
            save_items = getChecked();
            string status = "";

            string sysName = "";
            string serverName = "";
            string serverIp = "";
            string port = "";
            string userName = "";
            string password = "";
            string dbName = "";


            try
            {
                sysName = txt_sysName.Text.ToString();
                dbName = cmb_database.SelectedItem.ToString();
                serverName = txt_serverName.Text.ToString();
                port = msktxt_serverPort.Text.ToString();
                userName = txt_username.Text.ToString();
                password = txt_password.Password.ToString();
                serverIp = cmb_serverIp.SelectedItem.ToString();
            }
            catch
            {
            }

            if (ckbox_name.IsChecked == true)
            {
                status = MYSQLC.create_MYSQLconnection(sysName, serverName, port, dbName, userName, password, save_items, 1);
            }
            else if (ckbox_ip.IsChecked == true)
            {
                status = MYSQLC.create_MYSQLconnection(sysName, serverIp, port, dbName, userName, password, save_items, 2);
            }

            if (status == "saved")
            {
                MessageBox.Show("Successfully Saved");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving Failed");
            }
            else if (status == "exist")
            {
                MessageBox.Show("The system name is already exist");
            }
        }

        private void btn_refresh_Click_1(object sender, RoutedEventArgs e)
        {
            cmb_database.Items.Clear();

            string mysqlConStr;
            string server = "";

            if (ckbox_name.IsChecked == true)
            {
                server = txt_serverName.Text.ToString();
            }
            else if (ckbox_ip.IsChecked == true)
            {
                server = cmb_serverIp.SelectedItem.ToString();
            }

            string userName = txt_username.Text.ToString();
            string password = txt_password.Password.ToString();
            string port = msktxt_serverPort.Text.ToString();

            mysqlConStr = "server=" + server + "; uid=" + userName + ";pwd=" + password + ";Port=" + port + ";Persist Security Info=True";

            try
            {
                MySqlConnection conn = new MySqlConnection(mysqlConStr);
                MySqlCommand cmd = new MySqlCommand("show databases;");

                cmd.Connection = conn;
                conn.Open();
                MySqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    cmb_database.Items.Add(dr[0].ToString());
                }

                conn.Close();
            }
            catch (Exception ee)
            {

            }
        }

        private void btn_testConn_1_Click(object sender, RoutedEventArgs e)
        {
            string server = "";

            if (ckbox_name.IsChecked == true)
            {
                server = txt_serverName.Text.ToString();
            }
            else if (ckbox_ip.IsChecked == true)
            {
                server = cmb_serverIp.SelectedItem.ToString();
            }

            string userName = txt_username.Text.ToString();
            string password = txt_password.Password.ToString();
            string port = msktxt_serverPort.Text.ToString();

            tree_main.Visibility = Visibility.Visible;
            MYSQlConnections MYSQLConn = new MYSQlConnections();
            tables.Clear();

            try
            {
                tables = MYSQLConn.test_MYSQLconnection(server, port, cmb_database.SelectedItem.ToString(), txt_username.Text.ToString(), txt_password.Password.ToString());
            }
            catch (Exception ee)
            {

            }
            
            if (tables.Capacity > 0 && tables != null)
            {
                grd_tables.IsEnabled = true;
                btn_makeConn.IsEnabled = true;
                tree_main.IsEnabled = true;
                tree_main.Items.Clear();
                //lbl_status.Content = "Successful";
                //MessageBox.Show("Test Successful");
                ckbx_all.IsEnabled = true;
                ckbx_non.IsEnabled = true;


                //    tree_main = setTree(tables);

                /*  TreeViewItem tree = new TreeViewItem();// for all tables 
                  CheckBox all = new CheckBox();
                  all.Content = "ALL";
                  all.Name = "cb_all";
                  all.Checked += all_Checked;
                  all.Unchecked += all_UnChecked;
                  tree.Header = all;
                  tree_main.Items.Add(tree);*/

                for (int b = 0; b < tables.Count; b++)
                {
                    TreeViewItem treeItem = new TreeViewItem();// for tabel name 
                    String TableName = tables[b][0].ToString();
                    CheckBox cbt = new CheckBox();
                    cbt.Content = TableName;
                    cbt.Name = TableName;
                    cbt.Checked += cbt_Checked;
                    cbt.Unchecked += cbt_UnChecked;
                    treeItem.Header = cbt;

                    //get columns
                    for (int c = 1; c < tables[b].Count ; c++)
                    {
                        CheckBox cbc = new CheckBox();
                        cbc.Content = tables[b][c].ToString();
                        cbc.Name = tables[b][c].ToString();
                        cbc.Checked += cbc_Checked;
                        cbc.Unchecked += cbc_UnChecked;
                        treeItem.Items.Add(new TreeViewItem() { Header = cbc });
                    }


                    tree_main.Items.Add(treeItem);
                }
            }
            else
            {
                //lbl_status.Content = "Connection Fail";
                MessageBox.Show("Connection Fail");
                grd_tables.IsEnabled = false;
                btn_makeConn.IsEnabled = false;
                tree_main.IsEnabled = false;
                ckbx_all.IsEnabled = false;
                ckbx_non.IsEnabled = false;
            }
       
        }

        // tree veiw event handlers-----------------------------------------------------------------------------------------------

        public void cbc_Checked(object sender, EventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox clicked_cb_table = (CheckBox)tree_items.Header;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    if ((tree_item.Header) == clicked_cb && clicked_cb_table.IsChecked == false)
                    {
                        clicked_cb_table.IsChecked = true;
                    }

                }


            }
        }

        public void cbc_UnChecked(object sender, EventArgs e)
        {

        }

        public void ckbx_all_Checked(object sender, EventArgs e)
        {
            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox cbt = (CheckBox)tree_items.Header;
                cbt.IsChecked = true;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    CheckBox cb = (CheckBox)tree_item.Header;
                    cb.IsChecked = true;
                }

            }
        }

        public void ckbx_non_Checked(object sender, EventArgs e)
        {
            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox cbt = (CheckBox)tree_items.Header;
                cbt.IsChecked = false;
                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    CheckBox cb = (CheckBox)tree_item.Header;
                    cb.IsChecked = false; ;
                }

            }
        }

        private void cbt_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_non.IsChecked = false;
            bool check_full = true;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb)
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox cb = (CheckBox)tree_item.Header;

                        if (cb.IsChecked == true)
                        {
                            check_full = false;
                        }

                    }
                }

            }

            if (check_full == true)
            {
                foreach (TreeViewItem tree_items in tree_main.Items)
                {
                    if ((tree_items.Header) == clicked_cb)
                    {
                        foreach (TreeViewItem tree_item in tree_items.Items)
                        {
                            CheckBox cb = (CheckBox)tree_item.Header;
                            cb.IsChecked = true;

                        }
                    }

                }
            }

        }

        private void cbt_UnChecked(object sender, RoutedEventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb)
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox cb = (CheckBox)tree_item.Header;
                        cb.IsChecked = false;
                    }
                }

            }
        }


        //functions------------------------------------------------------------------------------------------------------

        public List<string> LocalIPAddress()
        {
            IPHostEntry host;
            List<string> localIP = new List<string>();
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP.Add(ip.ToString());
                }
            }

            return localIP;
        }

        public void load()
        {
            List<string> ips = LocalIPAddress();

            foreach (string ip in ips)
            {
                cmb_serverIp.Items.Add(ip);
            }

        }        

        public List<List<string>> getChecked()
        {
            List<List<string>> save_items = new List<List<string>>();


            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox table_checked = (CheckBox)tree_items.Header;

                if (table_checked.IsChecked == true)
                {
                    List<string> save_table = new List<string>();

                    save_table.Add(table_checked.Content.ToString());

                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox column_checked = (CheckBox)tree_item.Header;

                        if (column_checked.IsChecked == true)
                        {
                            save_table.Add(column_checked.Content.ToString());
                        }

                    }

                    save_items.Add(save_table);
                }

            }            
            return save_items;
        }
        

    }
}
