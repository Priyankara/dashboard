﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for CreateSQLConnection.xaml
    /// </summary>
    public partial class CreateSQLConnections : Page
    {
        string myServer = Environment.MachineName;
        DataTable servers = SqlDataSourceEnumerator.Instance.GetDataSources();

        int server_size = 0;
        List<List<string>> tables = new List<List<string>>();
        int systemType = -1;

        public CreateSQLConnections()
        {
            InitializeComponent();
            load();
            cmb_servers.SelectionChanged += servers_selectionChange;
            cmb_instances.SelectionChanged += instances_selectionChange;
            cmb_ip.SelectionChanged += ip_selectionChange;
            cmb_ip.MouseDoubleClick += ip_selectionChange;
            txt_ip.TextChanged += ip_textchange;
            cmb_authentication_2.SelectionChanged += athentication_change_2;
            cmb_authentication.SelectionChanged += athentication_change;
            cmb_catelog.SelectionChanged += cmb_catelog_SelectionChanged;
            cmb_catelog_2.SelectionChanged += cmb_catelog_2SelectionChanged;

           // msktxt_port.TextChanged += port_txtChange;
            txt_ip.TextChanged += ip_txtChange;

            ckbx_all.Checked +=ckbx_all_Checked;
            ckbx_non.Checked +=ckbx_non_Checked;
                        
        }

        //event handlers----------------------------------------------------------------------------------------

        void cmb_catelog_2SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txt_sysName_2.Text.ToString() == "" || txt_sysName_2.Text.ToString()==null)
            {
                txt_sysName_2.Text = cmb_catelog_2.SelectedItem.ToString();
            }
        }

        void cmb_catelog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txt_sysName.Text.ToString() == "" || txt_sysName.Text.ToString() == null)
            {
                txt_sysName.Text= cmb_catelog.SelectedItem.ToString();
            }
        }      

        public void ip_txtChange(object sender, EventArgs e)
        {
            msktxt_port.Text = "1433";
            cmb_catelog_2.Items.Clear();
        }

        public void port_txtChange(object sender, EventArgs e)
        {
            string selected_server = "";
            string selected_instance = "";
            cmb_catelog_2.Items.Clear();
            try
            {
                selected_server = txt_ip.Text.ToString();
                selected_instance = msktxt_port.Text.ToString();

            }
            catch (Exception ee)
            {
            }

            SqlConnection con = new SqlConnection("Data Source=" + selected_server + "," + selected_instance + "; Integrated Security=True;");

            try
            {
                con.Open();
                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog_2.Items.Add(databaseName);

                }
                con.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void ip_textchange(object sender, EventArgs e)
        {
            List<string> ips = LocalIPAddress();
            bool match = false;

            if (ips.Contains(txt_ip.Text.ToString()))
            {
                match = true;
            }    
            
            cmb_authentication_2.Items.Clear();
            if (match == true)
            {
                try
                {
                    cmb_authentication_2.Items.Add("SQL Server Authentication");
                    cmb_authentication_2.Items.Add("Windows Authentication");
                }
                catch (Exception ee)
                {
                }
            }
            else
            {
                cmb_authentication_2.Items.Add("SQL Server Authentication");
            }    
            
           
        }

        public void ip_selectionChange(object sender, EventArgs e)
        {
            txt_ip.Clear();
            try
            {
                txt_ip.Text = cmb_ip.SelectedItem.ToString();
            }
            catch
            {
            }
        }

        public void instances_selectionChange(object sender, EventArgs e)
        {            
           
           

        }

        public void servers_selectionChange(object sender, EventArgs e)
        {
            string selected = cmb_servers.SelectedItem.ToString();
           
            cmb_instances.Items.Clear();
            
            for (int i = 0; i < server_size; i++)
            {
                if (servers.Rows[i].ItemArray[0].ToString() == selected)
                {
                    if (servers.Rows[i].ItemArray[1].ToString() == "")
                    {
                        cmb_instances.Items.Add("Default");
                    }
                    else
                    {
                        cmb_instances.Items.Add(servers.Rows[i].ItemArray[1].ToString());
                    }                    
                }               
            }

            cmb_authentication.Items.Clear();
                        
           /* if (selected.CompareTo(System.Net.Dns.GetHostName().ToString())!=1)
            {
                try
                {
                    cmb_authentication.Items.Add("SQL Server Authentication");
                }
                catch (Exception ee)
                {
                }
            }
            else 
            {
                cmb_authentication.Items.Add("SQL Server Authentication");
                cmb_authentication.Items.Add("Windows Authentication");                               
            }
            */

            //temp
            cmb_authentication.Items.Add("SQL Server Authentication");
            cmb_authentication.Items.Add("Windows Authentication"); 

        }

        private void athentication_change_2(object sender, SelectionChangedEventArgs e)
        {
            if (cmb_authentication_2.SelectedIndex == 0)
            {
                grd_sqlAuthenticate_2.IsEnabled = true;
            }
            else
            {
                grd_sqlAuthenticate_2.IsEnabled = false;
            }
        }

        private void athentication_change(object sender, SelectionChangedEventArgs e)
        {
            if (cmb_authentication.SelectedIndex == 0)
            {
                grd_sqlAuthenticate.IsEnabled = true;
            }
            else
            {
                grd_sqlAuthenticate.IsEnabled = false;
            }
        }

        //button clicks----------------------------------------------------------------------------------------------------------

        private void register_connection_Click(object sender, RoutedEventArgs e)
        {
            SQLConnections SQLC = new SQLConnections();
            List<List<string>> save_items = new List<List<string>>();
            save_items = getChecked();
            string status = "";

            string sysName = "";
            string serverName = "";
            string instanceName = "";
            string dbName = "";
            string athenticationType = "";
            string userName = "";
            string password = "";
            string serverIp = "";
            string port = "";

            if (systemType == 1)
            {
                if (cmb_instances.SelectedItem.ToString()!="Default")
                {
                    instanceName = cmb_instances.SelectedItem.ToString();
                }
                sysName = txt_sysName.Text.ToString();
                serverName = cmb_servers.SelectedItem.ToString();                
                dbName = cmb_catelog.SelectedItem.ToString();
                athenticationType = cmb_authentication.SelectedItem.ToString();
                userName = txt_username.Text.ToString();
                password = txt_password.Password.ToString();

                status = SQLC.create_SQLconnection(sysName, serverName, instanceName, dbName, athenticationType, userName, password, save_items, "", "", 1);
            }
            else if (systemType == 2)
            {
                sysName = txt_sysName_2.Text.ToString();
                dbName = cmb_catelog_2.SelectedItem.ToString();
                athenticationType = cmb_authentication_2.SelectedItem.ToString();
                userName = txt_username_2.Text.ToString();
                password = txt_password_2.Password.ToString();
                serverIp = txt_ip.Text.ToString();
                port = msktxt_port.Text.ToString();

                status = SQLC.create_SQLconnection(sysName, "", "", dbName, athenticationType, userName, password, save_items, serverIp, port, 2);
            }

            if (status == "saved")
            {
                MessageBox.Show("Successfully Saved");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving Failed");
            }
            else if (status == "exist")
            {
                MessageBox.Show("The system name is already exist");
            }

        }

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            WaitThread wt = new WaitThread();
            wt.WaitWindowShow();

            string selected_server = "";
            string selected_instance = "";
            cmb_catelog_2.Items.Clear();
            try
            {
                selected_server = txt_ip.Text.ToString();
                selected_instance = msktxt_port.Text.ToString();

            }
            catch (Exception ee)
            {
            }

            SqlConnection con = con = new SqlConnection(" Data Source =" + selected_server + "," + selected_instance + "; Network Library=DBMSSOCN; User ID=" + txt_username_2.Text.ToString() + "; Password=" + txt_password_2.Password.ToString() + "; Integrated Security=False;Encrypt=False");
      
            try
            {
                con.Open();
                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog_2.Items.Add(databaseName);

                }
                con.Close();
            }
            catch (Exception ex)
            {
            }

            wt.ThreadAbort();
        }

        private void btn_testConn_2_Click(object sender, RoutedEventArgs e)
        {
            tree_main.Visibility = Visibility.Visible;
            SQLConnections SQLConn = new SQLConnections();
            tables.Clear();

            try
            {
                tables = SQLConn.test_SQLconnection(txt_ip.Text.ToString(), msktxt_port.Text.ToString(), cmb_catelog_2.SelectedItem.ToString(), cmb_authentication_2.SelectedItem.ToString(), txt_username_2.Text.ToString(), txt_password_2.Password.ToString(), 2);
            }
            catch (Exception ee)
            {

            }

            if (tables.Capacity > 0 && tables != null)
            {
                grd_tables.IsEnabled = true;
                btn_makeConn.IsEnabled = true;
                systemType = 2;
                tree_main.IsEnabled = true;
                tree_main.Items.Clear();
                //lbl_status.Content = "Successful";
                //MessageBox.Show("Test Successful");
                ckbx_all.IsEnabled = true;
                ckbx_non.IsEnabled = true;

                genarateTree(tables);
            }
            else
            {
                //lbl_status.Content = "Connection Fail";
                MessageBox.Show("Connection Fail");
                grd_tables.IsEnabled = false;
                btn_makeConn.IsEnabled = false;
                tree_main.IsEnabled = false;
                ckbx_all.IsEnabled = false;
                ckbx_non.IsEnabled = false;
            }
        }

        private void btn_testConn_1_Click(object sender, RoutedEventArgs e)
        {
            tree_main.Visibility = Visibility.Visible;
            SQLConnections SQLConn = new SQLConnections();
            tables.Clear();
            string instance = "";           
            
            try
            {
                if (cmb_instances.SelectedItem.ToString() == "Default")
                {
                    instance = "";
                }
                else
                {
                    instance = cmb_instances.SelectedItem.ToString();
                }

                tables = SQLConn.test_SQLconnection(cmb_servers.SelectedItem.ToString(), instance, cmb_catelog.SelectedItem.ToString(), cmb_authentication.SelectedItem.ToString(), txt_username.Text.ToString(), txt_password.Password.ToString(), 1);
            }
            catch(Exception ee)
            {
                
            }                   

            if (tables.Capacity > 0 && tables != null)
            {
                grd_tables.IsEnabled = true;
                btn_makeConn.IsEnabled = true;
                systemType = 1;
                tree_main.IsEnabled = true;
                tree_main.Items.Clear();
               // lbl_status.Content = "Successful";
               // MessageBox.Show("Test Successful");
                ckbx_all.IsEnabled = true;
                ckbx_non.IsEnabled = true;

                genarateTree(tables);  
            }
            else
            {
               // lbl_status.Content = "Connection Fail";
                MessageBox.Show("Connection Fail");
                grd_tables.IsEnabled = false;
                btn_makeConn.IsEnabled = false;
                tree_main.IsEnabled = false;
                ckbx_all.IsEnabled = false;
                ckbx_non.IsEnabled = false;
            }
       
        }       

        //tree view event hadlers---------------------------------------------------------------------------------------- 

        public void cbc_Checked(object sender, EventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox clicked_cb_table = (CheckBox)tree_items.Header;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    if ((tree_item.Header) == clicked_cb && clicked_cb_table.IsChecked == false)
                    {
                        clicked_cb_table.IsChecked = true;
                    }
                    
                }
                

            }
        }

        public void cbc_UnChecked(object sender, EventArgs e)
        {
            ckbx_all.IsChecked = false;
        }

        public void ckbx_all_Checked(object sender, EventArgs e)
        {
            ckbx_non.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox cbt = (CheckBox)tree_items.Header;
                cbt.IsChecked = true;

                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    CheckBox cb = (CheckBox)tree_item.Header;
                    cb.IsChecked = true;
                }

            }
        }

        public void ckbx_non_Checked(object sender, EventArgs e)
        {
            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox cbt = (CheckBox)tree_items.Header;
                cbt.IsChecked = false;
                foreach (TreeViewItem tree_item in tree_items.Items)
                {
                    CheckBox cb = (CheckBox)tree_item.Header;
                    cb.IsChecked = false; ;
                }

            }
        }

        private void cbt_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_non.IsChecked = false;
            bool check_full = true;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb )
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox cb = (CheckBox)tree_item.Header;
                        
                        if (cb.IsChecked == true)
                        {
                            check_full = false;
                        }
                        
                    }
                }

            }

            if (check_full == true)
            {
                foreach (TreeViewItem tree_items in tree_main.Items)
                {
                    if ((tree_items.Header) == clicked_cb)
                    {
                        foreach (TreeViewItem tree_item in tree_items.Items)
                        {
                            CheckBox cb = (CheckBox)tree_item.Header;
                            cb.IsChecked = true;
                            
                        }
                    }

                }
            }

        }

        private void cbt_UnChecked(object sender, RoutedEventArgs e)
        {
            CheckBox clicked_cb = (CheckBox)sender;

            ckbx_all.IsChecked = false;

            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                if ((tree_items.Header) == clicked_cb )
                {
                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox cb = (CheckBox)tree_item.Header;
                        cb.IsChecked = false;
                    }
                }

            }
        }   

        //functions---------------------------------------------------------------------------------------------------------------

        public void genarateTree(List<List<string>> tables)
        {
            for (int b = 0; b < tables.Count; b++)
            {
                TreeViewItem treeItem = new TreeViewItem();// for tabel name 
                String TableName = tables[b][0].ToString();
                CheckBox cbt = new CheckBox();
                cbt.Content = TableName;
                cbt.Name = TableName;
                cbt.Checked += cbt_Checked;
                cbt.Unchecked += cbt_UnChecked;
                treeItem.Header = cbt;

                //get columns
                for (int c = 1; c < tables[b].Count ; c++)
                {
                    CheckBox cbc = new CheckBox();
                    cbc.Content = tables[b][c].ToString();
                    cbc.Name = tables[b][c].ToString();
                    cbc.Checked += cbc_Checked;
                    cbc.Unchecked += cbc_UnChecked;
                    treeItem.Items.Add(new TreeViewItem() { Header = cbc });
                }


                tree_main.Items.Add(treeItem);
            }

        }

        public List<List<string>> getChecked()
        {
            List<List<string>> save_items = new List<List<string>>();
            
            
            foreach (TreeViewItem tree_items in tree_main.Items)
            {
                CheckBox table_checked = (CheckBox)tree_items.Header;               

                if (table_checked.IsChecked == true)
                {
                    List<string> save_table = new List<string>();

                    save_table.Add(table_checked.Content.ToString());

                    foreach (TreeViewItem tree_item in tree_items.Items)
                    {
                        CheckBox column_checked = (CheckBox)tree_item.Header;

                        if (column_checked.IsChecked == true)
                        {
                            save_table.Add(column_checked.Content.ToString());
                        }

                    }

                    save_items.Add(save_table);
                }
           
            }

            return save_items;
        }        
        
        public List<string> LocalIPAddress()
        {
            IPHostEntry host;
            List<string> localIP = new List<string>();
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP.Add(ip.ToString());
                }
            }

            return localIP;
        }

        public void load()
        {
            List<string> ips = LocalIPAddress();

            foreach (string ip in ips)
            {
                cmb_ip.Items.Add(ip);
            }

            try
            {
                server_size = servers.Rows.Count;
            }
            catch (Exception e)
            {
            }
            for (int i = 0; i < server_size; i++)
            {
                if (!cmb_servers.Items.Contains(servers.Rows[i].ItemArray[0].ToString()))
                {
                    cmb_servers.Items.Add(servers.Rows[i].ItemArray[0].ToString());
                }

            }
        }

        private void btn_connect_Click(object sender, RoutedEventArgs e)
        {
            string selected_server = "";
            string selected_instance = "";
            cmb_catelog.Items.Clear();
            try
            {
                selected_server = cmb_servers.SelectedItem.ToString();
                selected_instance = cmb_instances.SelectedItem.ToString();
                if (selected_instance=="Default")
                {
                    selected_instance = "";
                }

            }
            catch (Exception ee)
            {
            }

            SqlConnection con=null;

            if (cmb_authentication.SelectedItem=="Windows Authentication")
            {
                con = new SqlConnection("Data Source =" + selected_server + " \\" + selected_instance + "; Network Library=DBMSSOCN; Integrated Security=True;Encrypt=False");
            }
            else if (cmb_authentication.SelectedItem == "SQL Server Authentication")
            {
                con = new SqlConnection("Data Source =" + selected_server + " \\" + selected_instance + "; Network Library=DBMSSOCN; User ID=" + txt_username.Text.ToString() + "; Password=" + txt_password.Password.ToString() + "; Integrated Security=False;Encrypt=False");
            }
          
            try
            {
                con.Open();
                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog.Items.Add(databaseName);

                }
                con.Close();
            }
            catch (Exception ex)
            {
            }
        }

    }

}
