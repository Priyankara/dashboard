﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace WpfToolkitChart.Priyankara
{
    class DbAccess
    {

        SqlConnection conn;

        public DbAccess()
        {
            conn = DbConnect.GetConnection();
        }

        public DataSet getStudentDetails()
        {
            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            SqlCommand newCmd = conn.CreateCommand();
            newCmd.Connection = conn;
            newCmd.CommandType = CommandType.Text;
            newCmd.CommandText = "  select * from ReportHistory1";

            SqlDataAdapter da = new SqlDataAdapter(newCmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "ReportHistory1");

            conn.Close();

            return ds;
        }

        public bool insertUser(string subTotal, string taxAmount, string totalDue)
        {
            bool status = false;

            if (conn.State.ToString() == "Closed")
            {
                conn.Open();
            }

            //SqlCommand newCmd1 = conn.CreateCommand();
            //newCmd1.Connection = conn;
            //newCmd1.CommandType = CommandType.Text;
            //newCmd1.CommandText = "select * from ReportHistory1 where taxAmount  ='" +  + "'";

            //SqlDataReader dr = newCmd1.ExecuteReader();
            //if (dr.HasRows)
            //{
            //    status = false;
            //}
            //else
            //{
            //    dr.Close();
                SqlCommand newCmd2 = conn.CreateCommand();

                newCmd2.Connection = conn;
                newCmd2.CommandType = CommandType.Text;
                newCmd2.CommandText = "  insert into ReportHistory1 values('" + subTotal +
                "','" + taxAmount + "','" + totalDue + "')";

                newCmd2.ExecuteNonQuery();

                status = true;
        
            return status;
        } 
    }
}
