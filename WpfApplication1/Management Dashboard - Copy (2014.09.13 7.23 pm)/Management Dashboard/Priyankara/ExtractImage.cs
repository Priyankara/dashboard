﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Runtime.ExceptionServices;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

namespace WpfToolkitChart.Priyankara
{

    class ExtractImage
    {

        Bitmap Extract(Bitmap bmp, int x1, int y1, int x2, int y2)
        {
            try
            {
                int width = x2 - x1;
                int height = y2 - y1;
                if (bmp == null || width < 1 || height < 1)
                {
                    return null;
                }

                Bitmap subImage = bmp.Clone(new System.Drawing.Rectangle(x1, y1, width, height), bmp.PixelFormat);
                return subImage;
            }
            catch
            {
                return null;
            }
        }
    }
}
