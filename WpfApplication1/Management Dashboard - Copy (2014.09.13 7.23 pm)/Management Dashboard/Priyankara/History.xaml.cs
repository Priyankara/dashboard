﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace WpfToolkitChart.Priyankara
{
    /// <summary>
    /// Interaction logic for History.xaml
    /// </summary>
    public partial class History : Window
    {
       // DbConnect dbcon =  new DbConnect();
        SqlConnection conn;
        public History()
        {
            conn = DbConnect.GetConnection();
            InitializeComponent();
            FillDataGrid();
        }


        private void FillDataGrid()
        {

            //SqlConnection conn = new SqlConnection(connction);
            //String ConString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
            //String cmdString = string.Empty;

            //using (SqlConnection con = new SqlConnection(ConString))
            //{
            //    cmdString = "SELECT Sub Total,Tax Amount, Total Due FROM Report History";
            //    SqlCommand cmd = new SqlCommand(cmdString, con);
            //    SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //    DataTable dt = new DataTable("Report History");
            //    sda.Fill(dt);
            //    gridEmp1.ItemsSource = dt.DefaultView;

            try 
	        {

                if (conn.State.ToString() == "Closed")
                {
                    conn.Open();
                }
                String query = "SELECT Sub_Total,Tax_Amount, Total_Due FROM ReportHistory1";
                SqlCommand createcommand = new SqlCommand(query,conn);
                createcommand.ExecuteNonQuery();

                SqlDataAdapter sqa  = new SqlDataAdapter(createcommand);
                DataTable dt = new DataTable("ReportHistory1");
                sqa.Fill(dt);
                datagrid.ItemsSource = dt.DefaultView;
                sqa.Update(dt);
                conn.Close();

	        }
	        catch (Exception EX)
	        {
		
		    MessageBox.Show(EX.Message);

	        }







            
        }
    }
}
