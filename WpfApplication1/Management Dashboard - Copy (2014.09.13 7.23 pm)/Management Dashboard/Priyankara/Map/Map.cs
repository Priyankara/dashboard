﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Loginnw.login;

namespace loadmap
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Map : Window
    {
        private string user_type = "";
        public Map(string userType)
        {
            InitializeComponent();
            user_type = userType;

            ImageBrush img12 = new ImageBrush(new BitmapImage(new Uri("C:\\Users\\DZN\\Documents\\Visual Studio 2010\\Projects\\loadmap\\map.jpg")));
            rectangle1.Fill = img12;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            string uriString = "http://priyankara.webuda.com/zerotype/gps/i-am-here.php";

            if (Uri.IsWellFormedUriString(uriString, UriKind.Absolute))
            {
                Uri uri = new Uri(uriString);
                webBrowser1.Navigate(uri);
            }
            else
            {
                // Logger.WriteEvent("invalid uriString: " + uriString);
                MessageBox.Show("Error");
            }

        }

        private void lnkbtn_home_Click(object sender, RoutedEventArgs e)
        {
            AdminMain am = new AdminMain(user_type);
            am.Show();
            this.Close();
        }

        private void lnkbtn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lnkbtn_signout_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
