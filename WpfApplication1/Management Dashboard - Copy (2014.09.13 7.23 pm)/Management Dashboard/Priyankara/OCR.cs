﻿using System;
using System.Windows;
using Microsoft.Win32;
using Puma.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;

using System.Drawing.Drawing2D;

using System.Windows.Media;
using System.Drawing;
using System.Diagnostics;
using Microsoft.Windows.Controls;
using System.Collections.Generic;
using WpfToolkitChart;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    /******************************************** 
    Extracting image
     
    *********************************************/
    public partial class OCR : Window
    {
        ExtractPart oc;
        public OCR()
        {
            InitializeComponent();
        }


        OpenFileDialog fdr = new OpenFileDialog();
        private void btn_SelectSource(object sender, RoutedEventArgs e)
        {

          //  fdr.ShowDialog();
            fdr.Multiselect = false;
              Nullable<bool> isSelected = fdr.ShowDialog();
              if (isSelected == true)
              {
                  string filepath = fdr.FileName;
                  txt_source.Text = filepath;
              }
              else {

                  MessageBox.Show("Invalid path");
              
              }

        }



        private void btn_SelectOutput(object sender, RoutedEventArgs e)
        {

            
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Multiselect = false;
            fileDialog.ShowDialog();
            string destinationpath = fileDialog.FileName;
            txt_destination.Text = destinationpath;

        }

        private void btn_View(object sender, RoutedEventArgs e)
        {


            string sourcepath = txt_source.Text.Trim();

            // Extracting part
           //// string result = "";
   
           // Bitmap newBmp = Extract(sourcepath, 5, 5, 3, 3);
           // String tempFile = CreateTempFile(newBmp);

            if (!string.IsNullOrEmpty(sourcepath))
            {
                PumaPage inputfile = new PumaPage(sourcepath);
                inputfile.FileFormat = PumaFileFormat.TxtAscii;
                inputfile.Language = PumaLanguage.English;
                string outputstring = inputfile.RecognizeToString();
                textBox3.Text = outputstring;
                inputfile.Dispose();


            }
        }

        private void btn_write(object sender, RoutedEventArgs e)
        {
            string sourcepath = txt_source.Text.Trim();
            string destinationpath = txt_destination.Text.Trim();
            if (!string.IsNullOrEmpty(sourcepath) & !string.IsNullOrEmpty(destinationpath))
            {
                PumaPage outputFile = new PumaPage(sourcepath);
                outputFile.FileFormat = PumaFileFormat.TxtAscii;
                outputFile.Language = PumaLanguage.English;
                outputFile.RecognizeToFile(destinationpath);
                outputFile.Dispose();
                MessageBox.Show("Written Successed");

            }
        }


        private void btn_Extract(object sender, RoutedEventArgs e)
        {
            oc = new ExtractPart();
            oc.Show();
            this.WindowState = WindowState.Minimized;
        }

       


    }
}
