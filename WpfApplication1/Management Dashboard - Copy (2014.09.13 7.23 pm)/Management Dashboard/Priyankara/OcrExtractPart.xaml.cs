﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;
using System.Runtime.ExceptionServices;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using WpfToolkitChart.Priyankara;
using Management_Dashboard;

namespace WpfToolkitChart
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  /// 
    /******************************************** 
    Extracting image part using cordinated is done in this code
      
    *********************************************/
  public partial class ExtractPart : Window
  {
  
    public ExtractPart()
    {
      InitializeComponent();
      showColumnChart();
    }



    private void btn_Process(object sender, RoutedEventArgs e)
    {
       
         this.WindowState = WindowState.Minimized;
      
         Process.Start("..\\..\\Priyankara\\screencap1.exe");

        //Thread.Sleep(5000);
       // this.WindowState = WindowState.Maximized;
    }

    private void btn_View(object sender, RoutedEventArgs e)
    {
        string result = "";
        Bitmap b = new Bitmap("..\\..\\Priyankara\\testBMP.jpeg");

        //calling Extract method
        // var newBmp = Extract(b , 1, 88, 290, 126);
        Bitmap newBmp = Extract(b, 816, 584, 1161, 612);
        if (newBmp == null)
        {
            result = string.Empty;
        }
        else
        {
            String tempFile = CreateTempFile(newBmp);
            result = OcrTempFile(tempFile);


            TextWriter tsw = new StreamWriter(@"..\\..\\Priyankara\\xmlWriter.txt"); // true without overriting


            //Writing text to the file.
            tsw.WriteLine(result);


            //Close the file.
            tsw.Close();
          //  MessageBox.Show("File written Successfully");


        }

        //converting text File to XML

        String[] data = File.ReadAllLines(@"..\\..\\Priyankara\\xmlWriter.txt");
             XElement root = new XElement("orders",
                                        from item in data
                                        select new XElement("Values", item));
             root.Save(@"..\\..\\Priyankara\\xmlWriter.xml");

        //Loading text File

             XDocument xmlDoc = XDocument.Load(@"..\\..\\Priyankara\\xmlWriter.xml");
             String[] test = xmlDoc.Descendants("orders").Elements("Values").Select(r => r.Value).ToArray();
             String result1 = string.Join("|", test);

             

             string fullName = result1;
             var names = fullName.Split('|');
             string firstName = names[0];
            string lastName = names[1];
            string ccc = names[2];
       // TextBox1.t
             txtUsername.Text = names[0];
             txtUsername1.Text = names[1];
             txtUsername2.Text = names[2];
          //  MessageBox.Show(ccc);

        // Early
        //string[] letters = result.Select(c => c.ToString()).ToArray();

        //textBox1.Text = Convert.ToString(result[0]);        

        }

        //Extracting part of the image 
        Bitmap Extract(Bitmap bmp, int x1, int y1, int x2, int y2)
        {
            try
            {
                int width = x2 - x1;
                int height = y2 - y1;
                if (bmp == null || width < 1 || height < 1)
                {
                    return null;
                }
                Bitmap subImage = bmp.Clone(new System.Drawing.Rectangle(x1, y1, width, height), bmp.PixelFormat);
                return subImage;
            }
            catch
            {
                return null;
            }
        }

        // save the newly created image into a temp file.
        private string CreateTempFile(Bitmap img) 
        {
            String fId = Guid.NewGuid().ToString("N");
            String path = string.Format("{0}{1}.tiff", System.IO.Path.GetTempPath(), fId); 
            img.Save(path, ImageFormat.Tiff);
          //  Globals.TempFiles.Add(path);
            return path; 
        }

     // read the data from the image.
        [HandleProcessCorruptedStateExceptions] 
        string OcrTempFile(string path) { 
            try { 

                MODI.Document Modi = new MODI.Document();
                Modi.Create(path);
                Modi.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, false, false);
                MODI.Image img = (MODI.Image)Modi.Images[0]; 
                MODI.Layout layout = img.Layout; 
                String str = layout.Text;
                Modi.Close(); 
                return str.Trim();

            } catch 
            { 
                return string.Empty;
            } 
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }



        private void showColumnChart()
        {
            
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            valueList.Add(new KeyValuePair<string, int>("Sub total", 20));
            valueList.Add(new KeyValuePair<string, int>("Tax Amount", 5));
            valueList.Add(new KeyValuePair<string, int>("Total Due", 70));


            //Setting data for column chart
            columnChart.DataContext = valueList;

            // Setting data for pie chart
            pieChart.DataContext = valueList;

            


        }

        private void textBox1_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //History his = new History();
          //  HistoryGraph his = new HistoryGraph();
           // his.Show();
         //   this.Close();
        }

        private void btn_temp_Click(object sender, RoutedEventArgs e)
        {
            ConnectionManagerMainWindow CMW = new ConnectionManagerMainWindow();
            CMW.Show();
        }
    
  }
}
