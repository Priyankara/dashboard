﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WpfToolkitChart.Priyankara
{
    /// <summary>
    /// Interaction logic for View_History.xaml
    /// </summary>
    public partial class View_History : Page
    {
        public View_History()
        {
            InitializeComponent();
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            String ConString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
            String cmdString = string.Empty;

            using (SqlConnection con = new SqlConnection(ConString))
            { 
                cmdString = "SELECT Sub Total,Tax Amount, Total Due FROM Report History";
                SqlCommand cmd = new SqlCommand(cmdString,con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("Report History");
                sda.Fill(dt);
                gridEmp.ItemsSource = dt.DefaultView;




            
            
            
            }
        }

        internal void ShowsNavigationUI()
        {
            throw new NotImplementedException();
        }
    }
}
