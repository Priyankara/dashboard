﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Management_Dashboard;
using WpfToolkitChart.Imeshe.Windows;
using Management_Dashboard.Windows;
using WpfApplication1;
using loadmap;

namespace Loginnw.login
{
    /// <summary>
    /// Interaction logic for AdminMain.xaml
    /// </summary>
    public partial class AdminMain : Window
    {
        private string user_type = "";

        public AdminMain(string userType)
        {
            InitializeComponent();
            user_type = userType;

            if (user_type=="Manager")
            {
                btn_blocking.IsEnabled = false;
                btn_conManager.IsEnabled = false;
                btn_configuration.IsEnabled = false;
                btn_createUser.IsEnabled = false;
                btn_queryBuild.IsEnabled = false;
                btn_repotr.IsEnabled = false;
                btn_updateUser.IsEnabled = false;
                btn_userLogs.IsEnabled = false;
            }
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OCR oep=new OCR();
            oep.Show();
            //this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            regUser rs = new regUser();
            rs.Show();
           // this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            UpdateUser cp = new UpdateUser();
            cp.Show();
            //this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ConnectionManagerMainWindow cmm=new ConnectionManagerMainWindow();
            cmm.Show();
            this.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            QueryBuilder qb=new QueryBuilder();
            qb.Show();
            this.Close();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            CreateDashBoardView cdbv=new CreateDashBoardView(user_type);
            cdbv.Show();
            this.Close();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            DashBoardWindow dbw = new DashBoardWindow(user_type,"");
            dbw.Show();
            this.Close();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            Map map = new Map(user_type);
            map.Show();
        }

        private void btn_configuration_Click(object sender, RoutedEventArgs e)
        {
            ConfiguratinWindow cw = new ConfiguratinWindow();
            cw.ShowDialog();
        }
    }
}
