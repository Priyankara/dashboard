﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for BarChartMain.xaml
    /// </summary>
    public partial class BarChartMain : Window
    {
        //CreateDashBoardView cdbv = new CreateDashBoardView();
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        List<string[]> queries = new List<string[]>();
        private CreateDashBoardView parent;
        
        public BarChartMain(CreateDashBoardView p)
        {
            InitializeComponent();

            parent = p;

            try
            {
                queries = ACM.getQueries();
            }
            catch { }

            for (int i = 0; i < queries.Count; i++)
            {
                cmb_singleQuery.Items.Add(queries[i][0].ToString());
            }
        }       

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if (i == 1)
            {
                tab_barchartProperties.SelectedIndex = 0;
                btn_back.IsEnabled = false;
                btn_next.IsEnabled = true;
                btn_finish.IsEnabled = false;
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if(i==0)
            {
                tab_barchartProperties.SelectedIndex = 1;
                btn_next.IsEnabled = false;
                btn_finish.IsEnabled = true;
                btn_back.IsEnabled = true;
                rbtn_multipleQuery.IsChecked = true;
            }        
        }

        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {
            string status = "";
            int type = 0;
            string chart_name=txt_chartName.Text.ToString();
            if (chart_name=="")
            {
                MessageBox.Show("Chart Name cannot be empty");
                return;
            }
            
            int width=1;
            int height=1;
            int bar_width=40;
            int refresh_period = 15;
            string description = "";

            TextRange textRange = new TextRange(rtxt_description.Document.ContentStart, rtxt_description.Document.ContentEnd);
            description = textRange.Text.ToString();

            try
            {
                width = Int32.Parse(lb_width.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                height = Int32.Parse(lb_height.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                bar_width = Int32.Parse(txt_barWidth.Text.ToString());
            }
            catch { }
            try
            {
                refresh_period = Int32.Parse(txt_refreshPeriod.Text.ToString());
            }
            catch { }

            string color=cmb_barColour.SelectedItem.ToString().Substring(38);
            string back_color = cmb_backColour.SelectedItem.ToString().Substring(38);
            string singel_query_name = "NULL";
            string x_axis="NULL";
            string y_axis = "NULL";

            List<string[]> bar_collection=new List<string[]>();
           
            if (rbtn_multipleQuery.IsChecked == true)
            {
                type = 1;
                x_axis = txt_xAxis.Text.ToString();
                y_axis = txt_yAxis.Text.ToString();

                WrapPanel n = new WrapPanel();
                TextBox bar_name=new TextBox();
                ComboBox query_name=new ComboBox();
                for (int i = 0; i < wrap_multipleQuery.Children.Count; i++)
                {
                    try
                    {
                        n = (WrapPanel)wrap_multipleQuery.Children[i];
                        bar_name=(TextBox)n.Children[1];
                        query_name=(ComboBox)n.Children[3];

                        string[] bar = new string[2];
                        bar[0] = bar_name.Text.ToString();
                        bar[1] = query_name.SelectedItem.ToString();

                        bar_collection.Add(bar);
                        
                    }
                    catch { }                    
                }
            }
            else
            {
                type = 2;
                try
                {
                    singel_query_name = cmb_singleQuery.SelectedItem.ToString();
                }
                catch
                {
                }
            }

            status = ACM.saveComponent(chart_name, "BarChart", width, height, refresh_period, type, bar_width.ToString(), color, back_color, x_axis, y_axis, description, singel_query_name, bar_collection);

            if(status=="exist")
            {
                MessageBox.Show("Chart Name alrady exist, tyr another name");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving failed. Check database connectivity");
            }
            else if (status == "saved")
            {
                BarChart bc = new BarChart();
                bc.generateChart(chart_name, width, height, bar_width, color, back_color, refresh_period, x_axis, y_axis, bar_collection, singel_query_name, type, description);
                parent.getComponent("BarChart", bc);
                parent.finish_clicked = true;
                this.Close();
            }       
           
        }

        private void btn_close_Click(object sender, RoutedEventArgs e)
        {
            parent.finish_clicked = false;
            this.Close();
        }
        
        private void rbtn_multipleQuery_Checked(object sender, RoutedEventArgs e)
        {
            btn_addBar.IsEnabled = true;
            wrap_multipleQuery.IsEnabled = true;
            cmb_singleQuery.IsEnabled = false;
        }

        private void rbtn_singleQuery_Checked(object sender, RoutedEventArgs e)
        {
            btn_addBar.IsEnabled = false;
            wrap_multipleQuery.IsEnabled = false;
            cmb_singleQuery.IsEnabled = true;
        }

        private void btn_addBar_Click(object sender, RoutedEventArgs e)
        {
            WrapPanel wp = new WrapPanel();

            Label barName = new Label();
            barName.Content = "Bar Name";

            Button delete = new Button();
            delete.Content = "X";
            delete.Click += delete_Click;
            delete.Width = 30;

            TextBox bar_name = new TextBox();
            bar_name.Text = "bar1";
            bar_name.Width = 130;

            Label queryName = new Label();
            queryName.Content = "Query Name";

            ComboBox query = new ComboBox();
            query.Width = 110;
            query.SelectedIndex = 0;
            for (int i = 0; i < queries.Count; i++)
            {
                query.Items.Add(queries[i][0].ToString());
            }

            wp.Children.Add(barName);
            wp.Children.Add(bar_name);
            wp.Children.Add(queryName);
            wp.Children.Add(query);
            wp.Children.Add(delete);

            wrap_multipleQuery.Children.Add(wp);
        }

        void delete_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            WrapPanel wp = (WrapPanel)btn.Parent;
            WrapPanel n = new WrapPanel();


            for (int i = 0; i < wrap_multipleQuery.Children.Count; i++)
            {
                try
                {
                     n = (WrapPanel)wrap_multipleQuery.Children[i];
                }
                catch { }
                if (n == wp)
                {
                    wrap_multipleQuery.Children.Remove(wp);
                }
            }
            
        }       
       
    }
}
