﻿using Management_Dashboard.Pages.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for ChartMain.xaml
    /// </summary>
    public partial class ChartMain : Window
    {
        //CreateDashBoardView cdbv = new CreateDashBoardView();
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        List<string[]> queries = new List<string[]>();
        private CreateDashBoardView parent;
        private string component = "";
        
        public ChartMain(CreateDashBoardView p, string Component)
        {
            InitializeComponent();

            parent = p;
            component = Component;

            try
            {
                queries = ACM.getQueries();
            }
            catch { }

            for (int i = 0; i < queries.Count; i++)
            {
                cmb_singleQuery.Items.Add(queries[i][0].ToString());
            }
        }
       

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if (i == 1)
            {
                tab_barchartProperties.SelectedIndex = 0;
                btn_back.IsEnabled = false;
                btn_next.IsEnabled = true;
                btn_finish.IsEnabled = false;
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if(i==0)
            {
                tab_barchartProperties.SelectedIndex = 1;
                btn_next.IsEnabled = false;
                btn_finish.IsEnabled = true;
                btn_back.IsEnabled = true;
                rbtn_multipleQuery.IsChecked = true;
            }        
        }

        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {
            string status = "";
            int type = 0;
            string chart_name=txt_chartName.Text.ToString();
            if (chart_name=="")
            {
                MessageBox.Show("Chart Name cannot be empty");
                return;
            }
            
            int width=1;
            int height=1;
            int refresh_period = 15;
            string x = txt_xaxis.Text.ToString();
            string y = txt_yaxis.Text.ToString();
            string description = "";

            TextRange textRange = new TextRange(rtxt_description.Document.ContentStart, rtxt_description.Document.ContentEnd);
            description = textRange.Text.ToString();

            try
            {
                width = Int32.Parse(lb_width.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                height = Int32.Parse(lb_height.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                refresh_period = Int32.Parse(txt_refreshPeriod.Text.ToString());
            }
            catch { }

            string back_color = cmb_backColour.SelectedItem.ToString().Substring(38);
            string singel_query_name = "NULL";
      
            List<string[]> query_collection=new List<string[]>();
           
            if (rbtn_multipleQuery.IsChecked == true)
            {
                type = 1;
    
                WrapPanel n = new WrapPanel();
                TextBox section_name=new TextBox();
                ComboBox query_name=new ComboBox();
                for (int i = 0; i < wrap_multipleQuery.Children.Count; i++)
                {
                    try
                    {
                        n = (WrapPanel)wrap_multipleQuery.Children[i];
                        section_name = (TextBox)n.Children[1];
                        query_name=(ComboBox)n.Children[3];

                        string[] section = new string[2];
                        section[0] = section_name.Text.ToString();
                        section[1] = query_name.SelectedItem.ToString();

                        query_collection.Add(section);
                        
                    }
                    catch { }                    
                }
            }
            else
            {
                type = 2;
                try
                {
                    singel_query_name = cmb_singleQuery.SelectedItem.ToString();
                }
                catch
                {
                }
            }

            status = ACM.saveComponent(chart_name, component, width, height, refresh_period, type, back_color, x, y, null, null, description, singel_query_name, query_collection);

            if(status=="exist")
            {
                MessageBox.Show("Chart Name alrady exist, tyr another name");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving failed. Check database connectivity");
            }
            else if (status == "saved")
            {
                ComponentFram cf = new ComponentFram(component);
                cf.generateChart(chart_name, width, height, back_color, refresh_period, query_collection, singel_query_name, type, component, x, y, description);
                parent.getComponent(component, cf);                                               
                parent.finish_clicked = true;
                this.Close();
            }            
        }

        private void btn_close_Click(object sender, RoutedEventArgs e)
        {
            parent.finish_clicked = false;
            this.Close();
        }
        
        private void rbtn_multipleQuery_Checked(object sender, RoutedEventArgs e)
        {
            btn_addBar.IsEnabled = true;
            wrap_multipleQuery.IsEnabled = true;
            cmb_singleQuery.IsEnabled = false;
        }

        private void rbtn_singleQuery_Checked(object sender, RoutedEventArgs e)
        {
            btn_addBar.IsEnabled = false;
            wrap_multipleQuery.IsEnabled = false;
            cmb_singleQuery.IsEnabled = true;
        }

        private void btn_addBar_Click(object sender, RoutedEventArgs e)
        {
            WrapPanel wp = new WrapPanel();

            Label sectionName = new Label();
            sectionName.Content = "Name";

            Button delete = new Button();
            delete.Content = "X";
            delete.Click += delete_Click;
            delete.Width = 30;

            TextBox section_name = new TextBox();
            section_name.Text = "1";
            section_name.Width = 130;

            Label queryName = new Label();
            queryName.Content = "Query Name";

            ComboBox query = new ComboBox();
            query.Width = 110;
            query.SelectedIndex = 0;
            for (int i = 0; i < queries.Count; i++)
            {
                query.Items.Add(queries[i][0].ToString());
            }

            wp.Children.Add(sectionName);
            wp.Children.Add(section_name);
            wp.Children.Add(queryName);
            wp.Children.Add(query);
            wp.Children.Add(delete);

            wrap_multipleQuery.Children.Add(wp);
        }

        void delete_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            WrapPanel wp = (WrapPanel)btn.Parent;
            WrapPanel n = new WrapPanel();


            for (int i = 0; i < wrap_multipleQuery.Children.Count; i++)
            {
                try
                {
                     n = (WrapPanel)wrap_multipleQuery.Children[i];
                }
                catch { }
                if (n == wp)
                {
                    wrap_multipleQuery.Children.Remove(wp);
                }
            }
            
        }
    }
}
