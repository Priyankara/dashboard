﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for ConfiguratinWindow.xaml
    /// </summary>
    public partial class ConfiguratinWindow : Window
    {
        string myServer = Environment.MachineName;
        DataTable servers = new DataTable();
        int server_size = 0;

        public ConfiguratinWindow()
        {
            InitializeComponent();
            
            cmb_servers.SelectionChanged += servers_selectionChange;
            cmb_instances.SelectionChanged += instances_selectionChange;
            cmb_ip.SelectionChanged += ip_selectionChange;
            cmb_ip.MouseDoubleClick += ip_selectionChange;
            txt_ip.TextChanged += ip_textchange;
            cmb_authentication_2.SelectionChanged += athentication_change_2;
            cmb_authentication.SelectionChanged += athentication_change;       
        }

        //button click------------------------------------------------------------------------------------------------------------------------

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            tab_configuration.SelectedIndex=0;
            btn_back.IsEnabled = false;
            btn_next.IsEnabled = true;
            btn_finish.IsEnabled = false;
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        { 
            btn_back.IsEnabled = true;
            btn_next.IsEnabled = false;
            btn_finish.IsEnabled = true;

            if (rbtn_default.IsChecked == true)
            {
                tab_configuration.SelectedIndex =1;

                if (servers.Rows.Count == 0)
                {
                    WaitThread wt = new WaitThread();
                    wt.WaitWindowShow();
                    servers = SqlDataSourceEnumerator.Instance.GetDataSources();
                    wt.ThreadAbort();
                }
                try
                {
                    lbl_server.Content = servers.Rows[0].ItemArray[0].ToString();
                }
                catch { }
            }
            else if (rbtn_existing.IsChecked == true)
            {
                if (servers.Rows.Count == 0)
                {
                    WaitThread wt = new WaitThread();
                    wt.WaitWindowShow();
                    servers = SqlDataSourceEnumerator.Instance.GetDataSources();
                    wt.ThreadAbort();
                }
                tab_configuration.SelectedIndex = 2;
                lbl_databaseName.Visibility = Visibility.Collapsed;
                txt_databaseName.Visibility = Visibility.Collapsed;
                lbl_catelog.Visibility = Visibility.Visible;
                cmb_catelog.Visibility = Visibility.Visible;

                lbl_databaseName1.Visibility = Visibility.Collapsed;
                txt_databaseName1.Visibility = Visibility.Collapsed;
                lbl_catalog1.Visibility = Visibility.Visible;
                cmb_catelog_2.Visibility = Visibility.Visible;
                lbl_title.Content = "Connect Existing Database";
                if (cmb_servers.HasItems == false)
                {
                    load();
                }
            }
            else if (rbtn_new.IsChecked == true)
            {
                if (servers.Rows.Count == 0)
                {
                    WaitThread wt = new WaitThread();
                    wt.WaitWindowShow();
                    servers = SqlDataSourceEnumerator.Instance.GetDataSources();
                    wt.ThreadAbort();
                }
                tab_configuration.SelectedIndex = 2;
                lbl_catelog.Visibility = Visibility.Collapsed;
                cmb_catelog.Visibility = Visibility.Collapsed;
                lbl_databaseName.Visibility = Visibility.Visible;
                txt_databaseName.Visibility = Visibility.Visible;

                lbl_catalog1.Visibility = Visibility.Collapsed;
                cmb_catelog_2.Visibility = Visibility.Collapsed;
                lbl_databaseName1.Visibility = Visibility.Visible;
                txt_databaseName1.Visibility = Visibility.Visible;
                lbl_title.Content = "Cerate New Database";
                if (cmb_servers.HasItems == false)
                {
                    load();
                }
            }
            else if (rbtn_view.IsChecked == true)
            {
                tab_configuration.SelectedIndex = 3;

                string[] connection = SetConfigFile(null, "GET");

                string datasource = connection[0].ToString();
                string database = connection[1].ToString();
                string username = connection[2].ToString();
                string password = connection[3].ToString();
                
                lbl_currentCatelog.Content = database.Substring((database.IndexOf('=')+1),(database.Length-(database.IndexOf('=')+1)));
                lbl_currentServer.Content = datasource.Substring((datasource.IndexOf('=') + 1), (datasource.Length - (datasource.IndexOf('=') + 1)));                
                lbl_currentUsername.Content = username.Substring((username.IndexOf('=')+1),(username.Length-(username.IndexOf('=')+1)));
                pwdbx_currentPassword.Password = password.Substring((password.IndexOf('=')+1),(password.Length-(password.IndexOf('=')+1)));

                if (datasource == ".")
                {
                    lbl_currentServer.Content = "Default";
                }

                btn_finish.IsEnabled = false;
            }   
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
           this.Close();           
        }
        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {
            if(rbtn_default.IsChecked==true)
            {
                string constr = "Data Source=.; Initial Catalog=ManagementDashBoard; User ID=; Password=; Integrated Security=True";

                if (testConnection(constr) == true)
                {
                    SetConfigFile(constr, "SET");
                }
                else
                {
                    MessageBox.Show("Connecting to the default database fail.");
                }
            }
            else if(rbtn_existing.IsChecked==true)
            {
                string constr="";

                if (cmb_catelog.SelectedIndex == -1 && cmb_catelog_2.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a database catalog.");
                    return;
                }

                if (tab_sqlConType.SelectedIndex ==0)
                {
                    string instance = "";
                    if (cmb_instances.SelectedItem.ToString() != "Default")
                    {
                        instance = cmb_instances.SelectedItem.ToString();
                    }
                    constr = "Data Source =" + cmb_servers.SelectedItem.ToString() + " \\" + instance + "; Initial Catalog=" + cmb_catelog.SelectedItem.ToString() + "; User ID=" + txt_username.Text.ToString() + "; Password=" + txt_password.Password.ToString() + "; Integrated Security=True";                   
                }
                else if (tab_sqlConType.SelectedIndex ==1)
                {
                    constr = "Data Source =" + txt_ip.Text.ToString() + "," + msktxt_port.Text.ToString() + "; Initial Catalog=" + cmb_catelog_2.SelectedItem.ToString() + "; User ID=" + txt_username_2.Text.ToString() + "; Password=" + txt_password_2.Password.ToString() + "; Integrated Security=True";
                }                

                if (testConnection(constr) == true)
                {
                    SetConfigFile(constr, "SET");
                }
                else
                {
                    MessageBox.Show("Connecting to new database fail");
                }
            }
            else if (rbtn_new.IsChecked == true)
            {
                string constr_toServer = "";
                string constr_toNewDb = "";
                string databaseName = "";

                if (txt_databaseName.Text.ToString() == null && txt_databaseName1.Text.ToString() == null)
                {
                    MessageBox.Show("Please enter a database name.");
                    return;
                }

                if (tab_sqlConType.SelectedIndex == 0)
                {
                    string instance = "";
                    databaseName = txt_databaseName.Text.ToString();

                    if (cmb_instances.SelectedItem.ToString() != "Default")
                    {
                        instance = cmb_instances.SelectedItem.ToString();
                    }

                    constr_toServer = "Data Source =" + cmb_servers.SelectedItem.ToString() + " \\" + instance + "; User ID=" + txt_username.Text.ToString() + "; Password=" + txt_password.Password.ToString() + "; Integrated Security=True";
                    constr_toNewDb = "Data Source =" + cmb_servers.SelectedItem.ToString() + " \\" + instance + "; Initial Catalog=" + databaseName + "; User ID=" + txt_username.Text.ToString() + "; Password=" + txt_password.Password.ToString() + "; Integrated Security=True";
                }
                else if (tab_sqlConType.SelectedIndex == 1)
                {
                    databaseName = txt_databaseName1.Text.ToString();
                    constr_toServer = "Data Source =" + txt_ip.Text.ToString() + "," + msktxt_port.Text.ToString() + "; User ID=" + txt_username_2.Text.ToString() + "; Password=" + txt_password_2.Password.ToString() + "; Integrated Security=True";
                    constr_toNewDb = "Data Source =" + txt_ip.Text.ToString() + "," + msktxt_port.Text.ToString() + "; Initial Catalog=" + databaseName + "; User ID=" + txt_username_2.Text.ToString() + "; Password=" + txt_password_2.Password.ToString() + "; Integrated Security=True";
                }

                if (createDatabase(constr_toServer, constr_toNewDb, databaseName) == true)
                {
                    SetConfigFile(constr_toNewDb, "SET");
                }
                else
                {
                    MessageBox.Show("New database not created");
                }
            }
        }

        private void btn_test_Click(object sender, RoutedEventArgs e)
        {
            string selected_server = "";
            string selected_instance = "";
            cmb_catelog.Items.Clear();
            try
            {
                selected_server = cmb_servers.SelectedItem.ToString();
                selected_instance = cmb_instances.SelectedItem.ToString();
                if (selected_instance == "Default")
                {
                    selected_instance = "";
                }

            }
            catch (Exception ee)
            {
            }

            SqlConnection con = null;

            if (cmb_authentication.SelectedItem.ToString() == "Windows Authentication")
            {
                con = new SqlConnection("Data Source =" + selected_server + " \\" + selected_instance + "; Network Library=DBMSSOCN; Integrated Security=True;Encrypt=False");
            }
            else if (cmb_authentication.SelectedItem.ToString() == "SQL Server Authentication")
            {
                con = new SqlConnection("Data Source =" + selected_server + " \\" + selected_instance + "; Network Library=DBMSSOCN; User ID=" + txt_username.Text.ToString() + "; Password=" + txt_password.Password.ToString() + "; Integrated Security=False;Encrypt=False");
            }

            try
            {
                con.Open();
                MessageBox.Show("Test successful.");
                lbl_catelog.IsEnabled = true;
                cmb_catelog.IsEnabled = true;
                lbl_databaseName.IsEnabled = true;
                txt_databaseName.IsEnabled = true;

                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog.Items.Add(databaseName);
                }
                txt_databaseName.Text = "ManagementDashBoard";
                con.Close();
            }
            catch (Exception ex)
            {
                lbl_catelog.IsEnabled = false;
                cmb_catelog.IsEnabled = false;
                lbl_databaseName.IsEnabled = false;
                txt_databaseName.IsEnabled = false;
                MessageBox.Show("Test Fail.");
            }
        }

        private void btn_test1_Click(object sender, RoutedEventArgs e)
        {
            string selected_server = "";
            string selected_instance = "";
            cmb_catelog_2.Items.Clear();
            try
            {
                selected_server = txt_ip.Text.ToString();
                selected_instance = msktxt_port.Text.ToString();

            }
            catch (Exception ee)
            {
            }

            SqlConnection con = con = new SqlConnection(" Data Source =" + selected_server + "," + selected_instance + "; Network Library=DBMSSOCN; User ID=" + txt_username_2.Text.ToString() + "; Password=" + txt_password_2.Password.ToString() + "; Integrated Security=False;Encrypt=False");

            try
            {
                con.Open();
                MessageBox.Show("Test successful.");

                lbl_catalog1.IsEnabled = true;
                cmb_catelog_2.IsEnabled = true;
                lbl_databaseName1.IsEnabled = true;
                txt_databaseName1.IsEnabled = true;

                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog_2.Items.Add(databaseName);
                }
                txt_databaseName1.Text = "ManagementDashBoard";
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Test Fail.");
                lbl_catalog1.IsEnabled = false;
                cmb_catelog_2.IsEnabled = false;
                lbl_databaseName1.IsEnabled = false;
                txt_databaseName1.IsEnabled = false;
            }
        }

        //methods-------------------------------------------------------------------------------------------------------------------------------------------------

        public bool createDatabase(string constr, string constr_toDb, string database)
        {
           
                /* string sqlConnectionString = @"Integrated Security=SSPI;Persist Security Info=False;Data Source=.\SQLEXPRESS";
            FileInfo file = new FileInfo(@"D:\SLIIT\Year 3\Semester 2\SEP II\Project\Work\Imeshe\Management Dashboard - Copy (2014.05.16)\Q1.sql");
            string script = file.OpenText().ReadToEnd();
            SqlConnection conn = new SqlConnection(sqlConnectionString);
            Microsoft.SqlServer.Management.Smo.Server newServer = new Microsoft.SqlServer.Management.Smo.Server(new Microsoft.SqlServer.Management.Common.ServerConnection(conn));
            newServer.ConnectionContext.ExecuteNonQuery(script);
            file.OpenText().Close();*/

            try
            {
                SqlConnection conn1 = new SqlConnection(constr);
                conn1.Open();
                DataTable databases = conn1.GetSchema("Databases");           
                foreach (DataRow db in databases.Rows)
                {
                    if (db.Field<String>("database_name")==database)
                    {
                        MessageBox.Show("Database name is alrady exist.");
                        return false;
                    }
                }
                conn1.Close();
            }
            catch (Exception ex)
            {
                return false;
            }

            try
            {
                SqlConnection conn = new SqlConnection(constr);
                conn.Open();
                SqlCommand newCmd = conn.CreateCommand();
                newCmd.Connection = conn;
                newCmd.CommandType = CommandType.Text;
                newCmd.CommandText = "CREATE DATABASE " + database + ";";
                newCmd.ExecuteNonQuery();
                newCmd.Dispose();
                conn.Close();
            }
            catch
            {
                MessageBox.Show("Connecting to the server instance fail");
                return false;
            }

            try
            {
                SqlConnection conn2 = new SqlConnection(constr_toDb);
                conn2.Open();
                SqlCommand newCmd2 = conn2.CreateCommand();
                newCmd2.Connection = conn2;
                newCmd2.CommandType = CommandType.Text;
                newCmd2.CommandText = "create table ConectedSystem(" +
                                    "systemName varchar(200)," +
                                    "DBName varchar(150)," +
                                    "serverName varchar(150)," +
                                    "instanceName varchar(150)," +
                                    "authenticationType varchar(150)," +
                                    "username varchar(100)," +
                                    "password varchar(100)," +
                                    "ip varchar(20)," +
                                    "port char(5)," +
                                    "type varchar(30)," +
                                    "activated int default 0," +
                                    "CONSTRAINT CS_pk PRIMARY KEY (systemName))" +

                                    "create table SysTableColumn(" +
                                    "systemName varchar(200)," +
                                    "TableName varchar(200)," +
                                    "columnName varchar(300)," +

                                    "CONSTRAINT ST_pk PRIMARY KEY (systemName,TableName,columnName))" +

                                    "create table SysExcelFiles(" +
                                    "systemName varchar(200)," +
                                    "fileName varchar(300)," +
                                    "sheetName varchar(200)," +
                                    "columnName varchar(200)," +
                                    "CONSTRAINT SEF_pk PRIMARY KEY (systemName,fileName,sheetName,columnName))" +

                                    "create table Query(" +
                                    "queryName varchar(200)," +
                                    "systemName varchar(200)," +
                                    "queryType varchar(50)," +
                                    "query varchar(2000)," +
                                    "CONSTRAINT Q_pk PRIMARY KEY (queryName))" +

                                    "create table DashBoardView(" +
                                    "viewName varchar(200)," +
                                    "width real default (1192.0)," +
                                    "height real default (615.0)," +
                                    "CONSTRAINT DBV_pk PRIMARY KEY (viewName))" +

                                    "create table Component(" +
                                    "componentName varchar(200)," +
                                    "componentType varchar(200)," +
                                    "width int default 1," +
                                    "height int default 1," +
                                    "refreshRate int default 0," +
                                    "type int," +
                                    "field1 varchar(300), " +
                                    "field2 varchar(300), " +
                                    "field3 varchar(300), " +
                                    "field4 varchar(300), " +
                                    "field5 varchar(300), " +
                                    "field6 varchar(1000), " +
                                    "CONSTRAINT C_pk PRIMARY KEY (componentName))" +

                                    "create table ViewComponent(" +
                                    "componentName varchar(200)," +
                                    "viewName varchar(200)," +
                                    "location_x real," +
                                    "location_y real," +
                                    "scale_x real," +
                                    "scale_y real," +
                                    "CONSTRAINT VC_pk PRIMARY KEY (componentName,viewName)," +
                                    "CONSTRAINT VC_fk1 FOREIGN KEY(componentName) REFERENCES Component(componentName)," +
                                    "CONSTRAINT VC_fk2 FOREIGN KEY(viewName) REFERENCES DashBoardView(viewName))" +

                                    "create table QueryComponent(" +
                                    "componentName varchar(200)," +
                                    "queryName varchar(200)," +
                                    "fieldName varchar(200)," +
                                    "IdNumber int IDENTITY(1,1)," +
                                    "CONSTRAINT QC_pk PRIMARY KEY (IdNumber)," +
                                    "CONSTRAINT QC_fk1 FOREIGN KEY(componentName) REFERENCES Component(componentName)," +
                                    "CONSTRAINT QC_fk2 FOREIGN KEY(queryName) REFERENCES Query(queryName))";

                newCmd2.ExecuteNonQuery();
                newCmd2.Dispose();
                conn2.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }        

        public bool testConnection(string conn)
        {
            SqlConnection testCon = new SqlConnection(conn);
            string[] original_tables={"ConectedSystem","SysTableColumn","SysExcelFiles","Query","DashBoardView","Component","ViewComponent","QueryComponent"};

            try
            {                
                testCon.Open();
                List<string> table = new List<string>();                

                try
                {
                    SqlCommand newCmd = testCon.CreateCommand();
                    newCmd.Connection = testCon;
                    newCmd.CommandType = CommandType.Text;
                    newCmd.CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables";
                    SqlDataReader dr = newCmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (dr[0].ToString() == "sysdiagrams")
                        {
                            continue;
                        }
                        table.Add(dr[0].ToString());
                    }

                    foreach (string tbl in original_tables)
                    {
                        if (table.Contains(tbl))
                        {
                        }
                        else
                        {
                            MessageBox.Show("Selected Database is not matched");
                            testCon.Close();
                            return false;                            
                        }
                    }

                    testCon.Close();
                    return true;
                }
                catch
                {
                    testCon.Close();
                    MessageBox.Show("Connected. But cannot access selected database");
                    return false;
                }               
            }
            catch 
            {
                testCon.Close();
                MessageBox.Show("Connection Fail");
                return false;
            }            
        }

        public string[] SetConfigFile(string con, string operation)
        {
            //updating config file
            XmlDocument XmlDoc = new XmlDocument();
            //Loading the Config file
            XmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement xElement in XmlDoc.DocumentElement)
            {
                if (xElement.Name == "connectionStrings")
                {
                    if (operation=="SET")
                    {
                        //setting the coonection string                  
                        xElement.FirstChild.Attributes[1].Value = con;
                        try
                        {
                            XmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                            MessageBox.Show("Application configuration is Changed. Changes will not take effect until the application is restarted.");
                        }
                        catch
                        {
                            MessageBox.Show("Configuration Not Changed");
                        }
                        return null;
                    }
                    else if (operation == "GET")
                    {
                        string conStrn="";
                        string[] connection_property = new string[5];

                        //get the coonection string   
                        conStrn=xElement.FirstChild.Attributes[1].Value.ToString();
                        connection_property = conStrn.Split(';');                    

                        return connection_property;
                    }                                     
                }
            }
            return null;
        }

        public List<string> LocalIPAddress()
        {
            IPHostEntry host;
            List<string> localIP = new List<string>();
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP.Add(ip.ToString());
                }
            }

            return localIP;
        }

        public void load()
        {
            List<string> ips = LocalIPAddress();

            foreach (string ip in ips)
            {
                cmb_ip.Items.Add(ip);
            }

            try
            {
                server_size = servers.Rows.Count;
            }
            catch (Exception e)
            {
            }
            for (int i = 0; i < server_size; i++)
            {
                if (!cmb_servers.Items.Contains(servers.Rows[i].ItemArray[0].ToString()))
                {
                    cmb_servers.Items.Add(servers.Rows[i].ItemArray[0].ToString());
                }

            }
        }



        //event handlers----------------------------------------------------------------------------------------

        public void ip_txtChange(object sender, EventArgs e)
        {
            msktxt_port.Text = "1433";
            cmb_catelog_2.Items.Clear();
        }

        public void port_txtChange(object sender, EventArgs e)
        {
            string selected_server = "";
            string selected_instance = "";
            cmb_catelog_2.Items.Clear();
            try
            {
                selected_server = txt_ip.Text.ToString();
                selected_instance = msktxt_port.Text.ToString();

            }
            catch (Exception ee)
            {
            }

            SqlConnection con = new SqlConnection("Data Source=" + selected_server + "," + selected_instance + "; Integrated Security=True;");

            try
            {
                con.Open();
                DataTable databases = con.GetSchema("Databases");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("database_name");
                    cmb_catelog_2.Items.Add(databaseName);

                }
                con.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void ip_textchange(object sender, EventArgs e)
        {
            msktxt_port.Text = "1433";
            cmb_catelog_2.Items.Clear();

            List<string> ips = LocalIPAddress();
            bool match = false;

            if (ips.Contains(txt_ip.Text.ToString()))
            {
                match = true;
            }

            cmb_authentication_2.Items.Clear();
            if (match == true)
            {
                try
                {
                    cmb_authentication_2.Items.Add("SQL Server Authentication");
                    cmb_authentication_2.Items.Add("Windows Authentication");
                }
                catch (Exception ee)
                {
                }
            }
            else
            {
                cmb_authentication_2.Items.Add("SQL Server Authentication");
            }
        }

        public void ip_selectionChange(object sender, EventArgs e)
        {
            txt_ip.Clear();
            try
            {
                txt_ip.Text = cmb_ip.SelectedItem.ToString();
            }
            catch
            {
            }
        }

        public void instances_selectionChange(object sender, EventArgs e)
        {



        }

        public void servers_selectionChange(object sender, EventArgs e)
        {
            string selected = cmb_servers.SelectedItem.ToString();

            cmb_instances.Items.Clear();

            for (int i = 0; i < server_size; i++)
            {
                if (servers.Rows[i].ItemArray[0].ToString() == selected)
                {
                    if (servers.Rows[i].ItemArray[1].ToString() == "")
                    {
                        cmb_instances.Items.Add("Default");
                    }
                    else
                    {
                        cmb_instances.Items.Add(servers.Rows[i].ItemArray[1].ToString());
                    }
                }
            }

            cmb_authentication.Items.Clear();
            cmb_authentication.Items.Add("SQL Server Authentication");
            cmb_authentication.Items.Add("Windows Authentication");
        }

        private void athentication_change_2(object sender, SelectionChangedEventArgs e)
        {
            if (cmb_authentication_2.SelectedIndex == 0)
            {
                grd_sqlAuthenticate_2.IsEnabled = true;
            }
            else
            {
                grd_sqlAuthenticate_2.IsEnabled = false;
            }
        }

        private void athentication_change(object sender, SelectionChangedEventArgs e)
        {
            if (cmb_authentication.SelectedIndex == 0)
            {
                grd_sqlAuthenticate.IsEnabled = true;
            }
            else
            {
                grd_sqlAuthenticate.IsEnabled = false;
            }
        }        
    }
}
