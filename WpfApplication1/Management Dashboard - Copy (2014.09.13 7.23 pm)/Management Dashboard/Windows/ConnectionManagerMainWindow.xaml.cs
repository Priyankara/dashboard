﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Management_Dashboard.Pages;
using Management_Dashboard.Windows;
using Loginnw.login;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ConnectionManagerMainWindow: Window
    {
        public ConnectionManagerMainWindow()
        {
            InitializeComponent();
            ConnectionManagerHome1 cmh1 = new ConnectionManagerHome1();
            mainFram.Content = cmh1;          
        }
        //---------------------------------------------------------------------------------------
        private void add_new_connection_Click(object sender, RoutedEventArgs e)
        {
            lbl_screen.Content = "Register New System";
            add_new_connection.Visibility =Visibility.Hidden;

            AddNewConnection anc = new AddNewConnection();           
            mainFram.Content = anc;
        }

        private void create_databind_Click(object sender, RoutedEventArgs e)
        {
            lbl_screen.Content = "Create Data Set";
            add_new_connection.Visibility = Visibility.Hidden;

            CreateDataBind CDB = new CreateDataBind();
            mainFram.Content = CDB;
        }

        //-------------------------------------------------------------------------------------
        private void lnkbtn_signout_Click(object sender, RoutedEventArgs e)
        {
        }

        private void lnkbtn_home_Click(object sender, RoutedEventArgs e)
        {
            AdminMain am = new AdminMain("Admin");
            am.Show();
            this.Close();
        }

        private void lnkbtn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            ConnectionManagerHome1 cmh = new ConnectionManagerHome1();
            mainFram.Content = cmh;
            lbl_screen.Content = "Connection Manager Home";
            add_new_connection.Visibility = Visibility.Visible;
        }
    }
}
