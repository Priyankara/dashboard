﻿using Management_Dashboard.Pages.Components;
using Microsoft.Windows.Controls.Primitives;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for CreateComponent.xaml
    /// </summary>
    public partial class CreateComponent : Window
    {
        List<string[]> sysList;
        string componentType = "";
        string DragingTable = "";
        string DragingColumn = "";
        string DragingColumnTable = "";
        StackPanel stackPanel_TableMove = new StackPanel();
        Canvas stackPanel_ColumnMove = new Canvas();
        WrapPanel dragingView = new WrapPanel();

        TableConnector DraginConnector = null;
        List<object> connectors = new List<object>();

        double dragingCurrentX = 0;
        double dragingCurrentY = 0;
        string Key = "";
        string Value = "";
        bool groupBy = false;
        bool orderBy = false;
        int aggregation = -1;
        string condition = "";
        string AgrCondition = "";
        string chartQuery = "";

        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        SQLConnections sqlc = new SQLConnections();
        MYSQlConnections mysqlc = new MYSQlConnections();
        ExcelConnections excelc = new ExcelConnections();
        CreateDashBoardView parent;

        ComponentFram cf = null;
        BarChart bc = null;
        Gauge gg = null;

        Label lbl_key = null;
        Label lbl_value = null;
        Frame frm_chartContainer = new Frame();

        //--------------------------------------------------------------------FUNCTIONS----------------------------------------------------------------------

        public CreateComponent(string component_type, CreateDashBoardView p)
        {
            InitializeComponent();

            componentType = component_type;
            parent = p;
            cmb_system.SelectionChanged+=cmb_system_SelectionChanged;

            sysList = ACM.getsystems();
            
            stackPanel_TableMove.MouseLeftButtonUp += stackPanel_TableMove_MouseLeftUp;
            stackPanel_ColumnMove.MouseLeftButtonUp += stackPanel_ColumnMove_MouseLeftUp;

            for (int i = 0; i < sysList.Count; i++)
            {
                cmb_system.Items.Add(sysList[i][0].ToString());
            }

            createChartTemplate(componentType);
        }

        public void createChartTemplate(string componentType)
        {
            frm_chartContainer=new Frame();

            if(componentType=="BarChart")
            {
                bc = new BarChart();
                bc.generateChart(txt_chartName.Text.ToString(), null, componentType, "X", "Y");
                bc.lbl_deleteX.MouseLeftButtonDown += lbl_deleteX_MouseLeftButtonDown;
                bc.lbl_deleteY.MouseLeftButtonDown += lbl_deleteY_MouseLeftButtonDown;
                bc.lbl_deleteX.MouseEnter += lbl_delete_MouseEnter;
                bc.lbl_deleteY.MouseEnter += lbl_delete_MouseEnter;
                bc.lbl_deleteX.MouseLeave += lbl_delete_MouseLeave;
                bc.lbl_deleteY.MouseLeave += lbl_delete_MouseLeave;

                lbl_key = bc.lbl_key;
                lbl_value = bc.lbl_value;

                lbl_barColor.IsEnabled = true;
                lbl_barWidth.IsEnabled = true;
                txt_barWidth.IsEnabled = true;
                cmb_barColour.IsEnabled = true;

                frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / 600), ScaleY = (253.0 / 450) };
                frm_chartContainer.Content = bc;
            }
            else if (componentType == "Gauge")
            {
                gg = new Gauge();
                gg.generateChart(txt_chartName.Text.ToString(), null, componentType);
                gg.lbl_deleteX.MouseLeftButtonDown += lbl_deleteX_MouseLeftButtonDown;
                gg.lbl_deleteY.MouseLeftButtonDown += lbl_deleteY_MouseLeftButtonDown;
                gg.lbl_deleteX.MouseEnter += lbl_delete_MouseEnter;
                gg.lbl_deleteY.MouseEnter += lbl_delete_MouseEnter;
                gg.lbl_deleteX.MouseLeave += lbl_delete_MouseLeave;
                gg.lbl_deleteY.MouseLeave += lbl_delete_MouseLeave;

                lbl_key = gg.lbl_key;
                lbl_value = gg.lbl_value;

                frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / 450), ScaleY = (253.0 / 350) };
                frm_chartContainer.Content = gg;
            }
            else
            {
                cf = new ComponentFram(componentType);
                cf.generateChart(txt_chartName.Text.ToString(), null, componentType, "X", "Y");
                cf.lbl_deleteX.MouseLeftButtonDown += lbl_deleteX_MouseLeftButtonDown;
                cf.lbl_deleteY.MouseLeftButtonDown += lbl_deleteY_MouseLeftButtonDown;
                cf.lbl_deleteX.MouseEnter += lbl_delete_MouseEnter;
                cf.lbl_deleteY.MouseEnter += lbl_delete_MouseEnter;
                cf.lbl_deleteX.MouseLeave += lbl_delete_MouseLeave;
                cf.lbl_deleteY.MouseLeave += lbl_delete_MouseLeave;

                lbl_key = cf.lbl_x;
                lbl_value = cf.lbl_y;

                frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / cf.Width), ScaleY = (253.0 / 300) };
                frm_chartContainer.Content = cf;
            }   

            canvase_chart.Children.Clear();
            canvase_chart.Children.Add(frm_chartContainer);            
        }

        private void initialize()
        {
            wrap_tables.Children.Clear();
            canvas.Children.Clear();
            grid_chartTable.ItemsSource = null;
            createChartTemplate(componentType);
            chkbox_groupbyKey.IsChecked = false;
            chkbox_orderbyKey.IsChecked = false;
            chkbox_valueAggr.IsChecked = false;
            chkbox_valueCondition.IsChecked = false;
            chkbox_valueAgrCondition.IsChecked = false;
            cmb_aggrigation.SelectedIndex = 0;
            cmb_AgrOperator.SelectedIndex = 0;
            cmb_operator.SelectedIndex = 0;
            txt_AgrCondition.Text = "";
            txt_condition.Text = "";
                        
            
            DragingTable = "";
            DragingColumn = "";
            DragingColumnTable = "";
            //stackPanel_TableMove = new StackPanel();
            dd.Children.Remove(stackPanel_ColumnMove);
            canvas.Children.Remove(dragingView);
            
            DraginConnector = null;
            connectors.Clear();

            //dragingCurrentX = 0;
            //dragingCurrentY = 0;
            Key = "";
            Value = "";
            groupBy = false;
            orderBy = false;
            aggregation = -1;
            condition = "";
            AgrCondition = "";
            chartQuery = "";
        }

        public void genarateTableTree(List<List<string>> tables, string db)
        {
            TreeView tree = new TreeView();
            tree.Width = 350;
            tree.Background = new SolidColorBrush(Colors.Transparent);

            TreeViewItem treeItems = new TreeViewItem();
            String DBName = db;
            TextBlock cbts = new TextBlock();
            cbts.Text = DBName;
            treeItems.Header = cbts;

            for (int b = 0; b < tables.Count; b++)
            {
                ContextMenu mnu_all = new ContextMenu();
                MenuItem view_all = new MenuItem();
                MenuItem add_all = new MenuItem();
                mnu_all.Items.Add(add_all);
                add_all.Header = "Add All";
                mnu_all.Items.Add(view_all);
                view_all.Header = "View All";

                TreeViewItem treeItem = new TreeViewItem();
                String TableName = tables[b][0].ToString();
                Label cbt = new Label();
                cbt.MouseLeftButtonDown += Tree_label_MouseLeftDown;
                TextBlock cbtt = new TextBlock();
                cbtt.Text = TableName;
                cbt.ContextMenu = mnu_all;

                cbt.Content = cbtt;
                treeItem.Header = cbt;

                for (int c = 1; c < tables[b].Count; c++)
                {
                    ContextMenu mnu = new ContextMenu();
                    MenuItem view = new MenuItem();
                    MenuItem add = new MenuItem();
                    mnu.Items.Add(add);
                    add.Header = "Add";
                    mnu.Items.Add(view);
                    view.Header = "View";

                    TextBlock cbcc = new TextBlock();
                    cbcc.Text = tables[b][c].ToString();
                    Label cbc = new Label();
                    cbc.Content = cbcc;

                    treeItem.Items.Add(new TreeViewItem() { Header = cbc });
                }
                treeItems.Items.Add(new TreeViewItem() { Header = treeItem });
            }
            tree.Items.Add(treeItems);
            wrap_tables.Children.Add(tree);
        }

        private void createTableview(string tableName, string systemName)
        {
            DataTable dt = new DataTable();
            DataGrid dgv = new DataGrid();
            WrapPanel wp = new WrapPanel();
            StackPanel stp = new StackPanel();
            Label close = new Label();
            Label drage = new Label();
            Label table_name = new Label();

            dgv.Loaded += dgv_Loaded;

            string selectedDbType = "";

            for (int i = 0; i < sysList.Count; i++)
            {
                if (sysList[i][0].ToString() == systemName)
                {
                    selectedDbType = sysList[i][2].ToString();
                }
            }

            if (selectedDbType == "SQL")
            {
                dt = sqlc.getSQLdata(systemName, tableName, "");
            }
            else if (selectedDbType == "MYSQL")
            {
                dt = mysqlc.getMYSQLData(systemName, tableName, "");
            }
            else if (selectedDbType == "Excel")
            {
                dt = excelc.getExcelData(systemName, tableName, "");
            }

            wp.MouseEnter += wp_MouseLeaveEnter;
            wp.MouseLeave += wp_MouseLeaveEnter;
            close.MouseEnter += LableMouseEnter;
            close.MouseLeave += LableMouseLeave;
            close.MouseLeftButtonDown += close_MouseLeftButtonDown;
            drage.MouseEnter += LableMouseEnter;
            drage.MouseLeave += LableMouseLeave;
            drage.MouseLeftButtonDown += drage_MouseLeftButtonDown;
            scroll_view.MouseLeftButtonUp += canvas_scroll_MouseLeftButtonUp;
            scroll_view.MouseMove += canvas_scroll_MouseMove;
            stp.Visibility = Visibility.Collapsed;
            close.Background = new SolidColorBrush(Colors.Blue);
            drage.Background = new SolidColorBrush(Colors.Yellow);
            table_name.Content = tableName;
            table_name.FontSize = 15;
            table_name.Foreground = new SolidColorBrush(Colors.White);
            drage.Content = "..";
            close.Content = "X";
            dgv.ItemsSource = dt.DefaultView;
            stp.Children.Add(close);
            stp.Children.Add(drage);

            var newX = Mouse.GetPosition(canvas).X;
            var newY = Mouse.GetPosition(canvas).Y;
            wp.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX, newY);
            wp.Children.Add(table_name);
            wp.Children.Add(dgv);
            wp.Children.Add(stp);
            canvas.Children.Add(wp);

            dgv.SelectionUnit = DataGridSelectionUnit.Cell;
            dgv.CanUserSortColumns = false;
            dgv.CanUserAddRows = false;
            dgv.CanUserDeleteRows = false;
            dgv.CanUserReorderColumns = false;
            dgv.IsReadOnly = true;
        }

        public void createChartTableView(string systemName)
        {
            DataTable dt = new DataTable();            
            try
            {
                createQuery();
                btn_refresh.IsEnabled = true;
                btn_finish.IsEnabled = true;
            }
            catch 
            {
                string tempQuery = "";

                if (Key != "" && Value != "")
                {                    
                    btn_refresh.IsEnabled = true;
                    btn_finish.IsEnabled = true;
                    string from = "";

                    if (Key.Substring(0, Key.IndexOf('.')) == Value.Substring(0, Value.IndexOf('.')))
                    {
                        from = Value.Substring(0, Value.IndexOf('.'));
                    }
                    tempQuery = "select " + Key + " , " + Value + " from " + from + " "+from;
                }
                else if (Key != "")
                {
                    tempQuery = "select " + Key + " from " + Key.Substring(0, Key.IndexOf('.')) + " " + Key.Substring(0, Key.IndexOf('.'));
                }
                else if (Value != "")
                {
                    tempQuery = "select " + Value + " from " + Value.Substring(0, Value.IndexOf('.')) + " " + Value.Substring(0, Value.IndexOf('.'));
                }

                dt = RunQuery(systemName, tempQuery);
                grid_chartTable.ItemsSource = dt.DefaultView;

                if(componentType=="BarChart")
                {
                    bc.grd_barChart.Children.Clear();
                   // frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / bc.Width), ScaleY = (253.0 / 450) };
                   
                }
                else if (componentType == "Gauge")
                {
                    gg.Initialize();
                    frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / 450), ScaleY = (253.0 / 350) };
                }
                else
                {
                    cf.chart.Series.Clear();
                    cf.chart.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / cf.chart.Width), ScaleY = (253.0 / cf.chart.Height) };
                }
            }
            
        }       

        public void createQuery()
        {
            DataTable dt = new DataTable();
            string select = "";
            string from = " from ";
            string where = "";
            string groupby = "";
            string orderby = "";
            string having = "";
            string Query = "";
            chartQuery = "";

            List<string> fromList = new List<string>();

            fromList.Add(Key.Substring(0, Key.IndexOf('.')));
            if (!fromList.Contains(Value.Substring(0, Value.IndexOf('.'))))
            {
                fromList.Add(Value.Substring(0, Value.IndexOf('.')));
            }
            
            if (groupBy == true)
            {
                groupby = " group by " + Key;
            }
            if (orderBy == true)
            {
                orderby = " order by " + Key;
            }

            if (condition != "")
            {
                where = " where " + Value + " " + condition; ;
            }
            else
            {
                where = " where 1=1";
            }

            foreach(TableConnector tc in connectors)
            {   
                if ((tc.startTable == Key.Substring(0, Key.IndexOf('.'))) || (tc.endTable == Key.Substring(0, Key.IndexOf('.'))) || (tc.startTable == Value.Substring(0, Value.IndexOf('.'))) || (tc.endTable == Value.Substring(0, Value.IndexOf('.'))))
                {
                    where = where + " and " + tc.startTable + "." + tc.startColumn + "=" + tc.endTable + "." + tc.endColumn;
                    if (!fromList.Contains(tc.startTable))
                    {
                        fromList.Add(tc.startTable);
                    }
                    if (!fromList.Contains(tc.endTable))
                    {
                        fromList.Add(tc.endTable);
                    }  
                }
            }

            foreach(string frm in fromList)
            {
                if (from == " from ")
                {
                    from = from + " " + frm + " " + frm;
                }
                else
                {
                    from = from + " ," + frm + " " + frm;
                }
            }

            if (aggregation != -1)
            {
                string value = "";

                if (aggregation == 0)
                {
                    value = "Count(" + Value + ")";
                }
                else if (aggregation == 1)
                {
                    value = "Sum(" + Value + ")";
                }
                else if (aggregation == 2)
                {
                    value = "Min(" + Value + ")";
                }
                else if (aggregation == 3)
                {
                    value = "Max(" + Value + ")";
                }
                else if (aggregation == 4)
                {
                    value = "Avg(" + Value + ")";
                }

                select = "select " + Key + " ," + value;

                if (AgrCondition != "")
                {
                    having = " having " + value + " " + AgrCondition;
                }
                else
                {
                    having = " having 1=1";
                }
            }
            else
            {
                select = "select " + Key + " ," + Value;
            }

            Query = select + from + where + groupby + having + orderby; 
            chartQuery = Query;
            dt = RunQuery(cmb_system.SelectedItem.ToString(), Query);


            if(componentType =="BarChart")
            {
                bc.grd_barChart.Children.Clear();
                bc.generateChart(txt_chartName.Text.ToString(), dt, componentType, txt_xaxis.Text.ToString(), txt_yaxis.Text.ToString());
                frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / bc.Width), ScaleY = (253.0 / 450) };
                bc.lbl_value.Content = Value.Substring(Value.IndexOf('.') + 1, (Value.Length - (Value.IndexOf('.') + 1)));
                bc.lbl_value.Background = new SolidColorBrush(Colors.LawnGreen);
                bc.lbl_key.Content = Key.Substring(Key.IndexOf('.') + 1, (Key.Length - (Key.IndexOf('.') + 1)));
                bc.lbl_key.Background = new SolidColorBrush(Colors.LawnGreen);                    
            }
            else if (componentType == "Gauge")
            {
                gg.Initialize();
                gg.generateChart(txt_chartName.Text.ToString(), dt, componentType);
                frm_chartContainer.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / gg.Width), ScaleY = (253.0 / 350) };
                gg.lbl_value.Content = Value.Substring(Value.IndexOf('.') + 1, (Value.Length - (Value.IndexOf('.') + 1)));
                gg.lbl_value.Background = new SolidColorBrush(Colors.LawnGreen);
                gg.lbl_key.Content = Key.Substring(Key.IndexOf('.') + 1, (Key.Length - (Key.IndexOf('.') + 1)));
                gg.lbl_key.Background = new SolidColorBrush(Colors.LawnGreen); 
            }
            else
            {
                cf.generateChart(txt_chartName.Text.ToString(), dt, componentType, txt_xaxis.Text.ToString(), txt_yaxis.Text.ToString());
                cf.chart.LayoutTransform = new ScaleTransform() { ScaleX = (406.0 / cf.chart.Width), ScaleY = (253.0 / cf.chart.Height) };
                cf.lbl_y.Content = Value.Substring(Value.IndexOf('.') + 1, (Value.Length - (Value.IndexOf('.') + 1)));
                cf.lbl_y.Background = new SolidColorBrush(Colors.LawnGreen);
                cf.lbl_x.Content = Key.Substring(Key.IndexOf('.') + 1, (Key.Length - (Key.IndexOf('.') + 1)));
                cf.lbl_x.Background = new SolidColorBrush(Colors.LawnGreen);
            }            

            grid_chartTable.ItemsSource = dt.DefaultView;
        }

        public DataTable RunQuery(string systemName, string Query)
        {
            DataTable dt = new DataTable();
            string selectedDbType = "";

            for (int i = 0; i < sysList.Count; i++)
            {
                if (sysList[i][0].ToString() == systemName)
                {
                    selectedDbType = sysList[i][2].ToString();
                }
            }

            if (selectedDbType == "SQL")
            {
                dt = sqlc.RunQuery(systemName, Query);
            }
            else if (selectedDbType == "MYSQL")
            {
                dt = mysqlc.RunQuery(systemName, Query);
            }
            else if (selectedDbType == "Excel")
            {
                dt = excelc.RunQuery(systemName, Query);
            }
            return dt;
        }

        //--------------------------------------------------------------------EVENTS-------------------------------------------------------------------------

        //---------------------Chart Template Events-------------------------

        void lbl_delete_MouseLeave(object sender, MouseEventArgs e)
        {
            ((Label)sender).Background = new SolidColorBrush(Colors.White);
        }

        void lbl_delete_MouseEnter(object sender, MouseEventArgs e)
        {
            ((Label)sender).Background = new SolidColorBrush(Colors.Red);
        }

        void lbl_deleteY_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbl_value.Content = "Value";
            lbl_value.Background = new SolidColorBrush(Colors.LightBlue);
            Value = "";
            try
            {
                createChartTableView(cmb_system.SelectedItem.ToString());
            }
            catch { }
            btn_refresh.IsEnabled = false;
            btn_finish.IsEnabled = false;
        }

        void lbl_deleteX_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbl_key.Content = "Key";
            lbl_key.Background = new SolidColorBrush(Colors.LightBlue);
            Key = "";
            try
            {
                createChartTableView(cmb_system.SelectedItem.ToString());
            }
            catch { }
            btn_refresh.IsEnabled = false;
            btn_finish.IsEnabled = false;
        }

        //---------------------Select System Events-------------------------

        private void cmb_system_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            initialize();
            fillWrapPanel(cmb_system.SelectedItem.ToString(), sysList[cmb_system.SelectedIndex][2]);
        }   

        private void fillWrapPanel(string system, string sysType)
        {
            ApplicationConnectionManager ACM = new ApplicationConnectionManager();

            List<List<string>> tables = new List<List<string>>();

            if (sysType == "Excel")
            {
                tables = ACM.getexceltabelcolumn(ApplicationConnectionManager.GetConnection(), system);
            }
            else
            {
                tables = ACM.getsystabelcolumn(ApplicationConnectionManager.GetConnection(), system);
            }

            genarateTableTree(tables, system);
        }

        //---------------------Source Table Adding Events-------------------------

        private void Tree_label_MouseLeftDown(object sender, MouseButtonEventArgs e)
        {
            ((Label)sender).Background = new SolidColorBrush(Colors.LimeGreen);
            DragingTable = ((TextBlock)(((Label)sender).Content)).Text.ToString();

            stackPanel_TableMove.Width = 100;
            stackPanel_TableMove.Height = 100;
            ImageBrush img = new ImageBrush(new BitmapImage(new Uri("D:\\!#SLIIT#\\3rd year 2nd sem\\#SEP\\Management Dashboard - Copy (2014.05.16)\\Management Dashboard\\Resources\\table.PNG", UriKind.Relative)));
            stackPanel_TableMove.Opacity = 0.3;
            stackPanel_TableMove.Background = img;

            var newX = Mouse.GetPosition(dd).X;
            var newY = Mouse.GetPosition(dd).Y;
            var width = (this.Width) / 2;
            var height = (this.Height) / 2;
            //MessageBox.Show((this.Width).ToString());

            stackPanel_TableMove.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX - (width) + 50, newY - (height) + 50);
            dd.Children.Add(stackPanel_TableMove);
        }

        private void stackPanel_TableMove_MouseLeftUp(object sender, MouseButtonEventArgs e)
        {
            var mouseX = Mouse.GetPosition(canvas).X;
            var mouseY = Mouse.GetPosition(canvas).Y;

            if ((mouseX > 0 && mouseX < 1032) && (mouseY > 0 && mouseY < 407))
            {
                foreach (WrapPanel wp in canvas.Children)
                {
                    if (((Label)(wp.Children[0])).Content.ToString() == DragingTable)
                    {
                        DragingTable = "";
                        dd.Children.Remove(stackPanel_TableMove);
                        MessageBox.Show("Table alrady exist");
                        return;
                    }
                }
                createTableview(DragingTable, cmb_system.SelectedItem.ToString());
                dd.Children.Remove(stackPanel_TableMove);                
            }
            else
            {
                DragingTable = "";
                dd.Children.Remove(stackPanel_TableMove);
            }
        }

        private void grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && DragingTable != "")
            {
                var newX = Mouse.GetPosition(dd).X;
                var newY = Mouse.GetPosition(dd).Y;
                var width = (this.Width) / 2;
                var height = (this.Height) / 2;
                stackPanel_TableMove.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX - (width) + 50, newY - (height) + 50);
            }
            if (e.LeftButton == MouseButtonState.Pressed && DragingColumn != "")
            {

                var newX = Mouse.GetPosition(dd).X;
                var newY = Mouse.GetPosition(dd).Y;
                var width = (this.Width) / 2;
                var height = (this.Height) / 2;
                stackPanel_ColumnMove.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX - (width) + 40, newY - (height) + 15);
            }

        }        

        void canvas_scroll_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            dragingView = null;
            dragingCurrentX = 0;
            dragingCurrentY = 0;
        }       

        //---------------------Added Source Table Events-------------------------

        private void dgv_Loaded(object sender, RoutedEventArgs e)
        {
            DataGrid dg = ((DataGrid)sender);
            
            for (int i = 0; i < dg.Columns.Count; i++)
            {
                WrapPanel headerContent = new WrapPanel();
                Label headerText = new Label();
                Ellipse headerNode = new Ellipse();
                headerNode.Name = ((Label)(((WrapPanel)(dg.Parent)).Children[0])).Content.ToString();
                headerNode.Width = 10;
                headerNode.Height = 10;
                headerNode.Fill = new SolidColorBrush(Colors.Red);
                headerText.MinWidth = 60;
                headerText.Content = dg.Columns[i].Header.ToString();    
                headerText.Name = ((Label)(((WrapPanel)(dg.Parent)).Children[0])).Content.ToString();

                headerText.MouseLeftButtonDown += Header_LeftDown;
                headerNode.MouseLeftButtonDown += headerNode_MouseLeftButtonDown;
              
                headerContent.Children.Add(headerNode);
                headerContent.Children.Add(headerText);
                dg.Columns[i].Header = headerContent;
            }            
        }
        
        private void dd_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(DraginConnector!=null)
            {
                canvas.Children.Remove(DraginConnector.line);
                DraginConnector.node1.Fill = new SolidColorBrush(Colors.Red);
                DraginConnector = null;                
            }
        }

        void headerNode_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var mouseX = Mouse.GetPosition(canvas).X;
            var mouseY = Mouse.GetPosition(canvas).Y;

            
            string senderTable=((Ellipse)sender).Name.ToString();
            string senderColumn =((Label)(((WrapPanel)(((Ellipse)sender).Parent)).Children[1])).Content.ToString();
            
            //MessageBox.Show(senderColumn);

            if(DraginConnector==null)
            {
                TableConnector tc = new TableConnector();
                tc.line.X1 = mouseX;
                tc.line.Y1 = mouseY;
                tc.line.X2 = mouseX;
                tc.line.Y2 = mouseY;
                tc.startTable = senderTable;
                tc.startColumn = senderColumn;
                ((Ellipse)sender).Fill = new SolidColorBrush(Colors.Blue);
                tc.node1 = ((Ellipse)sender);
                DraginConnector = tc;
                
                canvas.Children.Add(tc.line);
            }
            else if (DraginConnector != null && DraginConnector.startTable != senderTable)
            {
                double closeX=0;
                double closeY=0;
                DraginConnector.line.X2 = mouseX;
                DraginConnector.line.Y2 = mouseY;
                DraginConnector.node2 = ((Ellipse)sender);
                ((Ellipse)sender).Fill = new SolidColorBrush(Colors.Green);
                DraginConnector.node1.Fill = new SolidColorBrush(Colors.Green); 
                DraginConnector.endTable = senderTable;
                DraginConnector.endColumn = senderColumn;
                
                if(DraginConnector.line.X1<DraginConnector.line.X2)
                {
                    closeX=((DraginConnector.line.X2 - DraginConnector.line.X1) / 2.0)+DraginConnector.line.X1;
                }
                else
                {
                    closeX=((DraginConnector.line.X1 - DraginConnector.line.X2) / 2.0)+DraginConnector.line.X2;
                }
                if(DraginConnector.line.Y1<DraginConnector.line.Y2)
                {
                    closeY=((DraginConnector.line.Y2 - DraginConnector.line.Y1) / 2.0)+DraginConnector.line.Y1;
                }
                else
                {
                    closeY=((DraginConnector.line.Y1 - DraginConnector.line.Y2) / 2.0)+DraginConnector.line.Y2;
                }

                DraginConnector.close.RenderTransform = new TranslateTransform(closeX,closeY);
                canvas.Children.Add(DraginConnector.close);
                DraginConnector.close.MouseLeftButtonDown += connector_remove_Click;               
                connectors.Add(DraginConnector);
                DraginConnector = null;
            }        
        }

        void connector_remove_Click(object sender, MouseButtonEventArgs ee) 
        {
            foreach(TableConnector tc in connectors)
            {
                if (tc.close == ((Label)sender))
                {
                    connectors.Remove(tc);
                    canvas.Children.Remove(tc.line);
                    canvas.Children.Remove(tc.close);
                    tc.node1.Fill = new SolidColorBrush(Colors.Red);
                    tc.node2.Fill = new SolidColorBrush(Colors.Red);
                    return;
                }
            }
        }

        void stackPanel_ColumnMove_MouseLeftUp(object sender, MouseButtonEventArgs ee) //draging column stackpanel mouse left up
        {  
            var mouseX = Mouse.GetPosition(canvase_chart).X;
            var mouseY = Mouse.GetPosition(canvase_chart).Y;
            var mousexX = Mouse.GetPosition(lbl_key).X;
            var mousexY = Mouse.GetPosition(lbl_key).Y;
            var mouseyX = Mouse.GetPosition(lbl_value).X;
            var mouseyY = Mouse.GetPosition(lbl_value).Y;
           // MessageBox.Show(mouseY.ToString());

            if ((mousexX > 0 && mousexX < 84) && (mousexY > 0 && mousexY < 24))
            {                   
                //MessageBox.Show("x");                
                lbl_key.Content = DragingColumn;
                lbl_key.Background = new SolidColorBrush(Colors.LawnGreen);
                Key = DragingColumnTable + "." + DragingColumn;
                createChartTableView(cmb_system.SelectedItem.ToString());

            }
            else if ((mouseyX > 0 && mouseyX < 84) && (mouseyY > 0 && mouseyY < 24))
            {
                lbl_value.Content = DragingColumn;
                lbl_value.Background = new SolidColorBrush(Colors.LawnGreen);
                Value = DragingColumnTable + "." + DragingColumn;
                createChartTableView(cmb_system.SelectedItem.ToString());        
            }
            else if ((mouseX > 0 && mouseX < 406) && (mouseY > 0 && mouseY < 253))
            {
               // createChartTableView(cmb_system.SelectedItem.ToString(),DragingTable,DragingColumn);
               // MessageBox.Show("Z");
            }
            else
            {
                DragingColumn = "";
                DragingColumnTable = "";
            }
            dd.Children.Remove(stackPanel_ColumnMove);
        }

        private void Header_LeftDown(object sender, MouseButtonEventArgs e) //draging column label mouse left down
        {
            string header = ((Label)sender).ToString();

            stackPanel_ColumnMove.Children.Clear();           
            DragingColumn = header.ToString().Substring(31);
            DragingColumnTable = ((Label)(sender)).Name.ToString();

            Label lbl = new Label();
            lbl.Content = DragingColumnTable + "/" + DragingColumn;
            lbl.Width = 100;
            lbl.Height = 30;
            stackPanel_ColumnMove.Background = new SolidColorBrush(Colors.LawnGreen);
            stackPanel_ColumnMove.Width = 100;
            stackPanel_ColumnMove.Height = 30;
            stackPanel_ColumnMove.Opacity = 1;
            stackPanel_ColumnMove.Children.Add(lbl);

            var newX = Mouse.GetPosition(dd).X;
            var newY = Mouse.GetPosition(dd).Y;
            var width = (this.Width) / 2;
            var height = (this.Height) / 2;
            stackPanel_ColumnMove.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX - (width) + 50, newY - (height) + 15);
            try
            {
                dd.Children.Add(stackPanel_ColumnMove);
            }
            catch
            {
                dd.Children.Remove(stackPanel_ColumnMove);
                dd.Children.Add(stackPanel_ColumnMove);
            }
        }     

        private void drage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) // source table draging
        {
            dragingView = ((WrapPanel)(((StackPanel)(((Label)sender).Parent)).Parent));
            dragingCurrentX = Mouse.GetPosition(canvas).X;
            dragingCurrentY = Mouse.GetPosition(canvas).Y;
        }

        void close_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) // source table close
        {
            canvas.Children.Remove(((WrapPanel)(((StackPanel)(((Label)sender).Parent)).Parent)));
        }

        void LableMouseEnter(object sender, MouseEventArgs e) // source table draging and close mouse enter
        {
            Label lbl=(Label)sender;

            if (lbl.Content == "X")
            {
                lbl.Background = new SolidColorBrush(Colors.Red);               
            }
            else
            {
                lbl.Background = new SolidColorBrush(Colors.Brown);
            }
            
        }
        void LableMouseLeave(object sender, MouseEventArgs e) // source table draging and close mouse leave
        {
            Label lbl = (Label)sender;

            if (lbl.Content == "X")
            {
                lbl.Background = new SolidColorBrush(Colors.Blue);
            }
            else
            {
                lbl.Background = new SolidColorBrush(Colors.Yellow);
            }

        }

        void wp_MouseLeaveEnter(object sender, MouseEventArgs e) // source table draging and close mouse enter and leave (Visibility)
        {
            StackPanel stp = new StackPanel();
            WrapPanel wp = new WrapPanel();
            wp = (WrapPanel)sender;

            for (int i = 0; i < wp.Children.Count; i++)
            {
                try
                {
                    stp = (StackPanel)wp.Children[i];

                    if (stp.Visibility == Visibility.Visible)
                    {
                        stp.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        stp.Visibility = Visibility.Visible;
                    }
                    break;
                }
                catch { }
            }
        }

        private void canvas_scroll_MouseMove(object sender, MouseEventArgs e)
        {
            // if mouse is down when its moving, then it's dragging current
            if (e.LeftButton == MouseButtonState.Pressed && dragingView != null)
            {
                string tableName = "";
                try
                {
                    tableName = ((Label)(dragingView.Children[0])).Content.ToString();
                }
                catch { return; }

                var newX = Mouse.GetPosition(canvas).X;
                var newY = Mouse.GetPosition(canvas).Y;
                var currentOffX = dragingView.RenderTransform.Value.OffsetX;
                var currentOffY = dragingView.RenderTransform.Value.OffsetY;
                dragingView.RenderTransform = new MatrixTransform(1, 0, 0, 1, newX - (dragingCurrentX - currentOffX), newY - 40);// new mouseX-(width of the table(previous mouseX-previous offX))

                if (newX>canvas.Width-20)
                {
                    canvas.Width = canvas.Width + 200;
                }
                if (newY > canvas.Height - 20)
                {
                    canvas.Height = canvas.Height + 200;
                }                

                foreach(TableConnector tc in connectors)
                {
                    if(tc.startTable==tableName)
                    {
                        tc.line.X1 = tc.line.X1 + (newX - dragingCurrentX);
                        tc.line.Y1 = tc.line.Y1 + (newY - dragingCurrentY);
                        tc.close.RenderTransform = new TranslateTransform(((newX - dragingCurrentX) / 2.0) + tc.close.RenderTransform.Value.OffsetX, ((newY - dragingCurrentY) / 2.0) + tc.close.RenderTransform.Value.OffsetY);
                    }
                    if(tc.endTable==tableName)
                    {
                        tc.line.X2 = tc.line.X2 + (newX - dragingCurrentX);
                        tc.line.Y2 = tc.line.Y2 + (newY - dragingCurrentY);
                        tc.close.RenderTransform = new TranslateTransform(((newX - dragingCurrentX) / 2.0) + tc.close.RenderTransform.Value.OffsetX, ((newY - dragingCurrentY) / 2.0) + tc.close.RenderTransform.Value.OffsetY);
                    }
                }

                dragingCurrentX = newX;
                dragingCurrentY = newY;

            }
            if (DraginConnector != null)
            {
                var newX = Mouse.GetPosition(canvas).X;
                var newY = Mouse.GetPosition(canvas).Y;
                DraginConnector.line.X2 = newX;
                DraginConnector.line.Y2 = newY;
            }
        }

        //---------------------Chart Table Option Events-------------------------

        private void chkbox_groupbyKey_Click(object sender, RoutedEventArgs e)
        {
            if (chkbox_groupbyKey.IsChecked == true)
            {
                groupBy = true;
            }
            else
            {
                groupBy = false;
            }
        }

        private void chkbox_orderbyKey_Click(object sender, RoutedEventArgs e)
        {
            if (chkbox_orderbyKey.IsChecked == true)
            {
                orderBy = true;
            }
            else
            {
                orderBy = false;
            }
        }

        private void chkbox_valueAggr_Click(object sender, RoutedEventArgs e)
        {
            if (chkbox_valueAggr.IsChecked == true)
            {
                chkbox_groupbyKey.IsChecked = true;
                groupBy = true;
                cmb_aggrigation.IsEnabled = true;
                chkbox_valueAgrCondition.IsEnabled = true;
                aggregation = cmb_aggrigation.SelectedIndex;
            }
            else
            {
                chkbox_valueAgrCondition.IsEnabled = false;
                chkbox_valueAgrCondition.IsChecked = false;
                cmb_aggrigation.IsEnabled = false;
                aggregation = -1;
                AgrCondition = "";
                cmb_AgrOperator.IsEnabled = false;
                txt_AgrCondition.IsEnabled = false;
            }
        }

        private void chkbox_valueCondition_Click(object sender, RoutedEventArgs e)
        {
            if (chkbox_valueCondition.IsChecked == true)
            {
                lbl_condition.IsEnabled = true;
                cmb_operator.IsEnabled = true;
                txt_condition.IsEnabled = true;
                condition = cmb_operator.SelectedItem.ToString().Substring(37) + " " + txt_condition.Text.ToString();
            }
            else
            {
                lbl_condition.IsEnabled = false;
                cmb_operator.IsEnabled = false;
                txt_condition.IsEnabled = false;
                condition = "";
            }
        }                       

        private void chkbox_groupbyKey_Unchecked(object sender, RoutedEventArgs e)
        {
            if(chkbox_valueAggr.IsChecked==true)
            {
                chkbox_groupbyKey.IsChecked = true;
            }
        }

        private void chkbox_valueAgrCondition_Click(object sender, RoutedEventArgs e)
        {
            if (chkbox_valueAgrCondition.IsChecked == true)
            {
                lbl_AgrCondition.IsEnabled = true;
                cmb_AgrOperator.IsEnabled = true;
                txt_AgrCondition.IsEnabled = true;
                AgrCondition = cmb_AgrOperator.SelectedItem.ToString().Substring(37) + " " + txt_AgrCondition.Text.ToString();
            }
            else
            {
                lbl_AgrCondition.IsEnabled = false;
                cmb_AgrOperator.IsEnabled = false;
                txt_AgrCondition.IsEnabled = false;
                AgrCondition = "";
            }
        }

        private void cmb_aggrigation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (chkbox_valueAggr.IsChecked == true)
            {
                aggregation = cmb_aggrigation.SelectedIndex;
            }
        }

        private void cmb_AgrOperator_SelectionChanged(object sender, EventArgs e)
        {
            if (chkbox_valueAgrCondition.IsChecked == true)
            {
                AgrCondition = cmb_AgrOperator.SelectedItem.ToString().Substring(37) + " " + txt_AgrCondition.Text.ToString();
            }
        }

        private void cmb_operator_SelectionChanged(object sender, EventArgs e)
        {
            if (chkbox_valueCondition.IsChecked == true)
            {
                condition = cmb_operator.SelectedItem.ToString().Substring(37) + " " + txt_condition.Text.ToString();
            }
        }

        //---------------------Advanced and Property canvase button Events-------------------------

        private void Canvas_MouseEnter(object sender, MouseEventArgs e)
        {
            ((Canvas)sender).Background = new SolidColorBrush(Colors.Blue);
        }

        private void Canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            ((Canvas)sender).Background = new SolidColorBrush(Color.FromArgb(100, 152, 182, 218));
        }

        private void canvase_chartProperty_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            canvas_advancedDatabind.Visibility = Visibility.Collapsed;
            if (canvas_chartProperties.IsVisible == false)
            {
                canvas_chartProperties.Visibility = Visibility.Visible;
            }
            else
            {
                canvas_chartProperties.Visibility = Visibility.Collapsed;
            }
        }

        private void canvase_advancedDataBind_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            canvas_chartProperties.Visibility = Visibility.Collapsed;
            if (canvas_advancedDatabind.IsVisible == false)
            {
                canvas_advancedDatabind.Visibility = Visibility.Visible;
            }
            else
            {
                canvas_advancedDatabind.Visibility = Visibility.Collapsed;
            }
        }

        //--------------------------------------------------------------------BUTTON CLICKS----------------------------------------------------------------------

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            createQuery();
        }

        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {
            string status = "";
            string status_querySave = "";
            string chart_name = txt_chartName.Text.ToString();
            if (chart_name == "")
            {
                MessageBox.Show("Chart Name cannot be empty");
                return;
            }

            int width = 1;
            int height = 1;
            int refresh_period = 15;
            string x = txt_xaxis.Text.ToString();
            string y = txt_yaxis.Text.ToString();
            int barWidth = 20;
            string barColor = cmb_barColour.SelectedItem.ToString().Substring(38);
            string description = "";

            TextRange textRange = new TextRange(rtxt_description.Document.ContentStart, rtxt_description.Document.ContentEnd);
            description = textRange.Text.ToString();

            try
            {
                barWidth = Int32.Parse(txt_barWidth.Text.ToString());
            }
            catch { }
            try
            {
                width = Int32.Parse(lb_width.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                height = Int32.Parse(lb_height.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                refresh_period = Int32.Parse(txt_refreshPeriod.Text.ToString());
            }
            catch { }

            string back_color = cmb_backColour.SelectedItem.ToString().Substring(38);
            string singel_query_name = "NULL";
           
            if(txt_queryName.Text.ToString()=="")
            {
                singel_query_name = txt_chartName.Text.ToString();               
            }

            createQuery();
            status_querySave = ACM.saveQuery(singel_query_name, cmb_system.SelectedItem.ToString(), sysList[cmb_system.SelectedIndex][2].ToString(),chartQuery);

            if (status_querySave == "exist")
            {
                MessageBox.Show("Query Name alrady exist, tyr another name");
            }
            else if (status_querySave == "fail")
            {
                MessageBox.Show("Query saving failed. Check database connectivity");
            }
            else if (status_querySave == "saved")
            {
                if(componentType=="BarChart")
                {
                    status = ACM.saveComponent(chart_name, "BarChart", width, height, refresh_period, 2, barWidth.ToString(), barColor, back_color, x, y, description, singel_query_name, null);
                }
                else if (componentType == "Gauge")
                {
                    status = ACM.saveComponent(chart_name, "Gauge", width, height, refresh_period, 2, back_color, null, null, null, null, description, singel_query_name, null);
                }
                else
                {
                    status = ACM.saveComponent(chart_name, componentType, width, height, refresh_period, 2, back_color, x, y, null, null, description, singel_query_name, null);
                }    
            }            

            if (status == "exist")
            {
                MessageBox.Show("Chart Name alrady exist, tyr another name");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving failed. Check database connectivity");
            }
            else if (status == "saved")
            {  
                if (componentType == "BarChart")
                {
                    BarChart bcc = new BarChart();
                    bcc.generateChart(chart_name, width, height, barWidth, barColor, back_color, refresh_period, x, y, null, singel_query_name, 2, description);
                    parent.getComponent(componentType, bcc);
                }
                else if (componentType == "Gauge")
                {
                    Gauge g = new Gauge();
                    g.generateChart(chart_name, width, height, back_color, refresh_period, singel_query_name, description);
                    parent.getComponent("Gauge", g);
                }
                else
                {
                    ComponentFram cff = new ComponentFram(componentType);
                    cff.generateChart(chart_name, width, height, back_color, refresh_period, null, singel_query_name, 2, componentType, x, y, description);
                    parent.getComponent(componentType, cff);
                } 

                parent.finish_clicked = true;
                this.Close();
            }
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }      
        
    }

    public class TableConnector
    {
       public Line line = new Line();
       public Label close = new Label();
       public Ellipse node1 = new Ellipse();
       public Ellipse node2 = new Ellipse();
       public string startTable = "";
       public string endTable = "";
       public string startColumn = "";
       public string endColumn = "";
       
       public TableConnector()
       {
           line.Stroke = Brushes.LightSteelBlue;
           line.StrokeThickness = 2;
           close.Content = "X";
           close.Background = new SolidColorBrush(Colors.Red);
           close.MouseLeftButtonDown += close_MouseLeftButtonDown;
       }

       void close_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
       {
           Canvas c=(Canvas)(((Label)sender).Parent);
           c.Children.Remove(((Label)sender));
           c.Children.Remove(line);
       }
    }
}
