﻿using Loginnw.login;
using Management_Dashboard.Pages.Components;
using Management_Dashboard.Windows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for CreateDashBoardView.xaml
    /// </summary>
    /// 
    [Serializable]
    public partial class CreateDashBoardView : Window
    {
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        List<object> components = new List<object>();
        List<string> component_types = new List<string>();
        public bool finish_clicked = false;
        Frame selectedFrame = new Frame();
        public int createChartOption = -1;

        string user_type = "";
        public CreateDashBoardView(string userType)
        {
            InitializeComponent();
            user_type = userType;
        }
        
        private void lnkbtn_home_Click(object sender, RoutedEventArgs e)
        {
            AdminMain am = new AdminMain(user_type);
            am.Show();
            this.Close();
        }

        private void lnkbtn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lnkbtn_signout_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            string status = "";
            List<string[]> component_list = new List<string[]>();           
            string view_name = "";

            view_name = txt_viewName.Text.ToString();
            if(view_name=="")
            {
                MessageBox.Show("View Name cannot be empty");
                return;
            }

            foreach(Frame frm in canvas.Children)
            {
                string[] com_property = new string[5];

                try
                {
                    com_property[0]=((BarChart)frm.Content).chartName;

                }
                catch{ }
                try
                {
                    com_property[0] = ((ComponentFram)frm.Content).chartName;

                }
                catch { }
                try
                {
                    com_property[0] = ((Gauge)frm.Content).chartName;

                }
                catch { }

                com_property[1] = (frm.RenderTransform.Value.OffsetX).ToString();
                com_property[2] = (frm.RenderTransform.Value.OffsetY).ToString();
                com_property[3] = (frm.LayoutTransform.Value.M11).ToString();
                com_property[4] = (selectedFrame.LayoutTransform.Value.M22).ToString();

                component_list.Add(com_property);
            }

            status = ACM.saveDashBoardView(view_name,canvas.Width,canvas.Height,component_list);

            if (status == "exist")
            {
                MessageBox.Show("DashBoard View Name alrady exist, tyr another name");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving failed. Check database connectivity");
            }
            else if (status == "saved")
            {
                MessageBox.Show("DashBoard View saved successfully");
                canvas.Children.Clear();
            }
        }

        public void AddChart(string type)
        {
            BarChartMain bcm=null;
            ChartMain cm=null;
            GaugeMain gm = null;
            
            try
            {
                finish_clicked = false;

                if(createChartOption==2)
                {
                    if (type == "BarChart")
                    {
                        bcm = new BarChartMain(this);
                        bcm.ShowDialog();
                    }
                    else if (type == "Gauge")
                    {
                        gm = new GaugeMain(this);
                        gm.ShowDialog();
                    }
                    else
                    {
                        cm = new ChartMain(this, type);
                        cm.ShowDialog();
                    }  
                }
                else if(createChartOption==1)
                {
                    CreateComponent cp = new CreateComponent(type, this);
                    cp.ShowDialog();
                }    

                if (finish_clicked == true)
                {
                    Frame frm = new Frame();                    
                    double TargetWidth = 0;
                    double TargetHeight = 0;                   
                    string backColor = "";

                    if (type == "BarChart")
                    {
                        BarChart bar_chart = (BarChart)components[components.Count - 1];
                        TargetWidth = (bar_chart.width) * 298;
                        TargetHeight = (bar_chart.height) * 205;                        
                        backColor = bar_chart.back_Color;
                        frm.Width = bar_chart.Width;
                        frm.Height = 450;
                        frm.Content = bar_chart;
                    }
                    else if (type == "Gauge")
                    {
                        Gauge gauge = (Gauge)components[components.Count - 1];
                        TargetWidth = (gauge.width) * 298;
                        TargetHeight = (gauge.height) * 205;
                        backColor = gauge.back_Color;
                        frm.Width = gauge.Width;
                        frm.Height = 350;
                        frm.Content = gauge;
                    }
                    else
                    {
                        ComponentFram component_frame = (ComponentFram)components[components.Count - 1];
                        TargetWidth = (component_frame.width) * 298;
                        TargetHeight = (component_frame.height) * 205;                    
                        backColor = component_frame.back_Color;
                        frm.Width = component_frame.Width;
                        frm.Height = 400;
                        frm.Content = component_frame;
                    }    
                    
                    ContextMenu mnu = new ContextMenu();

                    MenuItem edit = new MenuItem();
                    edit.Header = "Edit";
                    edit.Click += edit_Click;

                    MenuItem delete = new MenuItem();
                    delete.Header = "Remove";
                    delete.Click += delete_Click;

                    MenuItem reset = new MenuItem();
                    reset.Header = "Reset Size";
                    reset.Click += reset_Click;

                    MenuItem zoomIn = new MenuItem();
                    zoomIn.Header = "Zoom In";
                    zoomIn.Click += zoomIn_Click;

                    MenuItem zoomOut = new MenuItem();
                    zoomOut.Header = "Zoom Out";
                    zoomOut.Click += zoomOut_Click;

                    mnu.Items.Add(edit);
                    mnu.Items.Add(delete);
                    mnu.Items.Add(reset);
                    mnu.Items.Add(zoomIn);
                    mnu.Items.Add(zoomOut);                 

                    if (backColor == "Transparent")
                    {
                        frm.Background = new SolidColorBrush(Colors.Transparent);
                    }
                    else if (backColor == "DarkGray")
                    {
                        frm.Background = new SolidColorBrush(Colors.DarkGray);
                    }
                    else if (backColor == "White")
                    {
                        frm.Background = new SolidColorBrush(Colors.White);
                    }
                    else
                    {
                        frm.Background = new SolidColorBrush(Colors.DarkGray);
                    }
                    
                    frm.LayoutTransform = new ScaleTransform() { ScaleX = (TargetWidth / frm.Width), ScaleY = (TargetHeight / frm.Height) };
                    frm.MouseLeftButtonDown += MouseLeftDown;
                    frm.MouseLeftButtonUp += MouseLeftUp;
                    frm.ContextMenu = mnu;
                    frm.RenderTransform = new TranslateTransform();
                    frm.MouseRightButtonDown += frm_MouseRightButtonDown;
                    canvas.Children.Add(frm);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                bcm.Close();
                cm.Close();
            }
        }

        private void popupCall(string componentType)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();

            if (createChartOption == 2)
            {
                AddChart(componentType);
            }
            else if (createChartOption == 1)
            {
                CreateComponent cp = new CreateComponent(componentType,this);
                cp.ShowDialog();
            }
        }

        private void btn_barChartAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("BarChart");
        }

        private void btn_pieChartAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("PieChart");
        }

        private void btn_lineChartAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("LineChart");
        }

        private void btn_areaChartAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("AreaChart");            
        }

        private void btn_scatterChartAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("ScatterChart");
        }

        private void btn_gaugeAdd_Click(object sender, RoutedEventArgs e)
        {
            UserPopup upp = new UserPopup(this);
            upp.ShowDialog();
            AddChart("Gauge");
        }

        void frm_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedFrame = ((Frame)((UIElement)sender));
        }

        void edit_Click(object sender, RoutedEventArgs e)
        {

        }
        void delete_Click(object sender, RoutedEventArgs e)
        {
            canvas.Children.Remove(selectedFrame);
        }
        void reset_Click(object sender, RoutedEventArgs e)
        {
            selectedFrame.LayoutTransform = new ScaleTransform() { ScaleX = 1, ScaleY = 1 };           
        }
        void zoomIn_Click(object sender, RoutedEventArgs e)
        {
            selectedFrame.LayoutTransform = new ScaleTransform() { ScaleX = (selectedFrame.LayoutTransform.Value.M11)+0.1, ScaleY =(selectedFrame.LayoutTransform.Value.M22)+0.1 };  
            
        }
        void zoomOut_Click(object sender, RoutedEventArgs e)
        {
            selectedFrame.LayoutTransform = new ScaleTransform() { ScaleX = (selectedFrame.LayoutTransform.Value.M11) - 0.1, ScaleY = (selectedFrame.LayoutTransform.Value.M22) - 0.1 };               
        }

        public void getComponent(string type, object cmpnt)
        {
            components.Add(cmpnt);
            component_types.Add(type);
        }



        //--------------------------------------------------------

        private Elements current = new Elements();

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.current.X = Mouse.GetPosition((IInputElement)sender).X;
            this.current.Y = Mouse.GetPosition((IInputElement)sender).Y;

            // Ensure object receives all mouse events.
            //this.current.InputElement.CaptureMouse();
        }

        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.current.InputElement != null)
            {
                this.current.InputElement.ReleaseMouseCapture();
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            // if mouse is down when its moving, then it's dragging current
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.current.IsDragging = true;
            }
            else
            {
                var max_height = 0.0;
                var max_width = 0.0;
                foreach (Frame frm in canvas.Children)
                {
                    var total_width = ((frm.Width) * (frm.LayoutTransform.Value.M11)) + frm.RenderTransform.Value.OffsetX;
                    var total_height = ((450) * (frm.LayoutTransform.Value.M22)) + frm.RenderTransform.Value.OffsetY;
                    if (total_width > max_width)
                    {
                        max_width = total_width;
                    }
                    if (total_height > max_height)
                    {
                        max_height = total_height;
                    }
                }
                if (max_width < canvas.Width)
                {
                    canvas.Width = max_width;
                }
                if (max_height < canvas.Height)
                {
                    canvas.Height = max_height;
                }
            }

            if (this.current.IsDragging && current.InputElement != null)
            {
                // Retrieve the current position of the mouse.
                var newX = Mouse.GetPosition((IInputElement)sender).X;
                var newY = Mouse.GetPosition((IInputElement)sender).Y;

                // Reset the location of the object (add to sender's renderTransform
                // newPosition minus currentElement's position

                var rt = ((UIElement)this.current.InputElement).RenderTransform;
                var offsetX = rt.Value.OffsetX;
                var offsetY = rt.Value.OffsetY;

                rt.SetValue(TranslateTransform.XProperty, offsetX + newX - current.X);
                rt.SetValue(TranslateTransform.YProperty, offsetY + newY - current.Y);

                // Update position of the mouse
                current.X = newX;
                current.Y = newY;

                Frame frm = (Frame)((UIElement)this.current.InputElement);              
                var wth = (frm.Width) * (frm.LayoutTransform.Value.M11);
                var hgt = (450) * (frm.LayoutTransform.Value.M22);                
                var rt_new = ((UIElement)this.current.InputElement).RenderTransform;
                var offsetX_new = rt.Value.OffsetX;
                var offsetY_new = rt.Value.OffsetY;

                if ((wth + offsetX_new)>canvas.Width)
                {
                    canvas.Width = wth + offsetX_new;
                }
                if ((hgt + offsetY_new) > canvas.Height)
                {
                    canvas.Height = hgt + offsetY_new;
                }
            }
            
        }

        private void MouseLeftDown(object sender, MouseButtonEventArgs e)
        {            
            this.current.InputElement = (IInputElement)sender;
        }
        private void MouseLeftUp(object sender, MouseButtonEventArgs e)
        {
            this.current.InputElement = null; ;
        }       

        private void btn_createNew_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to close current view.", "Confirm", MessageBoxButton.YesNo)==MessageBoxResult.Yes)
            {
                canvas.Children.Clear();
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
        }
        private void btn_openNew_Click(object sender, RoutedEventArgs e)
        {
        }             
    }

    public class Elements
    {
        bool isDragging = false;
        IInputElement inputElement = null;
        double x, y = 0;

        public Elements() { }
     
        public IInputElement InputElement
        {

            get { return this.inputElement; }
            set
            {
                this.inputElement = value;

                /* every time inputElement resets, the draggin stops  */
                this.isDragging = false;
            }
        }

        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public bool IsDragging
        {
            get { return this.isDragging; }
            set { this.isDragging = value; }
        }
    }  

}
