﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Expression.Shapes;
using Management_Dashboard.Pages.Components;
using Loginnw.login;

namespace Management_Dashboard
{
    /// <summary>
    /// Interaction logic for DashBoardWindow.xaml
    /// </summary>
    public partial class DashBoardWindow : Window
    {
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();

        List<string[]> views = new List<string[]>();
        List<string[]> view_component = new List<string[]>();
        List<string> component_property = new List<string>();
        List<string[]> component_query = new List<string[]>();

        Frame selectedFrame_onRightClick = new Frame();
        List<int> loaded_view_index = new List<int>();
        List<Canvas> loaded_views = new List<Canvas>();
        int displayed_view_index = -1;
        string user_type = "";
        private string user_name = "";

        public DashBoardWindow(string userType, string userName)
        {
            InitializeComponent();
            user_name = userName;
            user_type = userType;
            getViews(user_name);

            if (user_type == "User")
            {
                lnkbtn_home.Visibility =Visibility.Hidden;
            }
        }

        public void getViews(string username)
        {
            views = ACM.getDashBoardView(username);
            for (int i = 0; i < views.Count;i++ )
            {
                cmb_openView.Items.Add(views[i][0].ToString());
            }
        }

        private void cmb_openView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbl_viewName.Content = cmb_openView.SelectedItem.ToString();
            slider_scale.Value = 1;

            if (cmb_openView.SelectedIndex > 0)
            {
                line_back.IsEnabled = true;
            }
            else
            {
                line_back.IsEnabled = false;
            }

            if (cmb_openView.SelectedIndex < views.Count - 1)
            {
                line_foward.IsEnabled = true;
            }
            else
            {
                line_foward.IsEnabled = false;
            }

            if (displayed_view_index != -1)
            {
                while (canvas.Children.Count>0)
                {
                    Frame frm = (Frame)canvas.Children[0];
                    try
                    {
                        ((BarChart)frm.Content).StopUpdate();
                    }
                    catch
                    {
                    }
                    try
                    {
                        ((ComponentFram)frm.Content).StopUpdate();
                    }
                    catch
                    {
                    }
                    try
                    {
                        ((Gauge)frm.Content).StopUpdate();
                    }
                    catch
                    {
                    }
                    
                    canvas.Children.RemoveAt(0);                   
                    loaded_views[displayed_view_index].Children.Add(frm);
                }
            }

            if (!loaded_view_index.Contains(cmb_openView.SelectedIndex))
            {
                Canvas new_view = new Canvas();

                new_view = loadView(cmb_openView.SelectedItem.ToString(), Double.Parse(views[cmb_openView.SelectedIndex][1].ToString()), Double.Parse(views[cmb_openView.SelectedIndex][2].ToString()));

                loaded_view_index.Add(cmb_openView.SelectedIndex);
                loaded_views.Add(new_view);
                drawView(loaded_view_index.Count-1);
                displayed_view_index = loaded_view_index.Count - 1;
               
            }
            else
            {
                drawView(loaded_view_index.IndexOf(cmb_openView.SelectedIndex));
                displayed_view_index = loaded_view_index.IndexOf(cmb_openView.SelectedIndex);
            }
            
        }

        public void drawView(int viewIndex)
        {            
            while (loaded_views[viewIndex].Children.Count > 0)
            {
                Frame frm = (Frame)loaded_views[viewIndex].Children[0];              
                loaded_views[viewIndex].Children.RemoveAt(0);
                try
                {
                    ((BarChart)frm.Content).StartUpdate();                   
                }
                catch { }
                try
                {
                    ((ComponentFram)frm.Content).StartUpdate();                    
                }
                catch { }
                try
                    {
                        ((Gauge)frm.Content).StartUpdate(); 
                    }
                    catch
                    {
                    }
               
                canvas.Children.Add(frm);                
            }
            canvas.Width = loaded_views[viewIndex].Width;
            canvas.Height = loaded_views[viewIndex].Height;                   
        }

        public Canvas loadView(string view, double width, double height)
        {
            Canvas viewCanvas = new Canvas();
            viewCanvas.Width = width;
            viewCanvas.Height = height;

            view_component = ACM.getViewComponents(view);

            for (int i = 0; i < view_component.Count;i++ )
            {
                string single_query_name="NULL";
                List<string[]> multiple_queries=new List<string[]>();

                component_property = ACM.getComponent(view_component[i][0].ToString());
                component_query = ACM.getQueryComponent(view_component[i][0].ToString());
                
                if (Int32.Parse(component_property[4].ToString()) == 1)
                {

                    for (int j = 0; j < component_query.Count; j++)
                    {
                        string[] bar = new string[2];
                        bar[0] = component_query[j][1].ToString();
                        bar[1] = component_query[j][0].ToString();
                        multiple_queries.Add(bar);
                    }
                }
                else if (Int32.Parse(component_property[4].ToString()) == 2)
                {
                    single_query_name = component_query[0][0].ToString();
                }


                TranslateTransform translateTransform = new TranslateTransform(Double.Parse(view_component[i][1].ToString()), Double.Parse(view_component[i][2].ToString()));
                ScaleTransform scaleTransform = new ScaleTransform(Double.Parse(view_component[i][3].ToString()), Double.Parse(view_component[i][4].ToString()));
               
                if (component_property[0].ToString()=="BarChart")
                {    
                    BarChart bar_chart = new BarChart();
                    bar_chart.generateChart(view_component[i][0].ToString(), Int32.Parse(component_property[1].ToString()), Int32.Parse(component_property[2].ToString()), Int32.Parse(component_property[5].ToString()), component_property[6].ToString(), component_property[7].ToString(), Int32.Parse(component_property[3].ToString()), component_property[8].ToString(), component_property[9].ToString(), multiple_queries, single_query_name, Int32.Parse(component_property[4].ToString()), component_property[10].ToString());
                  
                    viewCanvas.Children.Add(addToView(bar_chart, translateTransform, scaleTransform, "BarChart")); 
                }
                else if (component_property[0].ToString() == "Gauge")
                {
                    Gauge gauge = new Gauge();
                    gauge.generateChart(view_component[i][0].ToString(), Int32.Parse(component_property[1].ToString()), Int32.Parse(component_property[2].ToString()), component_property[5].ToString(), Int32.Parse(component_property[3].ToString()), single_query_name, component_property[10].ToString());

                    viewCanvas.Children.Add(addToView(gauge, translateTransform, scaleTransform, "Gauge"));
                }
                else
                {
                    ComponentFram chart = new ComponentFram(component_property[0].ToString());
                    chart.generateChart(view_component[i][0].ToString(), Int32.Parse(component_property[1].ToString()), Int32.Parse(component_property[2].ToString()), component_property[5].ToString(), Int32.Parse(component_property[3].ToString()), multiple_queries, single_query_name, Int32.Parse(component_property[4].ToString()), component_property[0].ToString(), component_property[6].ToString(), component_property[7].ToString(), component_property[10].ToString());

                    viewCanvas.Children.Add(addToView(chart, translateTransform, scaleTransform, component_property[0].ToString())); 
                }
            }

            return viewCanvas;
        }

        public Frame addToView(object obj, TranslateTransform tt, ScaleTransform st, string chartType)
        {
            Frame frm = new Frame();
            string colour = "";

            ContextMenu mnu = new ContextMenu();
            MenuItem view_one = new MenuItem();
            view_one.Header = "View";
            view_one.Click += view_one_Click;
            mnu.Items.Add(view_one);

            StackPanel stkpnl = new StackPanel();
            stkpnl.Width = 300;            
            ImageBrush img = new ImageBrush(new BitmapImage(new Uri("D:\\!#SLIIT#\\3rd year 2nd sem\\#SEP\\Management Dashboard - Copy (2014.05.16)\\Management Dashboard\\Resources\\tooltip_background.jpg")));
            stkpnl.Background = new SolidColorBrush(Color.FromRgb(162,215,240));
            
            frm.LayoutTransform = st;
            frm.RenderTransform = tt;
            frm.ContextMenu = mnu;
            frm.MouseRightButtonDown += frm_MouseRightButtonDown;

            TextBlock tb_title = new TextBlock();
            tb_title.Background = new SolidColorBrush(Colors.YellowGreen);
            tb_title.FontSize = 14;
            tb_title.FontWeight = FontWeights.Bold;
            tb_title.Height = 30;
            tb_title.Width = stkpnl.Width;
            tb_title.TextAlignment = TextAlignment.Center;
            TextBlock tb_space = new TextBlock();
            tb_space.Height = 10;
            TextBlock tb_name = new TextBlock();
            tb_name.Height = 30;
            tb_name.FontWeight = FontWeights.Bold;
            TextBlock tb_type = new TextBlock();
            tb_type.Height = 30;
            tb_type.FontWeight = FontWeights.Bold;
            TextBlock tb_x = new TextBlock();
            tb_x.Height = 30;
            tb_x.FontWeight = FontWeights.Bold;
            TextBlock tb_y = new TextBlock();
            tb_y.Height = 30;
            tb_y.FontWeight = FontWeights.Bold;
            TextBlock tb_description_title = new TextBlock();
            tb_description_title.Height = 20;
            tb_description_title.FontWeight = FontWeights.Bold;
            tb_description_title.Text = "Chart Description :";
            RichTextBox tb_description = new RichTextBox();
            tb_description.Width  = stkpnl.Width;
            tb_description.Background = new SolidColorBrush(Colors.Transparent);
            tb_description.BorderBrush = new SolidColorBrush(Colors.Transparent);

            if(chartType=="BarChart")
            {
                BarChart barChart = (BarChart)obj;
                colour = barChart.back_Color.ToString();
                frm.Content = barChart;
                
                tb_title.Text = "Barchart : " + ((BarChart)obj).chartName.ToString();
                tb_name.Text = "Chart Name : " + ((BarChart)obj).chartName.ToString();                
                tb_type.Text = "Chart Type : Bar Chart";
                tb_x.Text = "X Axis : " + ((BarChart)obj).xAxis.ToString();
                tb_y.Text = "Y Axis : " + ((BarChart)obj).yAxis.ToString();
                tb_description.AppendText(((BarChart)obj).chart_description.ToString()); 
                
            }
            else if (chartType == "Gauge")
            {
                Gauge gauge = (Gauge)obj;
                colour = gauge.back_Color.ToString();
                frm.Content = gauge;

                tb_title.Text = "Gauge : " + ((Gauge)obj).chartName.ToString();
                tb_name.Text = "Chart Name : " + ((Gauge)obj).chartName.ToString();
                tb_type.Text = "Chart Type : Gauge";
                tb_description.AppendText(((Gauge)obj).chart_description.ToString());
            }
            else
            {
                ComponentFram component = (ComponentFram)obj;
                colour = component.back_Color.ToString();
                frm.Content = component;
                frm.Height = 276;

                tb_title.Text = ((ComponentFram)obj).componentType.ToString()+" : " +((ComponentFram)obj).chartName.ToString();
                tb_name.Text = "Chart Name : "+((ComponentFram)obj).chartName.ToString();
                tb_type.Text = "Chart Type : " + ((ComponentFram)obj).componentType.ToString();
                tb_x.Text = "X Axis : " + ((ComponentFram)obj).Xaxis.ToString();
                tb_y.Text = "Y Axis : " + ((ComponentFram)obj).Yaxis.ToString();
                tb_description.AppendText(((ComponentFram)obj).chart_description.ToString());
            } 
            
            if (colour == "Transparent")
            {
                frm.Background = new SolidColorBrush(Colors.Transparent);
            }
            else if (colour == "DarkGray")
            {
                frm.Background = new SolidColorBrush(Colors.DarkGray);
            }
            else if (colour == "White")
            {
                frm.Background = new SolidColorBrush(Colors.White);
            }
           // frm.Background = new SolidColorBrush(Colors.DarkGray);   

            stkpnl.Children.Add(tb_title);
            stkpnl.Children.Add(tb_space);
            stkpnl.Children.Add(tb_name);
            stkpnl.Children.Add(tb_type);
            stkpnl.Children.Add(tb_x);
            stkpnl.Children.Add(tb_y);
            stkpnl.Children.Add(tb_description_title);  
            stkpnl.Children.Add(tb_description);

            ToolTip tltp = new System.Windows.Controls.ToolTip();
            ToolTipService.SetShowDuration(frm, 5000);
            tltp.Content = stkpnl;
            frm.ToolTip = tltp;
            return frm;
        }

        void frm_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedFrame_onRightClick = ((Frame)((UIElement)sender));
        }

        void view_one_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void ForwardArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
                cmb_openView.SelectedIndex++;            
        }

        private void BackArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
                cmb_openView.SelectedIndex--;           
        }

        private void Arrow_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((BlockArrow)sender).Fill = new SolidColorBrush(Colors.Blue);
        }

        private void Arrow_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (((BlockArrow)sender).IsEnabled == false)
            {
                ((BlockArrow)sender).Fill = new SolidColorBrush(Color.FromRgb(184, 187, 189));
            }
            else
            {
                ((BlockArrow)sender).Fill = new SolidColorBrush(Color.FromRgb(133, 133, 207));
            }
        }

        private void rec_close_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((Rectangle)sender).Fill = new SolidColorBrush(Colors.White);
        }

        private void rec_minimiz_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((Rectangle)sender).Fill = new SolidColorBrush(Colors.White);
        }

        private void rect_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((Rectangle)sender).Fill = new SolidColorBrush(Colors.Transparent);
        }

        void rec_close_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        void rec_minimize_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void line_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (((BlockArrow)sender).IsEnabled == false)
            {
                ((BlockArrow)sender).Fill = new SolidColorBrush(Color.FromRgb(184, 187, 189));
            }
            else
            {
                ((BlockArrow)sender).Fill = new SolidColorBrush(Color.FromRgb(133, 133, 207));
            }
            
        }


        private void lnkbtn_home_Click(object sender, RoutedEventArgs e)
        {
            AdminMain am = new AdminMain(user_type);
            am.Show();
            this.Close();
        }

        private void lnkbtn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lnkbtn_signout_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
