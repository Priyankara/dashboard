﻿using Management_Dashboard.Pages.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for GaugeMain.xaml
    /// </summary>
    public partial class GaugeMain : Window
    {
        //CreateDashBoardView cdbv = new CreateDashBoardView();
        ApplicationConnectionManager ACM = new ApplicationConnectionManager();
        List<string[]> queries = new List<string[]>();
        private CreateDashBoardView parent;

        public GaugeMain(CreateDashBoardView p)
        {
            InitializeComponent();

            parent = p;

            try
            {
                queries = ACM.getQueries();
            }
            catch { }

            for (int i = 0; i < queries.Count; i++)
            {
                cmb_singleQuery.Items.Add(queries[i][0].ToString());
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if (i == 1)
            {
                tab_barchartProperties.SelectedIndex = 0;
                btn_back.IsEnabled = false;
                btn_next.IsEnabled = true;
                btn_finish.IsEnabled = false;
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            int i = tab_barchartProperties.SelectedIndex;
            if (i == 0)
            {
                tab_barchartProperties.SelectedIndex = 1;
                btn_next.IsEnabled = false;
                btn_finish.IsEnabled = true;
                btn_back.IsEnabled = true;               
            }
        }

        private void btn_finish_Click(object sender, RoutedEventArgs e)
        {
            string status = "";
            int type = 2;
            string chart_name = txt_chartName.Text.ToString();
            if (chart_name == "")
            {
                MessageBox.Show("Chart Name cannot be empty");
                return;
            }

            int width = 1;
            int height = 1;
            int refresh_period = 15;
            string description = "";

            TextRange textRange = new TextRange(rtxt_description.Document.ContentStart, rtxt_description.Document.ContentEnd);
            description = textRange.Text.ToString();

            try
            {
                width = Int32.Parse(lb_width.SelectedItem.ToString().Substring(38));
            }
            catch { }
            try
            {
                height = Int32.Parse(lb_height.SelectedItem.ToString().Substring(38));
            }
            catch { }           
            try
            {
                refresh_period = Int32.Parse(txt_refreshPeriod.Text.ToString());
            }
            catch { }

            string back_color = cmb_backColour.SelectedItem.ToString().Substring(38);
            string singel_query_name = "NULL";
           
                try
                {
                    singel_query_name = cmb_singleQuery.SelectedItem.ToString();
                }
                catch
                {
                }
           

            status = ACM.saveComponent(chart_name, "Gauge", width, height, refresh_period, type, back_color, null, null, null, null, description, singel_query_name,null);

            if (status == "exist")
            {
                MessageBox.Show("Chart Name alrady exist, tyr another name");
            }
            else if (status == "fail")
            {
                MessageBox.Show("Saving failed. Check database connectivity");
            }
            else if (status == "saved")
            {
                Gauge g = new Gauge();
                g.generateChart(chart_name, width, height, back_color, refresh_period, singel_query_name, description);
                parent.getComponent("Gauge", g);
                parent.finish_clicked = true;
                this.Close();
            }
        }

        private void btn_close_Click(object sender, RoutedEventArgs e)
        {
            parent.finish_clicked = false;
            this.Close();
        }           
    }
}
