﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Loginnw.login;
using Management_Dashboard;

namespace WpfToolkitChart.Imeshe.Windows
{
    /// <summary>
    /// Interaction logic for QueryBuilder.xaml
    /// </summary>
    public partial class QueryBuilder : Window
    {
        public QueryBuilder()
        {
            InitializeComponent();
            CreateDataBind CDB = new CreateDataBind();
            mainFram.Content = CDB;
        }

        private void lnkbtn_exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lnkbtn_home_Click(object sender, RoutedEventArgs e)
        {
            AdminMain am=new AdminMain("Admin");
            am.Show();
            this.Close();
        }

        private void lnkbtn_signout_Click(object sender, RoutedEventArgs e)
        {
            //LoginHome lo=new LoginHome();
            //lo.Show();
            //this.Close();
        }
    }
}
