﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Login;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for UpdateUser.xaml
    /// </summary>
    public partial class UpdateUser : Window
    {
        DBAccess dba=new DBAccess();
        List<string> users = new List<string>();

        public UpdateUser()
        {
            InitializeComponent();
            users = dba.getUsers();
            for(int i=0;i<users.Count;i++)
            {
                cmb_usernames.Items.Add(users[i].ToString());
            }
        }

        public void loadViews()
        {
            List<string> views = new List<string>();
            views = dba.loadViews();

            for (int b = 0; b < views.Count; b++)
            {
                TreeViewItem treeItem = new TreeViewItem();// for tabel name 
                String View = views[b].ToString();
                CheckBox cbt = new CheckBox();
                cbt.Content = View;
                treeItem.Header = cbt;
                tree_views.Items.Add(treeItem);

                if (dba.getUserViews(cmb_usernames.SelectedItem.ToString(),View))
                {
                    cbt.IsChecked = true;
                }
            }
        }

        private void cmb_usernames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tree_views.Items.Clear();
            loadViews();
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            List<string> selectedViews=new List<string>();
            bool status = false;

            foreach (TreeViewItem tv in tree_views.Items)
            {
                CheckBox cb = (CheckBox)(tv.Header);

                if (cb.IsChecked == true)
                {
                    selectedViews.Add(cb.Content.ToString());
                }
            }
            
           status= dba.UpdateUser(cmb_usernames.SelectedItem.ToString(),selectedViews);

            if (status)
            {
                MessageBox.Show("User Updated");
            }
            else
            {
                MessageBox.Show("Update Fail");
            }
        }
    }
}
