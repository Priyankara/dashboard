﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Management_Dashboard.Windows
{
    /// <summary>
    /// Interaction logic for UserPopup.xaml
    /// </summary>
    public partial class UserPopup : Window
    {
        private CreateDashBoardView parent;

        public UserPopup(CreateDashBoardView p)
        {
            InitializeComponent();
            parent = p;
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            parent.createChartOption = -1;
            this.Close();
        }

        private void btn_select_Click(object sender, RoutedEventArgs e)
        {
            if (rbtn_create.IsChecked == true)
            {
                parent.createChartOption = 1;
                this.Close();
            }
            else
            {
                parent.createChartOption = 2;
                this.Close();
            }
        }
    }
}
