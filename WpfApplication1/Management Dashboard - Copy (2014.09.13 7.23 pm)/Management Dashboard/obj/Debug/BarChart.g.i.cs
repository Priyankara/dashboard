﻿#pragma checksum "..\..\BarChart.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D4EC7343DC84D740B7F914B69CFF34E3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Expression.Controls;
using Microsoft.Expression.Media;
using Microsoft.Expression.Shapes;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace Management_Dashboard {
    
    
    /// <summary>
    /// BarChart
    /// </summary>
    public partial class BarChart : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Controls.LineArrow line_x;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Expression.Controls.LineArrow line_y;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_y;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_x;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grd_barChart;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_chartName;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid_scale;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_1;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_2;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_3;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_4;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_5;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_6;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_7;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_8;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_9;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\BarChart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_10;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Management Dashboard;component/barchart.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\BarChart.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.line_x = ((Microsoft.Expression.Controls.LineArrow)(target));
            return;
            case 2:
            this.line_y = ((Microsoft.Expression.Controls.LineArrow)(target));
            return;
            case 3:
            this.lbl_y = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lbl_x = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.grd_barChart = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.lbl_chartName = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.grid_scale = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.lbl_1 = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lbl_2 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lbl_3 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lbl_4 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lbl_5 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.lbl_6 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lbl_7 = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.lbl_8 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.lbl_9 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.lbl_10 = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

