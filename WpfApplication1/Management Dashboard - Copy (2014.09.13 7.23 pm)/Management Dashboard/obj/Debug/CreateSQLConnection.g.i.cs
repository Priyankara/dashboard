﻿#pragma checksum "..\..\CreateSQLConnection.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9FE2EB49CA24E794F94674E86BACA378"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace Management_Dashboard {
    
    
    /// <summary>
    /// CreateSQLConnections
    /// </summary>
    public partial class CreateSQLConnections : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_makeConn;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid g;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_testConn;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbl_status;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grd_tables;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView tree_main;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ckbx_all;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ckbx_non;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_servers;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_sysName;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_instances;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_testConn_1;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_authentication;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_catelog;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grd_sqlAuthenticate;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_username;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txt_password;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_testConn_2;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_sysName_2;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_authentication_2;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox msktxt_port;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_ip;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_ip;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grd_sqlAuthenticate_2;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_username_2;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txt_password_2;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_catelog_2;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_refresh;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\CreateSQLConnection.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel abc;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Management Dashboard;component/createsqlconnection.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CreateSQLConnection.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btn_makeConn = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\CreateSQLConnection.xaml"
            this.btn_makeConn.Click += new System.Windows.RoutedEventHandler(this.register_connection_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.g = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btn_testConn = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\CreateSQLConnection.xaml"
            this.btn_testConn.Click += new System.Windows.RoutedEventHandler(this.btn_testConn_2_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lbl_status = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.grd_tables = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.tree_main = ((System.Windows.Controls.TreeView)(target));
            return;
            case 7:
            this.ckbx_all = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 8:
            this.ckbx_non = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.cmb_servers = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.txt_sysName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.cmb_instances = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.btn_testConn_1 = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\CreateSQLConnection.xaml"
            this.btn_testConn_1.Click += new System.Windows.RoutedEventHandler(this.btn_testConn_1_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.cmb_authentication = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.cmb_catelog = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.grd_sqlAuthenticate = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.txt_username = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.txt_password = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 18:
            this.btn_testConn_2 = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\CreateSQLConnection.xaml"
            this.btn_testConn_2.Click += new System.Windows.RoutedEventHandler(this.btn_testConn_2_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.txt_sysName_2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.cmb_authentication_2 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 21:
            this.msktxt_port = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 22:
            this.cmb_ip = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 23:
            this.txt_ip = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.grd_sqlAuthenticate_2 = ((System.Windows.Controls.Grid)(target));
            return;
            case 25:
            this.txt_username_2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.txt_password_2 = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 27:
            this.cmb_catelog_2 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 28:
            this.btn_refresh = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\CreateSQLConnection.xaml"
            this.btn_refresh.Click += new System.Windows.RoutedEventHandler(this.btn_refresh_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.abc = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

