﻿#pragma checksum "..\..\..\Windows\AdminMain.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F5CEFF4BF53178CB786F29BAE816098B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Loginnw.login {
    
    
    /// <summary>
    /// AdminMain
    /// </summary>
    public partial class AdminMain : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_conManager;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_repotr;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_createDashBoard;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_queryBuild;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_viewDashBoard;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_updateUser;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_createUser;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_blocking;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_userLogs;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_locatioManager;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Windows\AdminMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_configuration;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Management Dashboard;component/windows/adminmain.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\AdminMain.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btn_conManager = ((System.Windows.Controls.Button)(target));
            
            #line 8 "..\..\..\Windows\AdminMain.xaml"
            this.btn_conManager.Click += new System.Windows.RoutedEventHandler(this.Button_Click_3);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btn_repotr = ((System.Windows.Controls.Button)(target));
            
            #line 9 "..\..\..\Windows\AdminMain.xaml"
            this.btn_repotr.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btn_createDashBoard = ((System.Windows.Controls.Button)(target));
            
            #line 10 "..\..\..\Windows\AdminMain.xaml"
            this.btn_createDashBoard.Click += new System.Windows.RoutedEventHandler(this.Button_Click_5);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btn_queryBuild = ((System.Windows.Controls.Button)(target));
            
            #line 11 "..\..\..\Windows\AdminMain.xaml"
            this.btn_queryBuild.Click += new System.Windows.RoutedEventHandler(this.Button_Click_4);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btn_viewDashBoard = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\Windows\AdminMain.xaml"
            this.btn_viewDashBoard.Click += new System.Windows.RoutedEventHandler(this.Button_Click_6);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btn_updateUser = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\Windows\AdminMain.xaml"
            this.btn_updateUser.Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btn_createUser = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\Windows\AdminMain.xaml"
            this.btn_createUser.Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btn_blocking = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.btn_userLogs = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.btn_locatioManager = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\Windows\AdminMain.xaml"
            this.btn_locatioManager.Click += new System.Windows.RoutedEventHandler(this.Button_Click_7);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btn_configuration = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\Windows\AdminMain.xaml"
            this.btn_configuration.Click += new System.Windows.RoutedEventHandler(this.btn_configuration_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

