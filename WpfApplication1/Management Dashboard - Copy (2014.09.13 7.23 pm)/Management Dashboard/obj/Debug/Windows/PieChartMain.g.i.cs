﻿#pragma checksum "..\..\..\Windows\PieChartMain.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F9A97749A2150B96638981F8A712DA71"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Management_Dashboard.Windows {
    
    
    /// <summary>
    /// PieChartMain
    /// </summary>
    public partial class PieChartMain : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tab_barchartProperties;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_chartName;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_refreshPeriod;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox lb_width;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox lb_height;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_backColour;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtn_multipleQuery;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtn_singleQuery;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmb_singleQuery;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_addBar;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scroll_multipleQuery;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel wrap_multipleQuery;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_next;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_back;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_finish;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Windows\PieChartMain.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_close;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Management Dashboard;component/windows/piechartmain.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\PieChartMain.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tab_barchartProperties = ((System.Windows.Controls.TabControl)(target));
            return;
            case 2:
            this.txt_chartName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txt_refreshPeriod = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.lb_width = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.lb_height = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cmb_backColour = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.rbtn_multipleQuery = ((System.Windows.Controls.RadioButton)(target));
            
            #line 40 "..\..\..\Windows\PieChartMain.xaml"
            this.rbtn_multipleQuery.Checked += new System.Windows.RoutedEventHandler(this.rbtn_multipleQuery_Checked);
            
            #line default
            #line hidden
            return;
            case 8:
            this.rbtn_singleQuery = ((System.Windows.Controls.RadioButton)(target));
            
            #line 41 "..\..\..\Windows\PieChartMain.xaml"
            this.rbtn_singleQuery.Checked += new System.Windows.RoutedEventHandler(this.rbtn_singleQuery_Checked);
            
            #line default
            #line hidden
            return;
            case 9:
            this.cmb_singleQuery = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.btn_addBar = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\Windows\PieChartMain.xaml"
            this.btn_addBar.Click += new System.Windows.RoutedEventHandler(this.btn_addBar_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.scroll_multipleQuery = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 12:
            this.wrap_multipleQuery = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 13:
            this.btn_next = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\Windows\PieChartMain.xaml"
            this.btn_next.Click += new System.Windows.RoutedEventHandler(this.Next_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btn_back = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\..\Windows\PieChartMain.xaml"
            this.btn_back.Click += new System.Windows.RoutedEventHandler(this.Back_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btn_finish = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\Windows\PieChartMain.xaml"
            this.btn_finish.Click += new System.Windows.RoutedEventHandler(this.btn_finish_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btn_close = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\..\Windows\PieChartMain.xaml"
            this.btn_close.Click += new System.Windows.RoutedEventHandler(this.btn_close_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

