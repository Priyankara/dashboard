create Database ManagementDashBoard

create table ConectedSystem(
systemName varchar(200),
DBName varchar(150),
serverName varchar(150),
instanceName varchar(150),
authenticationType varchar(150),
username varchar(100),
password varchar(100),
ip varchar(20),
port char(5),
type varchar(30),
activated int default 0,

CONSTRAINT CS_pk PRIMARY KEY (systemName))


create table SysTableColumn(
systemName varchar(200),
TableName varchar(200),
columnName varchar(300),

CONSTRAINT ST_pk PRIMARY KEY (systemName,TableName,columnName))


create table SysExcelFiles(
systemName varchar(200),
fileName varchar(300),
sheetName varchar(200),
columnName varchar(200),

CONSTRAINT SEF_pk PRIMARY KEY (systemName,fileName,sheetName,columnName))


create table Query(
queryName varchar(200),
systemName varchar(200),
queryType varchar(50),
query varchar(2000),

CONSTRAINT Q_pk PRIMARY KEY (queryName))


create table DashBoardView(
viewName varchar(200),
width real default (1192.0),
height real default (615.0),

CONSTRAINT DBV_pk PRIMARY KEY (viewName))


create table Component(
componentName varchar(200),
componentType varchar(200),
width int default 1,
height int default 1,
refreshRate int default 0,
type int,
field1 varchar(300), 
field2 varchar(300), 
field3 varchar(300), 
field4 varchar(300), 
field5 varchar(300),
field6 varchar(1000),  

CONSTRAINT C_pk PRIMARY KEY (componentName))


create table ViewComponent(
componentName varchar(200),
viewName varchar(200),
location_x real,
location_y real,
scale_x real,
scale_y real,

CONSTRAINT VC_pk PRIMARY KEY (componentName,viewName),
CONSTRAINT VC_fk1 FOREIGN KEY(componentName) REFERENCES Component(componentName),
CONSTRAINT VC_fk2 FOREIGN KEY(viewName) REFERENCES DashBoardView(viewName))


create table QueryComponent(
componentName varchar(200),
queryName varchar(200),
fieldName varchar(200),
IdNumber int IDENTITY(1,1),

CONSTRAINT QC_pk PRIMARY KEY (IdNumber),
CONSTRAINT QC_fk1 FOREIGN KEY(componentName) REFERENCES Component(componentName),
CONSTRAINT QC_fk2 FOREIGN KEY(queryName) REFERENCES Query(queryName))




-------------------------------------------------------
drop table ConectedSystem
drop table SysTableColumn
drop table SysExcelFiles
drop table Query
drop table DashBoardView
drop table Component
drop table ViewComponent
drop table QueryComponent

delete  from ConectedSystem
delete  from SysTableColumn
delete  from SysExcelFiles
delete  from Query
delete  from DashBoardView
delete  from Component
delete  from ViewComponent
delete  from QueryComponent

select * from ConectedSystem
select * from SysTableColumn
select * from SysExcelFiles
select * from Query
select * from DashBoardView
select * from Component
select * from ViewComponent
select * from QueryComponent



----------------------------------------------------------------------------------------------------

delete  from ConectedSystem where systemName=''

---------------------------------------------------------------------------------------------------

SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables

SELECT TABLE_SCHEMA + '.' + TABLE_NAME, *
FROM INFORMATION_SCHEMA.TABLES

SELECT COLUMN_NAME,* 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'M_CUSTOMER' 


AND TABLE_SCHEMA='YourSchemaName'


WHERE TABLE_TYPE = 'BASE TABLE'
ORDER BY TABLE_SCHEMA + '.' + TABLE_NAME

SELECT DISTINCT TableName FROM SysTableColumn where systemName='ITP_FINAL' 

SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables

SELECT *
FROM [ManagementDashboard].[dbo].[ConectedSystem] tab1
    INNER JOIN [DELLIA2012\SQLEXPRESS].[ITP_FINAL].[dbo].[P_JOB_ORDER] tab2
        ON tab1.systemName = tab2.JO_JOB_TYPE
        
        
        
SELECT *
FROM [ManagementDashboard].[dbo].[ConectedSystem] tab1
    INNER JOIN [DELLIA2012\TEST].[TestDB].[dbo].[M_INV_PART_DETAIL] tab2
        ON tab1.systemName = tab2.IPD_SERIAL_NO
        
        
sp_addlinkedserver  'DELLIA2012\TEST' 
 
 
SELECT M_INV_PARTS.IP_MODEL_NO as IP_MODEL_NO, M_INV_PARTS.IP_UNIT_PRICE as IP_UNIT_PRICE FROM M_INV_PARTS 


SELECT M_INV_PARTS.IP_MODEL_NO as IP_MODEL_NO, M_INV_PARTS.IP_NO_OF_QTY as IP_NO_OF_QTY FROM M_INV_PARTS WHERE  M_INV_PARTS.IP_NO_OF_QTY >=70  

SELECT M_INV_PARTS.IP_MODEL_NO as IP_MODEL_NO, M_INV_PARTS.IP_REORDER_LEVEL as IP_REORDER_LEVEL FROM M_INV_PARTS WHERE M_INV_PARTS.IP_REORDER_LEVEL >70  