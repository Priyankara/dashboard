
CREATE TABLE M_INV_PARTS
(
	IP_MODEL_NO VARCHAR(30),
	IP_NO_OF_QTY INTEGER NOT NULL,
	IP_UNIT_PRICE FLOAT NOT NULL,
	IP_REORDER_LEVEL INTEGER,	
	CONSTRAINT ip_pk1 PRIMARY KEY (IP_MODEL_NO)	
	
)


CREATE TABLE Numbers
(
	Number VARCHAR(20),
	value FLOAT,	
	CONSTRAINT num_pk1 PRIMARY KEY (Number)		
)


CREATE TABLE Books
(
	BookNumber int,
	BookName varchar(30),	
	CONSTRAINT book_pk1 PRIMARY KEY (BookNumber)		
)

CREATE TABLE BooksSales
(
	SalesId int identity(1,1),
	BookId int,
	BookName varchar(30),
	Price float,
	date date,	
	CONSTRAINT bookSales_pk1 PRIMARY KEY (SalesId)		
)

drop table BooksSales
---------------------------------------------------------------------------------------------------------

insert into Books values(001,'DBMS III');
insert into Books values(002,'SA');
insert into Books values(003,'SET');
insert into Books values(004,'SETM');
insert into Books values(005,'DCCN III');

insert into BooksSales values(001,'DBMS III',100,'02/03/12');
insert into BooksSales values(001,'DBMS III',200,'02/03/12');
insert into BooksSales values(002,'SA',150,'02/03/12');
insert into BooksSales values(003,'SET',100,'02/03/12');
insert into BooksSales values(003,'SET',200,'02/03/12');
insert into BooksSales values(003,'SET',300,'02/03/12');
insert into BooksSales values(004,'SETM',100,'02/03/12');
insert into BooksSales values(005,'DCCN III',200,'02/03/12');
insert into BooksSales values(005,'DCCN III',300,'02/03/12');


-------------------------------------

insert into M_INV_PARTS
values ('EN01',22,200,60)
insert into M_INV_PARTS
values ('EN02',56,650,80)
insert into M_INV_PARTS
values ('EN03',15,180,20)
insert into M_INV_PARTS
values ('G090',77,455,60)
insert into M_INV_PARTS
values ('G091',49,900,50)
insert into M_INV_PARTS
values ('GH01',65,350,15)

-------------------------------------

update M_INV_PARTS
set IP_NO_OF_QTY=60
where IP_MODEL_NO='EN01'

update M_INV_PARTS
set IP_NO_OF_QTY=30
where IP_MODEL_NO='EN02'

update M_INV_PARTS
set IP_NO_OF_QTY=48
where IP_MODEL_NO='EN03'

update M_INV_PARTS
set IP_NO_OF_QTY=80
where IP_MODEL_NO='G090'

update M_INV_PARTS
set IP_NO_OF_QTY=70
where IP_MODEL_NO='G091'

update M_INV_PARTS
set IP_NO_OF_QTY=20
where IP_MODEL_NO='GH01'

-----------------------------------

update M_INV_PARTS
set IP_UNIT_PRICE=300
where IP_MODEL_NO='EN01'

update M_INV_PARTS
set IP_UNIT_PRICE=400
where IP_MODEL_NO='EN02'

update M_INV_PARTS
set IP_UNIT_PRICE=800
where IP_MODEL_NO='EN03'

update M_INV_PARTS
set IP_UNIT_PRICE=150
where IP_MODEL_NO='G090'

update M_INV_PARTS
set IP_UNIT_PRICE=800
where IP_MODEL_NO='G091'

update M_INV_PARTS
set IP_UNIT_PRICE=200
where IP_MODEL_NO='GH01'

---------------------------------

update M_INV_PARTS
set IP_REORDER_LEVEL=40
where IP_MODEL_NO='EN01'

update M_INV_PARTS
set IP_REORDER_LEVEL=20
where IP_MODEL_NO='EN02'

update M_INV_PARTS
set IP_REORDER_LEVEL=80
where IP_MODEL_NO='EN03'

update M_INV_PARTS
set IP_REORDER_LEVEL=50
where IP_MODEL_NO='G090'

update M_INV_PARTS
set IP_REORDER_LEVEL=30
where IP_MODEL_NO='G091'

update M_INV_PARTS
set IP_REORDER_LEVEL=90
where IP_MODEL_NO='GH01'

------------------------------------- 

insert into Numbers values('One',1);
insert into Numbers values('Two',2);
insert into Numbers values('Three',3);

-------------------------------------

delete from M_INV_PARTS
select * from M_INV_PARTS
create database Test