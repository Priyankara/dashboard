﻿using System;
using System.Windows;
using Microsoft.Win32;
using Puma.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        OpenFileDialog fdr = new OpenFileDialog();
        private void button1_Click(object sender, RoutedEventArgs e)
        {

          //  fdr.ShowDialog();
            fdr.Multiselect = false;
              Nullable<bool> isSelected = fdr.ShowDialog();
              if (isSelected == true)
              {
            string filepath = fdr.FileName;
            textBox1.Text = filepath;
             }

        }


   
        private void button2_Click(object sender, RoutedEventArgs e)
        {

            
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Multiselect = false;
            fileDialog.ShowDialog();
            string destinationpath = fileDialog.FileName;
            textBox2.Text = destinationpath;

        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
       

            string sourcepath = textBox1.Text.Trim();

            // Extracting part
           //// string result = "";
   
           // var newBmp = Extract(sourcepath, 5, 5, 3, 3);
           // var tempFile = CreateTempFile(newBmp);

            if (!string.IsNullOrEmpty(sourcepath))
            {
                PumaPage inputfile = new PumaPage(sourcepath);
                inputfile.FileFormat = PumaFileFormat.TxtAscii;
                inputfile.Language = PumaLanguage.English;
                string outputstring = inputfile.RecognizeToString();
                textBox3.Text = outputstring;
                inputfile.Dispose();


            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            string sourcepath = textBox1.Text.Trim();
            string destinationpath = textBox2.Text.Trim();
            if (!string.IsNullOrEmpty(sourcepath) & !string.IsNullOrEmpty(destinationpath))
            {
                PumaPage outputFile = new PumaPage(sourcepath);
                outputFile.FileFormat = PumaFileFormat.TxtAscii;
                outputFile.Language = PumaLanguage.English;
                outputFile.RecognizeToFile(destinationpath);
                outputFile.Dispose();
                MessageBox.Show("Written Successed");

            }
        }

        // 
        private BitmapImage GetImage(string base64String)
        {
            byte[] binaryData = Convert.FromBase64String(base64String);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new MemoryStream(binaryData); //nonresizable instance of the memorystream.
            bi.EndInit();
            return bi;
        }

        // Extracting
        Bitmap Extract(Bitmap bmp, int x1, int y1, int x2, int y2)
        {
            try
            {
                var width = x2 - x1;
                var height = y2 - y1;
                if (bmp == null || width < 1 || height < 1)
                {
                    return null;
                }
                var subImage = bmp.Clone(new System.Drawing.Rectangle(x1, y1, width, height), bmp.PixelFormat);
                return subImage;
            }
            catch
            {
                return null;
            }
        }

       
        string CreateTempFile(Bitmap img)
        {
            var fId = Guid.NewGuid().ToString("N");
            var path = string.Format("{0}{1}.tiff", System.IO.Path.GetTempPath(), fId);
            img.Save(path, ImageFormat.Tiff);
            //
            
        //    Globals.TempFiles.Add(path);
            
            return path;
        }

      


    }
}
